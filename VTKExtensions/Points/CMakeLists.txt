set(classes
  vtkBoundedPlaneSource
  vtkBoundedVolumeSource)

vtk_module_add_module(AsyncParaView::VTKExtensionsPoints
  CLASSES ${classes})

paraview_add_server_manager_xmls(
  XMLS  "Resources/points.xml")
