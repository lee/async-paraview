#!/usr/bin/env bash

set -e
set -x
shopt -s dotglob

readonly name="rxcpp"
readonly ownership="RxCpp Upstream <kwrobot@kitware.com>"
readonly subtree="ThirdParty/$name/vtk$name"
readonly repo="https://gitlab.kitware.com/third-party/rxcpp.git"
readonly tag="for/async-paraview-master-g761b932-20221213"
readonly paths="
Rx/v2/src/rxcpp/
Rx/v2/license.txt

license.md
Readme.html
README.kitware.md

CMakeLists.vtkremoting.txt

.gitattributes
"

extract_source () {
    git_archive
    pushd "$extractdir"
    mv -v "$name-reduced/Rx/v2/src/rxcpp" "$name-reduced/Rx/v2/src/vtkrxcpp"
    mv -v "$name-reduced/CMakeLists.vtkremoting.txt" "$name-reduced/CMakeLists.txt"
    #TODO move thes changes to thirdparty repo
    sed -i "s/VTKRemoting/AsyncParaView/g" "$name-reduced/CMakeLists.txt"
    sed -i "s/HEADER_SUBDIR/HEADERS_SUBDIR/g" "$name-reduced/CMakeLists.txt"
    sed -i "s#src/rxcpp#src/vtkrxcpp#g" "$name-reduced/CMakeLists.txt"

    # Add missing newlines.
    echo >> "$name-reduced/Rx/v2/license.txt"
    popd
}

. "${BASH_SOURCE%/*}/../update-common.sh"
