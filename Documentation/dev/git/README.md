ParaView Async Git Usage
========================

ParaView version tracking and development is hosted by [Git](http://git-scm.com).
Please select a task for further instructions:

Main Tasks
----------

* [Install Git](http://public.kitware.com/Wiki/Git/Download) -
  Git 1.7.2 or greater is preferred (required for development)

* [Download ParaView Async](download.md) - Users start here

* [Develop ParaView Async](develop.md) - Contributors start here

Other Tasks
-----------

* [Review Changes](https://gitlab.kitware.com/async/paraview/-/merge_requests) -
  ParaView GitLab Merge Requests
* [Learn Git](http://public.kitware.com/Wiki/Git/Resources) -
  Third-party documentation
