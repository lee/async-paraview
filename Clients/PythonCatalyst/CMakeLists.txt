# SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
# SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
# SPDX-License-Identifier: BSD-3-Clause
set(classes
  #CPythonAdaptorAPI
  #vtkCPPythonAdaptorAPI
  #vtkCPPythonPipeline
  #vtkCPPythonScriptPipeline
  vtkCPPythonScriptV2Helper
  #vtkCPPythonScriptV2Pipeline
  #vtkCPPythonStringPipeline)
  )

vtk_module_add_module(AsyncParaView::PythonCatalyst
  CLASSES ${classes})
vtk_module_remoting_exclude()
