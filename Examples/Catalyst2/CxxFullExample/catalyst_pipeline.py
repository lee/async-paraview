# Greeting to ensure that ctest knows this script is being imported
print("executing catalyst_pipeline")

import asyncio
from async_paraview.services import (
    PipelineBuilder,
    PropertyManager,
    ActiveObjects,
)
from async_paraview.catalyst import detail

from async_paraview.modules.vtkAPVInSitu import vtkInSituInitializationHelper
from async_paraview.modules.vtkRemotingServerManagerViews import vtkSMApplyController
from async_paraview.modules.vtkRemotingServerManagerViews import (
    vtkSMTransferFunctionProxy,
    vtkSMTransferFunctionManager,
)


async def catalyst_initialize():
    print("executing catalyst_initialize")
    # Creating a view could be moved inside the vtkInSituInitializationHelper
    session = vtkInSituInitializationHelper.GetSession()
    builder = PipelineBuilder(session)
    view = await builder.CreateProxy("views", "RenderView")
    detail.RegisterView(view)


async def catalyst_finalize():
    print("executing catalyst_finalize")


async def take_screenshot(view, name):
    """Take two screenshots of the view

    Currently we manually create SaveScreenshot proxy and set the time of the
    image writer but in the future this sould be part of the extractor logic"""

    session = vtkInSituInitializationHelper.GetSession()
    pmanager = PropertyManager()
    builder = PipelineBuilder(session)
    save_screenshot = await builder.CreateProxy("misc", "SaveScreenshot")
    pmanager.SetValues(
        save_screenshot, force_push=True, **{"View": view, "Writer": "PNG"}
    )

    # first screenshot
    # get the writer and set filename to use
    writer = pmanager.GetValues(save_screenshot)["Writer"]
    values = {"FileName": name}
    pmanager.SetValues(writer, **values, force_push=True)
    await pmanager.ExecuteCommand(save_screenshot, "Write")

    # modify the camera to create a different view of the data
    view.GetCamera().Roll(90)
    await pmanager.Update(view)

    # second screenshot
    # get the writer and set filename to use
    writer = pmanager.GetValues(save_screenshot)["Writer"]
    values = {"FileName": "second_" + name}
    pmanager.SetValues(writer, **values, force_push=True)
    await pmanager.ExecuteCommand(save_screenshot, "Write")

    # reset camera
    view.GetCamera().Roll(-90)

    await builder.DeleteProxy(save_screenshot)


async def save_file(name, producer):
    """Save the data as an multiblock dataset.

    As with take_screenshot the logic here should be moved to an extractor
    """
    session = vtkInSituInitializationHelper.GetSession()
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()
    writer = await builder.CreateProxy(
        "writers", "XMLMultiBlockDataWriter", preinit_values={"Input": producer}
    )
    pmanager.SetValues(writer, FileName=name, force_push=True)
    await pmanager.UpdatePipeline(writer)
    await builder.DeleteProxy(writer)


async def catalyst_execute(info):
    session = vtkInSituInitializationHelper.GetSession()

    # create some microservices to aid with the creation of proxies
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    time = info.timestep
    # registrationName must match the channel name used in the
    # 'CatalystAdaptor'.
    producer = detail.CreateProducer("grid")

    await pmanager.UpdatePipeline(producer)
    # first lets get some data information
    # this is a Reduce operation across all ranks. Additionaly root_only=False
    # means that all reanks will get the final anwser so it is really an
    # AllReduce operation.
    data_info = await pmanager.GatherInformation(producer, root_only=False)
    print(data_info.GetBounds())

    # setup representation and coloring
    controller = vtkSMApplyController()
    active = ActiveObjects(session, "ActiveView")
    view = await anext(active.GetCurrentObservable())

    # for the first timestep we want to set the camera to some specific angle,
    # setup our transfer function to a specific range and create a
    # representation for the producer
    if info.timestep == 0:
        view.GetCamera().Azimuth(35)
        manager = vtkSMTransferFunctionManager()
        scf = manager.GetColorTransferFunction("velocity", view.GetProxyManager())
        vtkSMTransferFunctionProxy.RescaleTransferFunction(scf, 0, 60, True)

        representation = await builder.CreateRepresentation(
            producer, 0, view, "GeometryRepresentation"
        )
        controller.InitializeScalarColoring(producer, 0, view)
        pmanager.SetValues(representation, ColorArrayName=["0", "velocity"])
        pmanager.Push(representation)

    representation = view.FindRepresentation(producer, 0)
    controller.ApplyScalarColoring(representation, True)

    await pmanager.Update(view)
    view.ResetCameraUsingVisiblePropBounds()
    await pmanager.Update(view)

    # Create two tasks for saving some data since screenshots and data files
    # are begin handled exclusively by the Render Service and the Data service
    # respectively these tasks will run in parallel
    # -----------------------------------------
    name = "test_screenshot_" + str(time) + ".png"
    screenshot_task = take_screenshot(view, name)
    datafile_name = "test_file_" + str(time) + ".vtm"
    datafile_save_task = save_file(datafile_name, producer)
    # -----------------------------------------

    await asyncio.gather(screenshot_task, datafile_save_task)
