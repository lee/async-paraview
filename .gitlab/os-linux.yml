# Linux-specific builder configurations and build commands

## Base images

### Fedora

.fedora35:
    image: "kitware/paraview:ci-async-fedora35-20250117"

    variables:
        GIT_SUBMODULE_STRATEGY: none
        CMAKE_CONFIGURATION: fedora35
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci

.linux_disable_thallium_build:
    variables:
        APV_CI_ENABLE_THALLIUM: "OFF"

.linux_thallium_addon:
    variables:
        APV_COMMUNICATION_BACKEND: "THALLIUM"

.linux_asio_addon:
    variables:
        APV_COMMUNICATION_BACKEND: "ASIO"

.fedora_mpich_addon:
    variables:
        MODULES: mpi/mpich-x86_64
        # Even with SHM size of 512m, SIGBUS still happened. Let's just use the
        # network instead for reliability.
        # https://wiki.mpich.org/mpich/index.php/Frequently_Asked_Questions#Q:_Why_MPI_Put_raises_SIGBUS_error_inside_docker.3F
        MPIR_CVAR_NOLOCAL: 1

.fedora35_asio:
    extends:
        - .fedora35
        - .linux_asio_addon
        - .fedora_mpich_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_asio

.fedora35_thallium:
    extends:
        - .fedora35
        - .linux_thallium_addon
        - .fedora_mpich_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_thallium

### Sanitizer builds
.fedora35_memcheck:
    extends:
        - .fedora35
        - .fedora_mpich_addon

    variables:
        CMAKE_BUILD_TYPE: RelWithDebInfo

.fedora35_asan:
    extends: .fedora35_memcheck

    variables:
        CMAKE_CONFIGURATION: fedora35_asan
        CTEST_MEMORYCHECK_TYPE: AddressSanitizer
        # Disable LeakSanitizer for now. It's catching all kinds of errors that
        # need investigated or suppressed.
        CTEST_MEMORYCHECK_SANITIZER_OPTIONS: detect_leaks=0

.fedora35_ubsan:
    extends: .fedora35_memcheck

    variables:
        CMAKE_CONFIGURATION: fedora35_ubsan
        CTEST_MEMORYCHECK_TYPE: UndefinedBehaviorSanitizer
        # Disable LeakSanitizer for now. It's catching all kinds of errors that
        # need investigated or suppressed.
        CTEST_MEMORYCHECK_SANITIZER_OPTIONS: detect_leaks=0

.fedora35_tsan:
    extends: .fedora35_memcheck
    variables:
        CMAKE_CONFIGURATION: fedora35_tsan
        CTEST_MEMORYCHECK_TYPE: ThreadSanitizer
        # Disable LeakSanitizer for now. It's catching all kinds of errors that
        # need investigated or suppressed.
        CTEST_MEMORYCHECK_SANITIZER_OPTIONS: detect_leaks=0

.fedora35_asan_asio:
    extends:
        - .fedora35_asan
        - .linux_asio_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_asan_asio

.fedora35_tsan_asio:
    extends:
        - .fedora35_tsan
        - .linux_asio_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_tsan_asio

.fedora35_ubsan_asio:
    extends:
        - .fedora35_ubsan
        - .linux_asio_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_ubsan_asio

.fedora35_asan_thallium:
    extends:
        - .fedora35_asan
        - .linux_thallium_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_asan_thallium

.fedora35_tsan_thallium:
    extends:
        - .fedora35_tsan
        - .linux_thallium_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_tsan_thallium

.fedora35_ubsan_thallium:
    extends:
        - .fedora35_ubsan
        - .linux_thallium_addon
    variables:
        CMAKE_CONFIGURATION: fedora35_ubsan_thallium


## Tags

.linux_builder_tags:
    tags:
        - build
        - docker
        - linux-x86_64
        - paraview

.linux_test_tags:
    tags:
        - docker
        - linux-x86_64
        - paraview
        - x11

.linux_test_priv_tags:
    tags:
        - docker
        - linux-x86_64
        - paraview
        - x11
        - privileged

## Linux-specific scripts

.before_script_linux: &before_script_linux
    - .gitlab/ci/cmake.sh
    - .gitlab/ci/ninja.sh
    - .gitlab/ci/nasm.sh
    - export PATH=$PWD/.gitlab:$PWD/.gitlab/cmake/bin:$PATH
    - cmake --version
    - ninja --version
    - nasm -v
    - "git submodule update --init --recursive || :"
    - git submodule foreach --recursive cmake -P "$PWD/.gitlab/ci/fetch_submodule.cmake"
    - git submodule sync --recursive
    - git submodule update --init --recursive
    # Load any modules that may be necessary.
    - '[ -n "$MODULES" ] && . /etc/profile.d/modules.sh && module load $MODULES'

.config_spack_linux: &config_spack_linux
    - spack="$PWD/.gitlab/spack/src/bin/spack -C $PWD/.gitlab/spack/config"
    - $spack repo add "$PWD/.gitlab/spack/mochi"

.load_thallium_linux: &load_thallium_linux
    - eval `$spack load --sh mochi-thallium`

.cmake_prep_linux_spack:
    stage: prep

    script:
        - *before_script_linux
        - .gitlab/ci/thallium.sh
    interruptible: true
    timeout: 30 minutes

.cmake_build_linux_spack:
    stage: build

    script:
        - *before_script_linux
        - *config_spack_linux
        - *load_thallium_linux
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - $LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake
        - $LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake &> $GIT_CLONE_PATH/compile_output.log
        - sccache --show-stats
    interruptible: true
    timeout: 1 hour


.cmake_test_linux_spack:
    stage: test

    script:
        - *before_script_linux
        - *config_spack_linux
        - *load_thallium_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake"
    interruptible: true
    timeout: 30 minutes

.cmake_memcheck_linux_spack:
    stage: test

    script:
        - *before_script_linux
        - *config_spack_linux
        - *load_thallium_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_memcheck.cmake"
    interruptible: true
    timeout: 30 minutes

.cmake_build_linux:
    stage: build

    script:
        - *before_script_linux
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - $LAUNCHER ctest -VV -S .gitlab/ci/ctest_configure.cmake
        - $LAUNCHER ctest -VV -S .gitlab/ci/ctest_build.cmake &> $GIT_CLONE_PATH/compile_output.log
        - sccache --show-stats
    interruptible: true
    timeout: 1 hour


.cmake_test_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake"
    interruptible: true
    timeout: 30 minutes

.cmake_memcheck_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_memcheck.cmake"
    interruptible: true
    timeout: 30 minutes
