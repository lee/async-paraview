# CI Docker images

These images build base images for use in the CI infrastructure. You may push
manually built images to the `kitware/paraview` Docker Hub repository.

## Updating images

After updating a `Dockerfile` here, it needs to be rebuilt before it can be
used by the CI. You will need to build and push it yourself to the Docker Hub repository.

### Manual building

After updating the `Dockerfile` (and associated scripts), it's a standard image
build sequence:

```sh
cd $name
docker build -t kitware/paraview:ci-async-$name-$YYYYMMDD .
docker push kitware/paraview:ci-async-$name-$YYYYMMDD
```

## Using the new image(s)

Once the updated image is pushed, the `.gitlab-ci.yml` file here needs to be
updated to the new tag to use the new image.
