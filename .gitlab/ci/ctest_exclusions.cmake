set(test_exclusions)

list(APPEND test_exclusions
  # currently broken on the ci
  "^pv.TestDevelopmentInstall$"

  # async/async-paraview#209
  "^AsyncParaView::APVPythonPython-MPI-Batch-TestFileSystem$"
  )
if(APPLE)
  list(APPEND test_exclusions
  # async/async-paraview#210
    "^pvcs.TestPythonApplication2$"
    "^pvcs.TestPythonApplication3$"
    "^pvcs.TestActiveObjectsMicroservice$"
    "^pvcs.TestDataFileMicroserviceOpenGroup$"
    "^pvcs.TestDataFileMicroserviceOpenSeries$"
    "^pvcs.TestDataFileMicroserviceOpenSingle$"
    "^pvcs.TestFileSystemMicroservice$"
    "^pvcs.TestFileSystemMicroserviceRestrictBrowsing$"
    "^pvcs.TestPipelineBuilderMicroservice$"
    "^pvcs.TestPipelineViewerMicroservice$"
    "^pvcs.TestStateMicroservice$"
    "^pvcs.TestSelectionMicroservice$"
    "^pvcs.TestPipelineBuilderMicroserviceWithRendering$"
    )
  if(CMAKE_SYSTEM_PROCESSOR STREQUAL "arm64")
    list(APPEND test_exclusions
     "^AsyncParaView::RemotingMicroservicesCxx-TestSelectionMicroservice$" #async/async-paraview#212
    )
  endif()
endif()

if(WIN32)
  list(APPEND test_exclusions
  "^pvcs.TestSelectionMicroservice$"
  "^AsyncParaView::RemotingMicroservicesCxx-TestSelectionMicroservice$" #async/async-paraview#213
  )
endif()

if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "asan" OR 
    "$ENV{CMAKE_CONFIGURATION}" MATCHES "tsan" OR
    "$ENV{CMAKE_CONFIGURATION}" MATCHES "ubsan")

    # disable rendering tests on sanitizers since the overhead added in opengl
    # context  operations make the two-stage rendering of viewproxy.Update() unreliable.
    list(APPEND test_exclusions
    "^AsyncParaView::APVPythonPython-TestPipelineBuilderWithRendering$"
    "^AsyncParaView::APVPythonPython-TestWaveletContourPipelineWithRendering$"
    "^AsyncParaView::APVPythonPython-TestRenderViewStreams$"
    "^AsyncParaView::RemotingServerManagerViewsCxx-TestBasicRenderView$"
    "^AsyncParaView::RemotingMicroservicesCxx-TestPipelineBuilderMicroserviceWithRendering$"
    "^pvcs.TestPipelineBuilderMicroserviceWithRendering$"
    )

endif()

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
