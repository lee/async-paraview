set(BUILD_EXAMPLES "OFF" CACHE STRING "")
set(VTK_DEBUG_LEAKS ON CACHE BOOL "")

set(APV_BUILD_TESTING WANT CACHE STRING "")
set(APV_USE_PYTHON WANT CACHE STRING "")
set(APV_USE_VTKM OFF CACHE BOOL "")
set(APV_ENABLE_LIBVPX ON CACHE BOOL "")

# By default, thallium is enabled for supported platforms.
# Disable it if CI environment does not want it.
if ("$ENV{APV_CI_ENABLE_THALLIUM}" STREQUAL "OFF")
  set(APV_ENABLE_THALLIUM OFF CACHE BOOL "")
endif()

set(CTEST_USE_LAUNCHERS "ON" CACHE STRING "")

# Default to Release builds.
if ("$ENV{CMAKE_BUILD_TYPE}" STREQUAL "")
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")
else ()
  set(CMAKE_BUILD_TYPE "$ENV{CMAKE_BUILD_TYPE}" CACHE STRING "")
endif ()

include("${CMAKE_CURRENT_LIST_DIR}/configure_cache.cmake")
