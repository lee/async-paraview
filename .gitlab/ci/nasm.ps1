$erroractionpreference = "stop"

$version = "2.15.05"
$sha256sum = "F5C93C146F52B4F1664FA3CE6579F961A910E869AB0DAE431BD871BDD2584EF2"
$filename = "nasm-$version-win64"
$tarball = "$filename.zip"

$outdir = $pwd.Path
$outdir = "$outdir\.gitlab"
$ProgressPreference = "SilentlyContinue"
Invoke-WebRequest -Uri "https://www.nasm.us/pub/nasm/releasebuilds/$version/win64/$tarball" -OutFile "$outdir\$tarball"
$hash = Get-FileHash "$outdir\$tarball" -Algorithm SHA256
if ($hash.Hash -ne $sha256sum) {
    exit 1
}

Add-Type -AssemblyName System.IO.Compression.FileSystem
[System.IO.Compression.ZipFile]::ExtractToDirectory("$outdir\$tarball", "$outdir")
Move-Item -Path "$outdir\nasm-$version\nasm.exe" -Destination "$outdir\nasm.exe"
