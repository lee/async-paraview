import async_paraview.modules.vtkRemotingMicroservices
from async_paraview.modules.vtkRemotingServerManager import vtkClientSession, vtkSMProxy
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)


class SelectionManager(
    async_paraview.modules.vtkRemotingMicroservices.vtkSelectionMicroservice
):
    """
    A thin Python Wrapper around vtkSelectionMicroservice.
    """

    def __init__(self, session):
        if not isinstance(session, vtkClientSession):
            raise TypeError(" 'session' argument is not of type vtkClientSession")

        super().SetSession(session)

    async def QuerySelect(
        self, query: str, elementType: int, invert: bool
    ) -> vtkSMProxy:
        """ """
        print(elementType)
        proxy = await vtkPythonObservableWrapperUtilities.GetFuture(
            super().QuerySelect(query, elementType, invert)
        )
        return proxy

    async def Extract(self, producer, selection):
        """ """
        proxy = await vtkPythonObservableWrapperUtilities.GetFuture(
            super().Extract(producer, selection)
        )
        return proxy
