import async_paraview.modules.vtkRemotingMicroservices
import async_paraview.modules.vtkRemotingPythonAsyncCore as asyncCore


class ProgressObserver(
    async_paraview.modules.vtkRemotingMicroservices.vtkProgressObserverMicroservice
):
    def __init__(self, session):
        self.SetSession(session)

    def GetServiceProgressObservable(self, service_name):
        return asyncCore.vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetServiceProgressObservable(service_name)
        )

    def GetServiceMetricsObservable(self, service_name):
        return asyncCore.vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetServiceMetricsObservable(service_name)
        )
