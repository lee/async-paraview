import asyncio
import re
import async_paraview.modules.vtkRemotingPythonAsyncCore as asyncCore
import async_paraview.modules.vtkRemotingServerManagerPython as serverManager
from async_paraview.modules.vtkRemotingServerManagerViews import vtkSMApplyController


class ParaT(object):
    """PythonApplication is a wrapper around vtkPVApplication for python that
    takes care of initializing vtkPVApplication with the event loop of asyncio
    which is required in order to allow waiting on futures while processing
    other application events."""

    __instance = None

    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = super(ParaT, cls).__new__(cls)
        return cls.__instance

    def __del__(self):
        """same effect as finalize"""
        self.clear()

    def clear(self):
        """Finalize the application"""

        if self.__Application is not None:
            self.__Application.Finalize()

        if self.__Loop is not None:
            self.__Loop = None

    async def initialize(self, applicationName="python", url=""):
        """initialize PVApplication with the current event loop.
        applicationName is used to identify the application and the main thread in the logs
        """

        # if we are running in apvpython vtkPVPythonApplication should exist
        Loop = asyncCore.vtkPythonRunLoop.GetInstance()
        if Loop is None:
            Loop = asyncCore.vtkPythonRunLoop()

        Loop.AcquireRunningLoopFromPython()
        # Keep a reference. If Loop was just created (usually when we run
        # through python instead of apvpython) we need to keep a reference until
        # the very end otherwise, the only reference to the loop will be destroyed
        # and the singleton release the object.
        self.__Loop = Loop

        App = serverManager.vtkPVPythonApplication.GetInstance()
        # if we are running in python start the PVApplication
        if App is None:
            App = serverManager.vtkPVPythonApplication()
            App.InitializeUsingPython(applicationName)

        self.__Application = App

        return await self._connect(url)

    async def finalize(self):
        """Shutdown the services and the associated endpoints"""
        assert self.__Application is not None
        self.clear()

    def get_options(self):
        """Get PVApplication options as provided in the pipeline. The rest of the arguments are accessible through sys.argv"""
        App = serverManager.vtkPVPythonApplication.GetInstance()
        if App is None:
            App = serverManager.vtkPVPythonApplication()
        return App.GetOptions()

    def rank(self):
        """Get the (MPI) rank of this process. This corresponds to the MPI rank of the CLIENT process"""
        assert self.__Application is not None, "Forgot to initialize application?"
        return self.__Application.GetRank()

    def num_ranks(self):
        """Get number of (MPI) ranks used. This corresponds to the CLIENT MPI ranks"""
        assert self.__Application is not None, "Forgot to initialize application?"
        return self.__Application.GetNumberOfRanks()

    def get_cxx_object(self):
        """Get access to all the C++ API via the wrapped vtkPVPythonApplication object"""
        return self.__Application

    async def _connect(self, url=""):
        assert self.__Loop is not None, "Forgot to initialize application?"
        assert self.__Application is not None, "Forgot to initialize application?"
        if len(url) == 0:
            sessionId = await asyncCore.vtkPythonObservableWrapperUtilities.GetFuture(
                self.__Application.CreateBuiltinSession()
            )
        else:
            sessionId = await asyncCore.vtkPythonObservableWrapperUtilities.GetFuture(
                self.__Application.CreateRemoteSession(url)
            )
        return self.__Application.GetSession(sessionId)

    async def close(self, session, exit_server=False):
        """Disconnect from server and close the session. Optionally, shutdown the remote server"""
        assert self.__Application is not None
        sessionId = self.__Application.GetSessionID(session)
        status = await asyncCore.vtkPythonObservableWrapperUtilities.GetFuture(
            session.Disconnect(exit_server)
        )
        self.__Application.CloseSession(sessionId)
        # TODO: when backend can manage multiple sessions of different types (builtin, remote)
        # remove this finalize step.
        await self.finalize()
        return status


class ReactiveCommandHelper(asyncCore.vtkReactiveCommandHelper):
    """Create an async iterator out of a vtkCommand event."""

    def __init__(self, object, eventId):
        self.SetObject(object)
        self.SetEventId(eventId)

    def __del__(self):
        self.Unsubscribe()

    def GetObservable(self):
        return asyncCore.vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetObservable()
        )


class ApplyController(vtkSMApplyController):
    """A thin wrapper for  vtkSMApplyController that can be observed for ApplyEvents"""

    def __init__(self, session):
        self._Session = session
        self._helper = None

    def __del__(self):
        self._Session = None
        self._helper = None

    def GetObservable(self):
        """Emits a value each time self.Apply() is executed"""
        if self._helper is None:
            self._helper = ReactiveCommandHelper(self, vtkSMApplyController.ApplyEvent)
        return self._helper.GetObservable()

    def Unsubscribe(self):
        """Terminate all asyn iterators created in GetObservable"""
        if self._helper is not None:
            self._helper.Unsubscribe()

    def Apply(self, time=0.0):
        pm = self._Session.GetProxyManager()
        super().Apply(pm, time)
