def set_logging_level(level=0):
    """Set the logging level for the async_paraview module.
    level can be an int [-9,9] or a string {"INFO","WARNING"."ERROR","TRACE"}
    The levels follow the vtkLogger conventions:
    OFF = -9
    ERROR = -2
    WARNING = -1
    INFO = 0
    TRACE = 9
    """
    from vtkmodules.vtkCommonCore import vtkLogger

    if isinstance(level, (str, int)):
        verbosity = vtkLogger.ConvertToVerbosity(level)
    else:
        verbosity = vtkLogger.VERBOSITY_INFO
    vtkLogger.SetStderrVerbosity(verbosity)
    LOGGING_PACKAGES = [
        "async_paraview.trame.simput",
        "async_paraview.trame.domains",
        "async_paraview.trame.rca",
        "async_paraview.services.active",
        "async_paraview.services.app",
        "async_paraview.services.datafile",
        "async_paraview.services.definitions",
        "async_paraview.services.filesystem",
        "async_paraview.services.pipeline",
        "async_paraview.services.progress",
        "async_paraview.services.properties",
        "async_paraview.services.selection",
    ]
    import logging

    vtk_to_logging = {
        vtkLogger.VERBOSITY_INFO: logging.INFO,
        vtkLogger.VERBOSITY_WARNING: logging.WARN,
        vtkLogger.VERBOSITY_ERROR: logging.ERROR,
        vtkLogger.VERBOSITY_TRACE: logging.DEBUG,
        vtkLogger.VERBOSITY_OFF: logging.CRITICAL + 1,
    }

    for package_name in LOGGING_PACKAGES:
        logging.getLogger(package_name).setLevel(
            vtk_to_logging.get(verbosity, logging.INFO)
        )
