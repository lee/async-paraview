from trame_simput import get_simput_manager
from trame_simput.widgets import simput
from trame_simput.core.proxy import Proxy as SimputProxy

from vtkmodules.vtkCommonCore import vtkObject
from async_paraview.modules.vtkRemotingServerManager import vtkSMProxy

from .domains import register_domains

from async_paraview.services import PropertyManager
import xml.etree.ElementTree as ET
import yaml
import json
import logging

QT_2_VUETIFY = {
    "root": "div",
    "group": "sw-group",
    "QCheckBox": "sw-switch",
    "pqDoubleRangeWidget": "sw-slider",
    "QComboBox": "sw-select",
    "pqDoubleLineEdit": "sw-text-field",  # FIXME
    "pqLineEdit": "sw-text-field",  # FIXME
    "pqIntRangeWidget": "sw-slider",  # FIXME
    "pqScalarValueListPropertyWidget": "sw-scalar-range",
    "pqArraySelectionWidget": "sw-select",
    "pqProxySelectionWidget": "sw-select",
    "ColorArray": "sw-group",  # FIXME
    "color_selector_with_palette": None,  # FIXME
    "color_selector": None,
    "InteractivePlane": "sw-group",
    "InteractiveCylinder": "sw-group",
    "InteractiveBox": "sw-group",
    "InteractiveLine": "sw-group",
    "InteractiveSphere": "sw-group",
}


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# -----------------------------------------------------------------------------
# Global instances

# Global instance of ParaViewSimput
PVS = None
# -----------------------------------------------------------------------------


class Resolver:
    def __init__(self):
        self._model = None
        self._labels = None

    def xml_elem_widget(self, entry, default=None):
        widget_name = entry.get("widget")
        if not widget_name:
            widget_name = default

        if widget_name in QT_2_VUETIFY:
            ui_elem = QT_2_VUETIFY[widget_name]
            if ui_elem:
                return ET.Element(ui_elem)
            return None

        logger.debug(f"Unknown widget: {widget_name}, {entry}")

    def property_xml_element(self, name, property, label=None, help=None):
        """Generate xml-based description for a property definition.
        This should use common html tags like div/v-col and tag known by simput
        like sw-select, sw-switch etc."""

        # Always hide input property
        if name == "Input":
            return None

        property_type = property.get("type")
        if property_type != "proxy":
            return self.xml_node(name, property, label, help)
        else:
            # proxy properties need the following format
            #   <v-col class="pa-0">
            #     <sw-{some widget} />
            #     <sw-proxy />
            #   </v-col>
            xml_elem = ET.Element("v-col")
            xml_elem.set(":mtime", "data.mtime")
            xml_elem.set("class", "pa-0")
            sw_widget = self.xml_node(name, property, label, help)
            if sw_widget is None:
                return None
            xml_elem.append(sw_widget)
            sw_proxy = self.xml_node(name, property, label, help)
            sw_proxy.tag = "sw-proxy"
            xml_elem.append(sw_proxy)
            return xml_elem

    def xml_node(self, name, property, label=None, help=None):

        ui_attributes = property.get("ui_attributes")
        xml_elem = self.xml_elem_widget(ui_attributes)
        if xml_elem is not None:
            xml_elem.set(":mtime", "data.mtime")
            xml_elem.set(
                "layout",
                ui_attributes["layout"]
                if ui_attributes["layout"] != ""
                else "horizontal",
            )
            xml_elem.set("name", name)
            xml_elem.set("label", label if label else name)
            xml_elem.set("size", f'{property.get("size", 1)}')
            xml_elem.set("type", property.get("type", "string"))
            xml_elem.set(":initial", f"data.original['{name}']")
            if help:
                xml_elem.set("help", help)
        else:
            logger.debug(f"Do not know how to handle property {ui_attributes}")

        return xml_elem

    def resolve(self, model, labels, ui=None):
        groups = {}
        order = []
        last_group = -1
        for prop_name in model.get("_ordered_properties", []):
            property = model.get(prop_name)
            group_name = property.get("_group")

            if group_name is None:
                last_group = -1
                order.append(("property", prop_name))
            elif group_name != last_group:
                last_group = group_name
                order.append(("group", group_name))

            if group_name is not None:
                groups.setdefault(group_name, [])
                groups[group_name].append(prop_name)

        # Generate output
        out_root = ET.Element("div")
        for type, name in order:
            if type == "group":
                group_name = name
                group = model.get(f"_group_{group_name}")
                group_container = self.xml_elem_widget(group, "group")
                if group_container is None:
                    logger.warning(f"skip group due to unknown widget {group}")
                    continue
                # TODO handle uniformly
                if group.get("label") == "Point Locator":
                    continue

                out_root.append(group_container)
                group_container.set("title", group.get("label"))
                group_container.set("label", f"_group_{group_name}")
                group_container.set("title", group.get("label"))
                group_container.set(":mtime", "data.mtime")

                for prop_name in groups.get(group_name):
                    xml_elem = self.property_xml_element(
                        prop_name,
                        model.get(prop_name),
                        labels.get(prop_name, {}).get("_label", prop_name),
                        labels.get(prop_name, {}).get("_help", None),
                    )
                    if xml_elem is not None:
                        group_container.append(xml_elem)

            if type == "property":
                prop_name = name
                xml_elem = self.property_xml_element(
                    prop_name,
                    model.get(prop_name),
                    labels.get(prop_name, {}).get("_label", prop_name),
                    labels.get(prop_name, {}).get("_help", None),
                )
                if xml_elem is not None:
                    out_root.append(xml_elem)

        ET.indent(out_root, space="  ", level=0)
        logger.debug(ET.tostring(out_root, encoding="utf-8").decode("utf-8"))
        return ET.tostring(out_root, encoding="utf-8").decode("utf-8")


# -----------------------------------------------------------------------------
class ObjectFactory:
    def __init__(self):
        self._next = None

    def next(self, proxy):
        self._next = proxy

    def create(self, name, **kwargs):
        obj = self._next
        self._next = None

        return obj


# -----------------------------------------------------------------------------
class ProxyAdapter:
    def __init__(self, server):
        self._server = server
        self.prop_manager = PropertyManager()

    def commit(self, simput_proxy):
        """Flush Modified Properties"""
        logger.debug("commit")
        pv_proxy = simput_proxy.object
        updates = {}
        for name in simput_proxy.edited_property_names:
            value = simput_proxy[name]
            if isinstance(value, SimputProxy):
                value = value.object if value else None
            elif value is None:
                continue
            updates[name] = value

        self.prop_manager.SetValues(pv_proxy, use_unchecked=False, **updates)

        self.prop_manager.Push(pv_proxy)
        self._server.controller.on_apply()

        return len(simput_proxy.edited_property_names)

    def reset(self, simput_proxy, *prop_name):
        """Undo any uncommited properties"""
        logger.debug("reset")
        pv_proxy = simput_proxy.object
        for name in prop_name[0]:
            pv_property = pv_proxy.GetProperty(name)
            if pv_property is not None:
                pv_property.ClearUncheckedElements()

    def fetch(self, simput_proxy):
        """Get the property values from the commited stage"""
        logger.debug("fetch")
        pv_proxy = simput_proxy.object
        values = self.prop_manager.GetValues(
            pv_proxy, *simput_proxy.list_property_names()
        )
        for prop_name, prop_value in values.items():
            if isinstance(prop_value, vtkSMProxy):
                PVS.pv_proxy_ensure_binding(prop_value)
                prop_value = PVS.to_sinput(prop_value)
                logger.debug(f"Set {prop_name} to {prop_value}")
                simput_proxy.set_property(prop_name, prop_value)
            elif isinstance(prop_value, vtkObject):
                logger.error(
                    f"Need to properly handle property {prop_name} of type {prop_value.GetClassName()}"
                )
            else:
                logger.debug(f"Set {prop_name} to {prop_value}")
                simput_proxy.set_property(prop_name, prop_value)

    def update(self, simput_proxy, *property_names):
        pv_proxy = simput_proxy.object
        logger.debug(f"update {property_names}")

        updates = {}
        for name in property_names:
            value = simput_proxy[name]
            if isinstance(value, SimputProxy):
                value = value.object if value else None
            elif value is None:
                continue
            updates[name] = value
        logger.debug(f"update {updates}")
        self.prop_manager.SetValues(pv_proxy, use_unchecked=True, **updates)
        # force a re-evaluation of domains
        simput_proxy.manager.dirty_proxy_domains.add(simput_proxy.id)

    def before_delete(self, simput_proxy):
        pv_proxy = simput_proxy.object
        print(
            "Deleting PV proxy",
            simput_proxy.id,
            pv_proxy.GetGlobalID(),
            pv_proxy.GetReferenceCount(),
        )
        # # simple.Delete(pv_proxy)
        # print("simple.Delete() => done", pv_proxy.GetReferenceCount())


# -----------------------------------------------------------------------------


class ParaViewSimput:
    def __init__(self, server, name="pxm"):
        self._server = server
        self._factory = ObjectFactory()
        self._definitions = None
        self._simput = get_simput_manager(
            name,
            object_factory=self._factory,
            object_adapter=ProxyAdapter(self._server),
            resolver=Resolver(),
        )

        register_domains()
        self._root_widget = simput.Simput(
            self._simput, prefix=name, trame_server=server
        )
        self._pv_to_simput_ids = {}

        # set global instance for accessing from other modules
        global PVS
        PVS = self

    def to_sinput(self, pv_id_or_proxy):
        pv_id = None
        name = None
        group = None
        if isinstance(pv_id_or_proxy, (int, str)):
            # id
            pv_id = str(pv_id_or_proxy)
        else:
            # proxy
            pv_id = str(pv_id_or_proxy.GetGlobalID())
            name = pv_id_or_proxy.GetXMLName()
            group = pv_id_or_proxy.GetXMLGroup()

        simput_id = self._pv_to_simput_ids.get(pv_id, None)
        if simput_id is not None:
            return self._simput.proxymanager.get(simput_id)

        # Proxy not found
        if name is not None:
            logger.warning(
                f"No simput proxy associated with PV proxy {pv_id} ({group}-{name})"
            )
        else:
            logger.warning(f"No simput proxy associated with PV proxy {pv_id}")
        return None

    def to_pv(self, simput_id_or_proxy):
        simput_proxy = simput_id_or_proxy
        if isinstance(simput_id_or_proxy, str):
            simput_proxy = self._simput.proxymanager.get(simput_id_or_proxy)

        return simput_proxy.object if simput_proxy else None

    def set_definition_manager(self, def_manager):
        self._definitions = def_manager

    @property
    def manager(self):
        return self._simput

    @property
    def pxm(self):
        return self._simput.proxymanager

    @property
    def root_widget(self):
        return self._root_widget

    def apply(self, **kwargs):
        self.root_widget.apply(**kwargs)

    def reset(self, **kwargs):
        self.root_widget.reset(**kwargs)

    def pv_proxy_ensure_binding(self, pv_proxy):
        if isinstance(pv_proxy, (int, str)):
            proxy_id = str(pv_proxy)
        else:
            proxy_id = str(pv_proxy.GetGlobalID())
        if proxy_id in self._pv_to_simput_ids:
            return

        # Take care of ourself
        definition = self._definitions.GetDefinition(pv_proxy)
        self._simput.load_model(yaml_content=yaml.dump(definition.as_dict()))
        logger.debug(json.dumps(definition.as_dict(), indent=2))
        proxy_type = f"{pv_proxy.GetXMLGroup()}__{pv_proxy.GetXMLName()}"
        self._factory.next(pv_proxy)
        simput_proxy = self.pxm.create(proxy_type)
        self._pv_to_simput_ids[str(pv_proxy.GetGlobalID())] = simput_proxy.id

        # Read property from proxy and update simput entry
        simput_proxy.fetch()
        # TODO document why we need commit
        simput_proxy.commit()
        return simput_proxy

    async def create(self, pv_proxy):
        # TODO no need for async
        simput_proxy = self.pv_proxy_ensure_binding(pv_proxy)
        return simput_proxy
