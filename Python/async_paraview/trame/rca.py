import time
import json
import asyncio
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)
from vtkmodules.vtkRenderingCore import vtkRenderWindowInteractor
from async_paraview.services import PropertyManager
from trame.app import asynchronous
from vtkmodules.vtkWebCore import vtkRemoteInteractionAdapter


class ViewAdapter:
    def __init__(self, view, name, target_fps=30):
        self._proxy_handler = PropertyManager()
        self._view = view
        self.area_name = name
        self.streamer = None
        self.last_meta = None
        self.animating = False
        self.target_fps = target_fps

        self.last_button = -1
        self.last_view_height = 300
        self._iren = vtkRenderWindowInteractor()
        self._iren.SetRenderWindow(view.GetRenderWindow())
        # Do not propagate interactor render events to the render window.
        self._iren.EnableRenderOff()
        # Do not show the window, images are streamed anyway.
        self._view.GetRenderWindow().ShowWindowOff()

        self._interaction_adapter = vtkRemoteInteractionAdapter()
        self._interaction_adapter.SetInteractor(self._iren)

        asynchronous.create_task(self._listen_to_view())

    async def _listen_to_view(self):
        async for package in vtkPythonObservableWrapperUtilities.GetIterator(
            self._view.GetViewOutputObservable()
        ):
            if package:
                # print(package.GetMimeType(), " ----- ", package.GetCodecLongName())
                self.push(
                    memoryview(package.GetData()),
                    dict(
                        type=package.GetMimeType(),
                        codec=package.GetCodecLongName(),
                        w=package.GetDisplayWidth(),
                        h=package.GetDisplayHeight(),
                        st=int(time.time_ns() / 1000000),
                        key=("key" if package.GetIsKeyFrame() else "delta"),
                    ),
                )

    async def _animate(self):
        while self.animating:
            self._view.InteractiveRender()
            await asyncio.sleep(1 / self.target_fps)
        self._view.StillRender()

    def set_streamer(self, stream_manager):
        self.streamer = stream_manager

    def update_size(self, origin, size):
        need_video_header = size.get("videoHeader", 0)
        width = int(size.get("w", 300))
        height = int(size.get("h", 300))

        self.last_view_height = height
        self._proxy_handler.SetValues(
            self._view,
            ViewSize=(width, height),
            RequestMediaInitializationSegment=need_video_header,
            force_push=True,
        )
        self._view.StillRender()

    def push(self, content, meta=None):
        if meta is not None:
            self.last_meta = meta
        self.streamer.push_content(self.area_name, self.last_meta, content)

    def on_interaction(self, origin, event):
        event_type = event["type"]
        if event_type == "StartInteractionEvent":
            if not self.animating:
                self.animating = True
                asynchronous.create_task(self._animate())
        elif event_type == "EndInteractionEvent":
            self.animating = False
        else:
            event_str = json.dumps(event)
            self._interaction_adapter.ProcessEvent(event_str)
