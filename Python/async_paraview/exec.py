import async_paraview.venv  # noqa
import importlib
import os
import sys

PV_EXEC = None

if "--exec" in sys.argv:
    base_index = sys.argv.index("--exec")
    PV_EXEC = sys.argv[base_index + 1]
    # remove --exec PV_EXEC from args
    sys.argv.pop(base_index)
    sys.argv.pop(base_index)
else:
    PV_EXEC = os.environ.get("PV_EXEC", None)


def main():
    if PV_EXEC is None:
        print("Could not find environment variable PV_EXEC or --exec arg")
        return

    module = importlib.import_module(PV_EXEC)
    module.main()


if __name__ == "__main__":
    main()
