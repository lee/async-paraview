import asyncio

from async_paraview.services import ParaT, FileSystem

# use python os for validation
import os


async def test_on_location(file_system, location, App):
    prefix = str(location) + "_"
    if App.num_ranks() > 1:
        prefix += "MPI-BATCH_"
    old_dir = os.path.join(os.getcwd(), prefix + "Old")
    new_dir = os.path.join(os.getcwd(), prefix + "New")

    assert await file_system.MakeDirectory(old_dir, location)
    if App.rank() == 0:  # do file_system tests in one rank to avoid race conditions
        assert os.path.exists(old_dir)

    assert await file_system.Rename(old_dir, new_dir, location)
    if App.rank() == 0:
        assert not os.path.exists(old_dir)
        assert os.path.exists(new_dir)

    # delete something that does not exist
    try:
        await file_system.Remove(old_dir, location)
        assert False
    except RuntimeError as error:
        print(str(error))

    # create dummy files to test Remove
    dummy1 = os.path.join(new_dir, prefix + "test_remove_python1")
    dummy2 = os.path.join(new_dir, prefix + "test_remove_python2")
    open(dummy1, "a")
    open(dummy2, "a")

    # remove a file
    assert await file_system.Remove(dummy1, location)
    # remove a file that does not exist raises an error
    try:
        await file_system.Remove(dummy1, location)
        assert False
    except RuntimeError as error:
        print(str(error))

    # remove a non-empty directory
    assert await file_system.Remove(new_dir, location)

    # Create and remove and empty directory
    assert await file_system.MakeDirectory(old_dir, location)

    assert await file_system.Remove(old_dir, location)

    # TODO add assertion test for format
    listing = await file_system.ListDirectory(os.getcwd(), location)
    import json

    print(json.dumps(listing, indent=1))


async def main():
    App = ParaT()
    session = await App.initialize()

    file_system = FileSystem(session)

    if App.num_ranks() == 1:
        await test_on_location(file_system, FileSystem.Location.CLIENT, App)
    await test_on_location(file_system, FileSystem.Location.SERVER, App)

    await App.close(session)


asyncio.run(main())
