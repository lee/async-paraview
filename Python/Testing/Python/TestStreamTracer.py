import asyncio

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager
import platform


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")
    tracer = await builder.CreateProxy(
        "filters",
        "StreamTracer",
        Input=ugrid,
        SelectInputVectors=["", "", "", "0", "Swirl"],
        MaximumPropagation=20.0,
        IntegrationDirection=2,
    )

    pmanager.Push(tracer)

    # setup seeds line
    seeds = pmanager.GetValues(tracer)["Source"]
    pmanager.SetValues(
        seeds, Point1=[-10, -10, -10], Point2=[10, 10, 10], Resolution=1000
    )
    pmanager.Push(seeds)

    status = await pmanager.UpdatePipeline(tracer, 0)
    assert status

    # expected values in serial execution
    ugrid_expected_npoints = 9261
    seeds_expected_npoints = 1001
    tracer_expected_npoints = 754460
    if  platform.system() == "Darwin" and platform.processor() == "arm": #this is a bug async/async-paraview#211
      tracer_expected_npoints = 375255

    print(f"{App.rank()} - {ugrid.GetDataInformation().GetNumberOfPoints()=}")
    print(f"{App.rank()} - {seeds.GetDataInformation().GetNumberOfPoints()=}")
    print(f"{App.rank()} - {tracer.GetDataInformation().GetNumberOfPoints()=}")
    if App.num_ranks() > 1:
        # local counters
        assert ugrid.GetDataInformation().GetNumberOfPoints() < ugrid_expected_npoints
        assert seeds.GetDataInformation().GetNumberOfPoints() == seeds_expected_npoints
        assert tracer.GetDataInformation().GetNumberOfPoints() < tracer_expected_npoints

        # perform AllGather to get global counters and distribute them to all ranks
        data_info_ugrid = await pmanager.GatherInformation(ugrid, root_only=False)
        data_info_seeds = await pmanager.GatherInformation(seeds, root_only=False)
        assert data_info_ugrid.GetNumberOfPoints() > ugrid_expected_npoints
        assert data_info_seeds.GetNumberOfPoints() > seeds_expected_npoints

        # compare local to global data information
        data_info_tracer_local = await pmanager.GatherInformation(
            tracer, root_only=True
        )
        data_info_tracer_global = await pmanager.GatherInformation(
            tracer, root_only=False
        )
        print(f"{App.rank()} - {data_info_tracer_local.GetNumberOfPoints()=}")
        print(f"{App.rank()} - {data_info_tracer_global.GetNumberOfPoints()=}")
        # compare with local
        if App.rank() > 0:
            assert (
                data_info_tracer_local.GetNumberOfPoints()
                < data_info_tracer_global.GetNumberOfPoints()
            )
            assert (
                tracer.GetDataInformation().GetNumberOfPoints()
                < data_info_tracer_global.GetNumberOfPoints()
            )
        else:
            assert (
                data_info_tracer_local.GetNumberOfPoints()
                == data_info_tracer_global.GetNumberOfPoints()
            )
            assert (
                tracer.GetDataInformation().GetNumberOfPoints()
                < data_info_tracer_global.GetNumberOfPoints()
            )

    else:
        assert ugrid.GetDataInformation().GetNumberOfPoints() == ugrid_expected_npoints
        assert seeds.GetDataInformation().GetNumberOfPoints() == seeds_expected_npoints
        assert (
            tracer.GetDataInformation().GetNumberOfPoints() == tracer_expected_npoints
        )

    await App.close(session)


asyncio.run(main())
