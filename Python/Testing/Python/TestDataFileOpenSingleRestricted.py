import asyncio

from async_paraview.services import ParaT, DataFile, PropertyManager, FileSystem
from vtkmodules.vtkCommonCore import VTK_IMAGE_DATA

from vtk.util.misc import vtkGetDataRoot
import os
import os.path
from pathlib import Path

expectedReaders = [("sources", "XMLImageDataReader")]
SEPARATOR = os.sep

# test that vtkDataFileMicroservice can work even in the presence of mapped directories


async def main():
    App = ParaT()
    session = await App.initialize()

    datafileService = DataFile(session)
    filesystemService = FileSystem(session)
    pm = PropertyManager()

    filename = os.path.join(vtkGetDataRoot(), "Testing/Data/rock.vti")
    await filesystemService.AddRootDirectory(
        SEPARATOR + "app-data", str(Path(filename).parent)
    )

    filename = os.path.join(SEPARATOR + "app-data", "rock.vti")
    possibleReaders = await datafileService.FindPossibleReaders(filename)
    assert possibleReaders == expectedReaders

    reader = await datafileService.Open(filename)
    assert reader is not None

    success = await pm.UpdatePipeline(reader, 0)
    assert success == True
    di = reader.GetDataInformation()

    if App.num_ranks() == 1:
        di = reader.GetDataInformation()
    else:
        di = reader.GetDataInformation()
        di = await pm.GatherInformation(reader)

    await App.close(session)

    if App.rank() == 0:
        assert di.GetDataSetType() == VTK_IMAGE_DATA
        if App.num_ranks() == 0:  # interface points are duplicated
            assert di.GetNumberOfPoints() == 17139400
        assert di.GetNumberOfCells() == 16917660


asyncio.run(main())
