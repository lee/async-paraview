import asyncio
from pathlib import Path

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pm = PropertyManager()

    # polydata
    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    status = await pm.UpdatePipeline(sphere)

    writer = await builder.CreateProxy(
        "writers", "XMLPolyDataWriter", preinit_values={"Input": sphere}
    )
    pm.SetValues(writer, FileName="out.vtp", force_push=True)
    status = await pm.UpdatePipeline(writer)
    assert status
    assert Path("./out.vtp").exists()
    assert Path("./out.vtp").stat().st_size > 0

    # unstructured grid
    cells = await builder.CreateProxy("sources", "UnstructuredCellTypes")
    status = await pm.UpdatePipeline(cells)

    writer = await builder.CreateProxy(
        "writers", "XMLUnstructuredGridWriter", preinit_values={"Input": cells}
    )
    pm.SetValues(writer, FileName="out.vtu", force_push=True)
    status = await pm.UpdatePipeline(writer)
    assert status
    assert Path("./out.vtu").exists()
    assert Path("./out.vtu").stat().st_size > 0

    # TODO re-read the file and make sure it matches the input

    await App.close(session)


asyncio.run(main())
