import asyncio

from async_paraview.services import ParaT, DataFile, PropertyManager
from vtkmodules.vtkCommonCore import VTK_IMAGE_DATA

from vtk.util.misc import vtkGetDataRoot
import os.path

expectedReaders = [("sources", "XMLImageDataReader")]


async def main():
    App = ParaT()
    session = await App.initialize()

    datafileService = DataFile(session)
    pm = PropertyManager()

    filename = os.path.join(vtkGetDataRoot(), "Testing/Data/rock.vti")
    possibleReaders = await datafileService.FindPossibleReaders(filename)
    assert possibleReaders == expectedReaders

    reader = await datafileService.Open(filename)
    assert reader is not None

    success = await pm.UpdatePipeline(reader, 0)
    assert success == True
    if App.num_ranks() == 1:
        di = reader.GetDataInformation()
    else:
        di = reader.GetDataInformation()
        di = await pm.GatherInformation(reader)

    await App.close(session)

    if App.rank() == 0:
        assert di.GetDataSetType() == VTK_IMAGE_DATA
        if App.num_ranks() == 0:  # interface points are duplicated
            assert di.GetNumberOfPoints() == 17139400
        assert di.GetNumberOfCells() == 16917660


asyncio.run(main())
