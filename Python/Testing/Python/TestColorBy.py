import asyncio

from async_paraview.services import (
    ParaT,
    DataFile,
    PropertyManager,
    ApplyController,
    PipelineBuilder,
    anext,
)
from async_paraview.testing import (
    apvtesting,
)

from vtkmodules.vtkRenderingCore import vtkRenderWindowInteractor
from vtk.util.misc import vtkGetDataRoot
import os.path

# Test that the ApplyController will update the coloring of the
# GeometryRepresentation  due to the ColorArrayName property begin modified.


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    datafileService = DataFile(session)
    acontroller = ApplyController(session)
    pm = PropertyManager()

    filename = os.path.join(vtkGetDataRoot(), "Testing/Data/disk_out_ref.ex2")
    reader = await datafileService.Open(filename)

    view = await builder.CreateProxy("views", "RenderView")
    acontroller.Apply()
    await anext(acontroller.GetObservable())
    view.GetCamera().Azimuth(90)
    # --------

    # Get representation and change array name. This should annotate the
    # representation and cause the next Apply() to update its coloring

    representation = view.FindRepresentation(reader, 0)
    pm.SetValues(representation, ColorArrayName=["0", "CH4"])
    pm.Push(representation)

    iren = vtkRenderWindowInteractor()
    iren.SetRenderWindow(view.GetRenderWindow())

    acontroller.Apply()
    await anext(acontroller.GetObservable())

    regressionTestPassed = await apvtesting.DoRegressionTest(view)

    await App.close(session)
    assert regressionTestPassed


asyncio.run(main())
