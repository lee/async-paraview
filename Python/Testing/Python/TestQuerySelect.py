import asyncio

from async_paraview.services import (
    ParaT,
    PipelineBuilder,
    PropertyManager,
    SelectionManager,
)


async def simple_query(session):
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()
    smanager = SelectionManager(session)

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")
    QueryString = "(DistanceSquared<=1)"
    selection = await smanager.QuerySelect(QueryString, SelectionManager.POINT, False)
    extractor = await smanager.Extract(ugrid, selection)
    status = await pmanager.UpdatePipeline(extractor)
    assert status
    assert extractor.GetDataInformation().GetNumberOfPoints() == 7


async def single_variable_expression(session):
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()
    smanager = SelectionManager(session)

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")

    QueryString = "(DistanceSquared>=5) & (DistanceSquared<=10)"
    selection = await smanager.QuerySelect(QueryString, SelectionManager.POINT, False)
    extractor = await smanager.Extract(ugrid, selection)
    status = await pmanager.UpdatePipeline(extractor)
    assert status
    assert extractor.GetDataInformation().GetNumberOfPoints() == 114


async def two_variable_expression(session):
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()
    smanager = SelectionManager(session)

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")
    QueryString = "(DistanceSquared<=5) & (X>=0)"
    selection = await smanager.QuerySelect(QueryString, SelectionManager.POINT, False)
    extractor = await smanager.Extract(ugrid, selection)
    status = await pmanager.UpdatePipeline(extractor)
    assert status
    assert extractor.GetDataInformation().GetNumberOfPoints() == 39


async def operation_expression(session):
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()
    smanager = SelectionManager(session)

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")
    QueryString = "(X == max(X))"
    selection = await smanager.QuerySelect(QueryString, SelectionManager.POINT, False)
    extractor = await smanager.Extract(ugrid, selection)
    status = await pmanager.UpdatePipeline(extractor)
    assert status
    assert extractor.GetDataInformation().GetNumberOfPoints() == 441


async def main():
    global builder
    App = ParaT()
    session = await App.initialize()

    await simple_query(session)
    await single_variable_expression(session)
    await two_variable_expression(session)
    await operation_expression(session)
    await App.close(session)


asyncio.run(main())
