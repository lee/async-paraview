import asyncio
import json
import os

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager
from async_paraview.modules.vtkRemotingMicroservices import vtkStateMicroservice
from vtkmodules.vtkRenderingCore import vtkRenderWindowInteractor


# Used to remove log_name key-value pairs because it encodes the
# memory address of a proxy which varies for different test runs.
def delete_keys_from_dict(dict_del, lst_keys):
    for k in lst_keys:
        try:
            del dict_del[k]
        except KeyError:
            pass
    for v in dict_del.values():
        if isinstance(v, dict):
            delete_keys_from_dict(v, lst_keys)

    return dict_del


# shift all proxy ids to start from 1 so we can compare state across different
# builds that may enable different prototypes. Since prototype creation shifts
# the internal ID counter we need to shift ids to be able to compare them.
def transform_state(state):
    old2new = {}
    counter = 1
    # top level id items i.e recorded proxy ids
    for key in state.copy().keys():
        if key.isnumeric():
            state[str(counter)] = state[key]
            del state[key]
            old2new[key] = str(counter)
            counter += 1

    # update dictionary based on new keys
    def update(d):
        for key, value in d.items():
            if key == "id":
                d[key] = old2new[str(value)]
            elif isinstance(value, dict):
                update(value)

    update(state)
    return state


async def main():
    global expectedState
    app = ParaT()
    session = await app.initialize()
    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    shrink = await builder.CreateProxy(
        "filters", "ShrinkFilter", Input=sphere, ShrinkFactor=0.3
    )
    view = await builder.CreateProxy("views", "RenderView")
    iren = vtkRenderWindowInteractor()
    iren.SetRenderWindow(view.GetRenderWindow())

    representation = await builder.CreateRepresentation(
        shrink, 0, view, "GeometryRepresentation"
    )

    pmanager.SetVisibility(representation, view, True)
    await pmanager.Update(view)
    view.ResetCameraUsingVisiblePropBounds()

    stateService = vtkStateMicroservice()
    stateService.SetSession(session)
    smstate = stateService.Save().DumpJSON()

    actualState = transform_state(
        delete_keys_from_dict(json.loads(smstate), ["log_name"])
    )
    stateFileName = os.path.dirname(__file__) + os.sep + "TestSaveState.json"

    # NOTE: If you've changed the state, set overwriteStateFile to refresh TestSaveState.json
    overwriteStateFile = False
    if overwriteStateFile:
        with open(stateFileName, "w") as stateFile:
            json.dump(actualState, stateFile, indent=4)
            # add a new line so that ghostflow checks on gitlab don't complain about missing newline.
            stateFile.write("\n")
            print(f"Wrote {stateFileName}")

    with open(stateFileName, "r") as stateFile:
        expectedState = transform_state(
            delete_keys_from_dict(json.load(stateFile), ["log_name"])
        )
        assert str(actualState) == str(expectedState)

    await app.close(session)


asyncio.run(main())
