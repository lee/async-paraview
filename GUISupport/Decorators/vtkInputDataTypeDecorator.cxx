/*=========================================================================

   Program: ParaView
   Module:    $RCSfile$

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#include "vtkInputDataTypeDecorator.h"

#include "vtkCommand.h"
#include "vtkObjectFactory.h"
#include "vtkPVDataInformation.h"
#include "vtkPVXMLElement.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMUncheckedPropertyHelper.h"

#include <vtksys/SystemTools.hxx>

#include <string>
#include <vector>

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkInputDataTypeDecorator);

//-----------------------------------------------------------------------------
vtkInputDataTypeDecorator::vtkInputDataTypeDecorator() = default;

//-----------------------------------------------------------------------------
vtkInputDataTypeDecorator::~vtkInputDataTypeDecorator()
{
  if (this->ObservedObject && this->ObserverId)
  {
    this->ObservedObject->RemoveObserver(this->ObserverId);
  }
}

//-----------------------------------------------------------------------------
void vtkInputDataTypeDecorator::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//-----------------------------------------------------------------------------
void vtkInputDataTypeDecorator::initialize(vtkPVXMLElement* config, vtkSMProxy* proxy)
{
  this->Superclass::initialize(config, proxy);
  this->ObserverId = 0;

  vtkSMProperty* prop = proxy ? proxy->GetProperty("Input") : nullptr;
  if (!prop)
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING, << "Could not locate property named 'Input'. "
                                             "vtkInputDataTypeDecorator will have no effect.");
    return;
  }

  this->ObservedObject = prop;
  this->ObserverId = prop->AddObserver(vtkCommand::UncheckedPropertyModifiedEvent, this,
    &vtkInputDataTypeDecorator::InvokeEnableStateChangedEvent);
}

//-----------------------------------------------------------------------------
bool vtkInputDataTypeDecorator::enableWidget() const
{
  const char* enableWidgetActive = this->xml()->GetAttribute("mode");
  // By default, if enable_toggle is not set, we go through
  if (!enableWidgetActive || !strcmp(enableWidgetActive, "enabled_state"))
  {
    if (!this->processState())
    {
      return false;
    }
  }
  return this->Superclass::enableWidget();
}

//-----------------------------------------------------------------------------
bool vtkInputDataTypeDecorator::canShowWidget(bool show_advanced) const
{
  const char* showWidgetActive = this->xml()->GetAttribute("mode");
  // By default, if show_toggle is not set, we DO NOT go through
  if (showWidgetActive && !strcmp(showWidgetActive, "visibility"))
  {
    if (!this->processState())
    {
      return false;
    }
  }
  return this->Superclass::canShowWidget(show_advanced);
}

//-----------------------------------------------------------------------------
bool vtkInputDataTypeDecorator::processState() const
{
  vtkSMProxy* proxy = this->proxy();
  vtkSMProperty* prop = proxy ? proxy->GetProperty("Input") : nullptr;
  if (prop)
  {
    /*
    vtkPipelineSource* source =
      vtkPVGUIApplication::GetInstance()->GetServerManagerModel()->findItem<vtkPipelineSource*>(
        vtkSMUncheckedPropertyHelper(prop).GetAsProxy());
    if (!source)
    {
      return false;
    }
    vtkOutputPort* cur_input = nullptr;
    QList<vtkOutputPort*> ports = source->getOutputPorts();
    cur_input = ports.empty() ? nullptr : ports[0];
    */
    // TODO is this the equivalent ?
    vtkSMSourceProxy* source =
      vtkSMSourceProxy::SafeDownCast(vtkSMUncheckedPropertyHelper(prop).GetAsProxy());
    if (!source)
    {
      return false;
    }

    int exclude = 0;
    this->xml()->GetScalarAttribute("exclude", &exclude);
    std::string dataname = this->xml()->GetAttributeOrDefault("name", "");
    std::vector<std::string> parts = vtksys::SystemTools::SplitString(dataname, ' ');
    if (!parts.empty())
    {
      vtkPVDataInformation* dataInfo = source->GetDataInformation();
      for (std::size_t i = 0; i < parts.size(); ++i)
      {
        const bool match = (parts[i] == "Structured") ? dataInfo->IsDataStructured()
                                                      : dataInfo->DataSetTypeIsA(parts[i].c_str());
        if (match)
        {
          return !exclude;
        }
      }
      return exclude;
    }
    else
    {
      return false;
    }
  }
  return true;
}
