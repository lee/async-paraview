/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsConnection.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioInternalsConnection.h"
#include "vtkAsioInternalsNetworkPacket.h"
#include "vtkServicesAsioLogVerbosity.h"

#include <iomanip>
#include <thread>

//-------------------------------------------------------------------
vtkAsioInternalsConnection::vtkAsioInternalsConnection(io::io_context& ioCtx)
  : Socket(ioCtx)
  , IOCtx(ioCtx)
{
  vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "New Connection");
}

//-------------------------------------------------------------------
vtkAsioInternalsConnection::~vtkAsioInternalsConnection()
{
  this->Close();
  vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Connection destroyed");
}

//-------------------------------------------------------------------
tcp::socket& vtkAsioInternalsConnection::GetSocket()
{
  return this->Socket;
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::AsyncRead()
{
  // Issue a read operation to read exactly the number of bytes in a header.
  asio::async_read(this->Socket, asio::buffer(this->InboundHeader),
    [this, self = shared_from_this()](
      const error_code& ec, std::size_t /*length*/) { this->HandleReadHeader(ec); });
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::PostClose()
{
  asio::post(this->IOCtx, [this, self = shared_from_this()]() { this->Close(); });
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::Close()
{
  if (this->Socket.is_open())
  {
    error_code ec;
    this->Socket.shutdown(tcp::socket::shutdown_both, ec);
    if (ec)
    {
      vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(),
        "EC (%d) (%s) ignored. Socket already disconnected?", ec.value(), ec.message().c_str());
    }
    this->Socket.close(ec);
    if (ec)
    {
      vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(),
        "EC (%d) (%s) ignored. Socket already disconnected?", ec.value(), ec.message().c_str());
    }
  }
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::HandleReadHeader(const error_code& ec)
{
  if (!ec)
  {
    // determine length of data.
    std::istringstream is(std::string(this->InboundHeader, this->Headerlength));
    std::size_t inboundDataSize = 0;
    if (!(is >> std::hex >> inboundDataSize))
    {
      // header is invalid. inform the caller.
      if (this->OnError != nullptr)
      {
        this->OnError(asio::error::invalid_argument);
      }
      return;
    }
    // clear header.
    std::fill(this->InboundHeader, this->InboundHeader + this->Headerlength, 0);

    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), "About to read " << inboundDataSize << " bytes");
    // start an async read of the data.
    this->InboundData.resize(inboundDataSize);
    asio::async_read(this->Socket, asio::buffer(this->InboundData),
      [this, self = shared_from_this()](
        const error_code& ec, std::size_t /*length*/) { this->HandleReadData(ec); });
  }
  else if (ec == asio::error::eof)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Connection was closed by remote peer.");
    this->PostClose();
  }
  else if (ec == asio::error::operation_aborted)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Connection has been closed by execution context.");
  }
  else if (ec == asio::error::bad_descriptor)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Attempt to read on closed socket.");
  }
  else
  {
    vtkLogF(ERROR, "Failed to read header. Closing socket. Error: (%d) (%s)", ec.value(),
      ec.message().c_str());
    this->PostClose();
  }
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::HandleReadData(const error_code& ec)
{
  if (!ec)
  {
    // extract the message from the data.
#ifdef NDEBUG
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(),
      "Read " << std::string(this->InboundData.begin(), this->InboundData.end()));
#else
    auto netPacket = FromBytes(std::string(this->InboundData.begin(), this->InboundData.end()));
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(),
      "Read vtkAsioInternalsNetworkPacket"
        << "Secret:" << netPacket.Secret << "|Destination:" << netPacket.Destination
        << "|Channel:" << netPacket.Channel << "|ResponseRequired:" << netPacket.ResponseRequired
        << "|Payload" << netPacket.Payload->ToString());
#endif
    if (this->OnRead != nullptr)
    {
      // inform caller that message was received.
      this->OnRead(ec, this->InboundData);
    }
    // clear inbound data.
    this->InboundData.clear();
  }
  else if (ec == asio::error::eof)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Connection was closed by remote peer.");
    this->PostClose();
  }
  else if (ec == asio::error::operation_aborted)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Connection has been closed by execution context.");
  }
  else if (ec == asio::error::bad_descriptor)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Attempt to read on closed socket.");
  }
  else
  {
    vtkLogF(ERROR, "Failed to read data. Closing socket. Error: (%d) (%s)", ec.value(),
      ec.message().c_str());
    this->PostClose();
  }
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::PostWrite(std::string&& message)
{
  asio::post(this->IOCtx, [this, message]() {
    // format the header.
    std::ostringstream headerStream;
    headerStream << std::setfill('0') << std::setw(Headerlength) << std::hex << message.size();
    if ((!headerStream || headerStream.str().size() != Headerlength) && this->OnError)
    {
      // something went wrong, inform the caller.
      this->OnError(asio::error::invalid_argument);
      return;
    }
    const auto outboundHeader = headerStream.str();
    vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Post hdr=\'%s\' data=\'%s\'", outboundHeader.c_str(),
      message.c_str());
#ifndef NDEBUG
    auto netPacket = FromBytes(message);
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(),
      "Post vtkAsioInternalsNetworkPacket"
        << "Secret:" << netPacket.Secret << "|Destination:" << netPacket.Destination
        << "|Channel:" << netPacket.Channel << "|ResponseRequired:" << netPacket.ResponseRequired
        << "|Payload" << netPacket.Payload->ToString());
#endif
    const bool idle = this->OutboundQueue.empty();
    this->OutboundQueue.emplace_back(outboundHeader + message);
    if (idle)
    {
      this->AsyncWrite();
    }
  });
}

//-------------------------------------------------------------------
void vtkAsioInternalsConnection::AsyncWrite()
{
  const std::string& outboundData = this->OutboundQueue.front();
  vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Write \'%s\'", outboundData.c_str());
  asio::async_write(this->Socket, asio::buffer(outboundData),
    [this, self = shared_from_this()](const error_code& ec, std::size_t /*length*/) {
      const bool notReady = ec == asio::error::broken_pipe || ec == asio::error::not_connected;
      if (notReady)
      {
        // connection has not yet been made, keep retrying until exhausted.
        vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Failed to write message. Retry in 5 ms ..");
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        this->AsyncWrite();
      }
      else if (ec == asio::error::operation_aborted)
      {
        vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(),
          << "Socket write has been aborted by execution context.");
      }
      else if (!ec)
      {
        if (this->OnWrite != nullptr)
        {
          this->OnWrite(ec);
        }
        this->OutboundQueue.pop_front();
        if (!this->OutboundQueue.empty())
        {
          this->AsyncWrite();
        }
      }
      else
      {
        vtkLogF(ERROR, "Failed to write message. Closing socket. Error (%d) %s", ec.value(),
          ec.message().c_str());
        this->PostClose();
      }
    });
}
