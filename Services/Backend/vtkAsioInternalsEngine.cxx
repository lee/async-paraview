/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsEngine.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioInternalsEngine.h"
#include "vtkServicesAsioLogVerbosity.h"

//----------------------------------------------------------------------------
vtkAsioInternalsEngine::vtkAsioInternalsEngine()
{
  this->IOContext.reset(new io::io_context());
}

//----------------------------------------------------------------------------
vtkAsioInternalsEngine::~vtkAsioInternalsEngine()
{
  // cancel and destroy acceptor as we're about to join the runner pretty soon.
  this->Acceptor.reset();
  for (const auto& conn : this->Connections)
  {
    if (conn != nullptr)
    {
      conn->PostClose();
    }
  }
  this->Runner->join();

  // ideally, in built-in session, the number of sends from endpoints should match number of
  // recvs to the services and vice-versa.
  // in a remote-session, refer to individual engine's log.
  auto& epComm =
    this->Communicators[this->GetSideAsInteger(VTKAsioEngineSideType::VTKAEST_EndpointSide)];
  auto& servComm =
    this->Communicators[this->GetSideAsInteger(VTKAsioEngineSideType::VTKAEST_ServiceSide)];
  vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "engine: %p |endpoints| sends: %zu, recvs: %zu", this,
    epComm.SendCounter, epComm.RecvCounter);
  vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "engine: %p |services | sends: %zu, recvs: %zu", this,
    servComm.SendCounter, servComm.RecvCounter);
}

//----------------------------------------------------------------------------
int vtkAsioInternalsEngine::GetSideAsInteger(VTKAsioEngineSideType side)
{
  static_assert(std::is_same<std::underlying_type<VTKAsioEngineSideType>::type, int>::value,
    "VTKAsioEngineSideType must have integer base type.");
  return static_cast<int>(side);
}

//----------------------------------------------------------------------------
void vtkAsioInternalsEngine::Start()
{
  // can only restart by destroying this instance of vtkAsioInternalsEngine
  if (this->Runner == nullptr)
  {
    this->Runner.reset(new std::thread([& ioCtx = (*this->IOContext)]() {
      vtkLogger::SetThreadName("io_context");
      ioCtx.run();
    }));
  }
}

//----------------------------------------------------------------------------
void vtkAsioInternalsEngine::Stop()
{
  this->IOContext->stop();
}

//----------------------------------------------------------------------------
tcp::resolver::results_type vtkAsioInternalsEngine::Resolve(
  std::string host, std::string port, tcp::resolver::flags resolve_flags)
{
  tcp::resolver resolver(*this->IOContext);
  error_code ec;
  auto endpoints = resolver.resolve(host, port, resolve_flags, ec);
  if (ec)
  {
    vtkLogF(ERROR, "Failed to resolve %s:%s. Error (%d) %s", host.c_str(), port.c_str(), ec.value(),
      ec.message().c_str());
    std::terminate();
  }
  return endpoints;
}

//----------------------------------------------------------------------------
void vtkAsioInternalsEngine::HandleAccept(const error_code& ec, int side)
{
  auto& conn = this->Connections[side];
  if (!ec)
  {
    auto remoteEp = conn->GetSocket().remote_endpoint();
    vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "Accepted connection from %s",
      remoteEp.address().to_string().c_str());
    conn->OnRead = [this, side](auto&& PH1, auto&& PH2) { HandleRead(PH1, PH2, side); };
    conn->OnWrite = [this, side](auto&& PH1) { HandleWrite(PH1, side); };
    conn->AsyncRead();
  }
  else if (ec == asio::error::operation_aborted)
  {
    vtkLogF(WARNING, "Accept aborted - %s. Code (%d).", ec.message().c_str(), ec.value());
    conn->GetSocket().close();
  }
  else
  {
    vtkLogF(
      ERROR, "Failed to accept connections. Error (%d). %s", ec.value(), ec.message().c_str());
    conn->GetSocket().close();
  }
}

//----------------------------------------------------------------------------
void vtkAsioInternalsEngine::HandleConnect(const error_code& ec, tcp::endpoint /*ep*/, int side)
{
  if (!ec)
  {
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Connected!");
    auto& conn = this->Connections[side];
    conn->OnRead = [this, side](auto&& PH1, auto&& PH2) { HandleRead(PH1, PH2, side); };
    conn->OnWrite = [this, side](auto&& PH1) { HandleWrite(PH1, side); };
    conn->AsyncRead();
  }
  else
  {
    vtkLogF(ERROR, "Connection failed! Error (%d). %s", ec.value(), ec.message().c_str());
    // maybe retry?
    std::terminate();
  }
}

//----------------------------------------------------------------------------
void vtkAsioInternalsEngine::HandleWrite(const error_code& ec, int side)
{
  if (ec)
  {
    return;
  }
  auto& comm = this->Communicators[side];
  // handle overflow in SendCounter
  if (comm.SendCounter == std::numeric_limits<std::size_t>::max())
  {
    comm.SendCounter = 0;
  }
  comm.SendCounter++;
}

//----------------------------------------------------------------------------
void vtkAsioInternalsEngine::HandleRead(const error_code& ec, const std::string& message, int side)
{
  if (ec)
  {
    return;
  }
  auto& comm = this->Communicators[side];
  auto& conn = this->Connections[side];
  // handle overflow in RecvCounter
  if (comm.RecvCounter == std::numeric_limits<std::size_t>::max())
  {
    comm.RecvCounter = 0;
  }
  comm.RecvCounter++;
  // push message into recvSubject.
  comm.Receiver.get_subscriber().on_next(FromBytes(message));
  // schedule a non-blocking read.
  conn->AsyncRead();
}
