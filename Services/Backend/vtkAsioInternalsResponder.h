/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsResponder.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkAsioInternalsResponder
 * @brief Responds to specific vtkAsioService requests.
 */

#ifndef vtkAsioInternalsResponder_h
#define vtkAsioInternalsResponder_h

#include "vtkAsioInternalsConnection.h"
#include "vtkPacket.h"
#include "vtkService.h"

class vtkAsioService;

class vtkAsioInternalsResponder
{
public:
  using ChannelSubscriptionMapT = std::map<std::string, rxcpp::composite_subscription>;
  using ChannelObservableT = rxcpp::observable<std::tuple<vtkPacket, bool>>;

  /**
   * Process requests for registered services.
   * Responds with true if service has been registered with engine on server, false otherwise.
   */
  static void ServiceExists(vtkAsioService* service, vtkAsioInternalsConnection* connection);

  /**
   * Dispatches message to the service.
   */
  static vtkServiceReceiver SendAMessage(vtkAsioService* service, const vtkPacket& message);

  /**
   * Dispatches request to the service with a callback. The callback responds with a reply.
   * Response contains a `vtkPacket`
   */
  static vtkServiceReceiver SendRequest(vtkAsioService* service, const vtkPacket& request,
    const std::string& secret, vtkAsioInternalsConnection* connection);

  /**
   * Extracts the value for `vtkAsioService::ChannelSubscribeKey` from this request.
   */
  static std::string GetChannelSubscribeTarget(const vtkPacket& request);

  /**
   * Processes a request to subscribe a channel on service.
   * Response is true if subscription was made, false otherwise.
   */
  static void ChannelSubscribeRequest(vtkAsioService* service, const vtkPacket& request,
    const std::string& secret, vtkAsioInternalsConnection* connection,
    ChannelSubscriptionMapT& subscriptions);

  /**
   * Processes a request to unsubscribe a channel on service.
   * Response is true if subscription was made, false otherwise.
   */
  static void ChannelUnsubscribeRequest(vtkAsioService* service, const vtkPacket& request,
    const std::string& secret, vtkAsioInternalsConnection* connection,
    ChannelSubscriptionMapT& subscriptions);

  /**
   * Respond to a client.
   * @param reply a response
   * @param secret a secret code provided by the requester when initiating a request.
   * @param clientConnection our connection to the requester on the client.
   * @param channel name of a channel on this service
   */
  static void Respond(const vtkPacket& reply, const std::string& secret,
    vtkAsioInternalsConnection* clientConnection, const std::string& channel = "");
};

#endif // vtkAsioInternalsResponder_h

// VTK-HeaderTest-Exclude: vtkAsioInternalsResponder.h
