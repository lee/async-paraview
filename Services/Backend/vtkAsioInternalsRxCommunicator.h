/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsRxCommunicator.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkAsioInternalsRxCommunicator_h
#define vtkAsioInternalsRxCommunicator_h

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

#include "vtkAsioInternalsNetworkPacket.h"

/**
 * The communicator helps enqueue incoming and track outgoing messages.
 *
 * Services and endpoints: enqueue outgoing messages with `vtkAsioInternalsConnection::PostWrite()`
 * Engine: invokes asio::async_write and bumps `SendCounter` after data send is complete.
 *
 * Engine: enqueues incoming messages from socket into `Receiver` and bumps `RecvCounter`
 * Services and endpoints: observe incoming messages from `Receiver`
 *
 * @warning The counters are left non-atomic for performance. These counters are bumped
 *          by completion handlers. Counter reads are valid only within
 *          the completion handlers of asio or after io_context has stopped.
 */
struct vtkAsioInternalsRxCommunicator
{
  using RecvSubjectT = rxcpp::subjects::subject<vtkAsioInternalsNetworkPacket>;
  RecvSubjectT Receiver;
  std::size_t RecvCounter = 0;
  std::size_t SendCounter = 0;
};

#endif // vtkAsioInternalsRxCommunicator_h

// VTK-HeaderTest-Exclude: vtkAsioInternalsRxCommunicator.h
