/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsResponder.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioInternalsResponder.h"
#include "vtkAsioInternalsNetworkPacket.h"
#include "vtkAsioService.h"
#include "vtkServicesAsioLogVerbosity.h"

//----------------------------------------------------------------------------
void vtkAsioInternalsResponder::Respond(const vtkPacket& reply, const std::string& secret,
  vtkAsioInternalsConnection* clientConnection, const std::string& channel)
{
  vtkAsioInternalsNetworkPacket netPacket;
  netPacket.Secret = secret;
  netPacket.Channel = channel;
  netPacket.Destination = "null";
  netPacket.ResponseRequired = true;
  netPacket.Payload = std::make_shared<vtkPacket>(reply);
  clientConnection->PostWrite(ToBytes(netPacket));
};

//----------------------------------------------------------------------------
void vtkAsioInternalsResponder::ServiceExists(
  vtkAsioService* service, vtkAsioInternalsConnection* connection)
{
  const std::string secret = vtkAsioService::ComposeServiceExistsResponseCode(service->GetName());
  vtkAsioInternalsResponder::Respond(
    vtkNJson{ { vtkAsioService::ServiceExistsKey, true } }, secret, connection);
}

//----------------------------------------------------------------------------
vtkServiceReceiver vtkAsioInternalsResponder::SendAMessage(
  vtkAsioService* service, const vtkPacket& message)
{
  return vtkServiceReceiver(service, message, nullptr);
}

//----------------------------------------------------------------------------
vtkServiceReceiver vtkAsioInternalsResponder::SendRequest(vtkAsioService* service,
  const vtkPacket& request, const std::string& secret, vtkAsioInternalsConnection* connection)
{
  auto callback = [secret, connection](const vtkPacket& reply) {
    vtkAsioInternalsResponder::Respond(reply, secret, connection);
  };
  return vtkServiceReceiver(service, request, callback);
}

//----------------------------------------------------------------------------
std::string vtkAsioInternalsResponder::GetChannelSubscribeTarget(const vtkPacket& request)
{
  const auto& json = request.GetJSON();
  return json.at(vtkAsioService::ChannelSubscribeKey).get<std::string>();
}

//----------------------------------------------------------------------------
void vtkAsioInternalsResponder::ChannelSubscribeRequest(vtkAsioService* service,
  const vtkPacket& request, const std::string& secret, vtkAsioInternalsConnection* connection,
  ChannelSubscriptionMapT& subscriptions)
{
  const std::string channelName = GetChannelSubscribeTarget(request);
  vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "subscribing %s", channelName.c_str());
  // for new emissions in the channel, respond with that cached/uncached content.
  auto subscription = service->MakeChannelSubscription(channelName);
  // response is true if we made a subscription, false otherwise
  const std::string responseKey =
    vtkAsioService::ComposeChannelSubscribeResponseCode(service->GetName(), channelName);
  if (!subscriptions.emplace(channelName, subscription).second)
  {
    subscription.unsubscribe();
    vtkAsioInternalsResponder::Respond(vtkNJson{ { responseKey, false } }, secret, connection);
  }
  else
  {
    vtkAsioInternalsResponder::Respond(vtkNJson{ { responseKey, true } }, secret, connection);
  }
}

//----------------------------------------------------------------------------
void vtkAsioInternalsResponder::ChannelUnsubscribeRequest(vtkAsioService* service,
  const vtkPacket& request, const std::string& secret, vtkAsioInternalsConnection* connection,
  ChannelSubscriptionMapT& subscriptions)
{
  const auto& json = request.GetJSON();
  const std::string channelName = json[vtkAsioService::ChannelUnsubscribeKey].get<std::string>();
  vtkVLogF(VTKSERVICESASIO_LOG_VERBOSITY(), "unsubscribing %s", channelName.c_str());
  // erases subscription.
  auto iter = subscriptions.find(channelName);
  const std::string responseKey =
    vtkAsioService::ComposeChannelUnsubscribeResponseCode(service->GetName(), channelName);
  if (iter != subscriptions.end())
  {
    iter->second.unsubscribe();
    subscriptions.erase(iter);
    vtkAsioInternalsResponder::Respond(vtkNJson{ { responseKey, true } }, secret, connection);
  }
  else
  {
    vtkLogF(ERROR, "A subscription for \'%s\' does not exist on service \'%s\'",
      channelName.c_str(), service->GetName().c_str());
    vtkAsioInternalsResponder::Respond(vtkNJson{ { responseKey, false } }, secret, connection);
  }
}
