/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioService.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioService.h"

#include "vtkAsioInternalsResponder.h"
#include "vtkAsioInternalsRxCommunicator.h"
#include "vtkAsioInternalsScheduler.h"
#include "vtkAsioServicesEngine.h"
#include "vtkMultiProcessController.h"
#include "vtkObjectFactory.h"
#include "vtkServicesAsioLogVerbosity.h"

#include <thread>

class vtkAsioService::vtkInternals
  : public std::enable_shared_from_this<vtkAsioService::vtkInternals>
{
public:
  std::map<std::string, rxcpp::composite_subscription> ChannelSubscriptions;

  //----------------------------------------------------------------------------
  explicit vtkInternals(
    const std::string& serviceName, vtkMultiProcessController* controller = nullptr)
    : Worker(serviceName, controller)
  {
  }

  //----------------------------------------------------------------------------
  ~vtkInternals()
  {
    // clean up unsubscribed subscriptions (if any)
    for (auto& item : this->ChannelSubscriptions)
    {
      auto& subscription = item.second;
      if (subscription.is_subscribed())
      {
        subscription.unsubscribe();
      }
    }
  }

  //----------------------------------------------------------------------------
  // reuse the worker we created during initialize.
  rxcpp::observe_on_one_worker GetCoordination()
  {
    return rxcpp::observe_on_one_worker(this->Worker.GetScheduler());
  }

private:
  vtkAsioInternalsScheduler Worker;
};

const std::string vtkAsioService::ServiceExistsKey = "service-exists";
const std::string vtkAsioService::ChannelSubscribeKey = "channel-subscribe";
const std::string vtkAsioService::ChannelUnsubscribeKey = "channel-unsubscribe";

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkAsioService);

//----------------------------------------------------------------------------
vtkAsioService::vtkAsioService() = default;

//----------------------------------------------------------------------------
vtkAsioService::~vtkAsioService() = default;

//----------------------------------------------------------------------------
void vtkAsioService::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
rxcpp::observe_on_one_worker vtkAsioService::GetRunLoopScheduler() const
{
  if (this->Internals == nullptr)
  {
    vtkLog(ERROR, << "Forgot to initialize the service?");
  }
  auto& internals = *this->Internals;
  return internals.GetCoordination();
}

//----------------------------------------------------------------------------
rxcpp::composite_subscription vtkAsioService::MakeChannelSubscription(
  const std::string& channelName)
{
  auto engine = vtkAsioServicesEngine::SafeDownCast(this->GetEngine());
  auto connection = engine->GetConnection(this->TAG);
  return this->GetChannelObservable(channelName)
    .subscribe(rxcpp::util::apply_to([=](const vtkPacket& content, bool cached) {
      vtkVLogF(
        VTKSERVICESASIO_LOG_VERBOSITY(), "publishing %s (cached=%d)", channelName.c_str(), cached);
      // secret is literally "cached" or "uncached".
      vtkAsioInternalsResponder::Respond(
        content, cached ? "cached" : "uncached", connection, channelName);
    }));
}

//----------------------------------------------------------------------------
void vtkAsioService::InitializeInternal()
{
  this->Internals =
    std::make_shared<vtkInternals>(this->GetName(), this->GetEngine()->GetController());
}

//----------------------------------------------------------------------------
bool vtkAsioService::StartInternal()
{
  auto engine = vtkAsioServicesEngine::SafeDownCast(this->GetEngine());
  if (engine == nullptr)
  {
    vtkLogF(ERROR, "Engine not of correct type! Expected instance of vtkAsioServicesEngine. Got %s",
      vtkLogIdentifier(this->GetEngine()));
    return false;
  }
  if (this->GetName().empty())
  {
    vtkLogF(ERROR, "Service does not have a name! Forgot to initialize service?");
    return false;
  }
  // in Asymmetric mode only the root process starts the engine
  if (!engine->GetSymmetricMPIMode() && engine->GetController()->GetLocalProcessId() > 0)
  {
    return true;
  }
  auto& receiver = engine->GetCommunicator(this->TAG)->Receiver;
  // these subscribers execute in the io_context thread. const ref should be fine.
  this->Subscription =
    receiver
      .get_observable()
      // single rxcpp pipeline here can receive messages, respond and publish content to a channel.
      .filter([myname = this->GetName()](const vtkAsioInternalsNetworkPacket& netPacket) {
        return myname == netPacket.Destination;
      })
      // keep internals alive as there is a potential race condition when shutdown destroys
      // internals and the subscriber dereferences internals.
      .subscribe([this, engine, keepAlive = this->Internals->shared_from_this()](
                   const vtkAsioInternalsNetworkPacket& netPacket) {
        const auto& secret = netPacket.Secret;
        if (!netPacket.ResponseRequired)
        {
          // one-way message
          const auto& payload = (*netPacket.Payload);
          auto receiver = vtkAsioInternalsResponder::SendAMessage(this, payload);
          this->Dispatch(std::move(receiver));
        }
        else
        {
          const auto& request = (*netPacket.Payload);
          const auto connection = engine->GetConnection(this->TAG);
          // what kind of response? refer to my keys.
          auto& json = request.GetJSON();
          // look for service exists request. is it registered with engine?
          if (json.contains(ServiceExistsKey) && this->GetEngine()->GetService(this->GetName()))
          {
            vtkAsioInternalsResponder::ServiceExists(this, connection);
          }
          // look for special channel subscribe request.
          else if (json.contains(ChannelSubscribeKey))
          {
            vtkAsioInternalsResponder::ChannelSubscribeRequest(
              this, request, secret, connection, keepAlive->ChannelSubscriptions);
          }
          // look for special channel unsubscribe request.
          else if (json.contains(ChannelUnsubscribeKey))
          {
            vtkAsioInternalsResponder::ChannelUnsubscribeRequest(
              this, request, secret, connection, keepAlive->ChannelSubscriptions);
          }
          // handle special request keys that are added in future here.
          // else if (json.contains(SomethingKey)) ..
          else
          {
            auto receiver =
              vtkAsioInternalsResponder::SendRequest(this, request, secret, connection);
            this->Dispatch(std::move(receiver));
          }
        }
      });
  return true;
}

//----------------------------------------------------------------------------
void vtkAsioService::ShutdownInternal()
{
  this->Subscription.unsubscribe();
  this->Internals = nullptr;
}
