/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsEngineSideTypes.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @class vtkAsioInternalsEngineSideTypes
 * @brief Used to associate communicators and connections with either side in
 *        a client-server or builtin session.
 */

#ifndef vtkAsioInternalsEngineSideTypes_h
#define vtkAsioInternalsEngineSideTypes_h

enum class VTKAsioEngineSideType : int
{
  // prefixed to avoid future collisions with decls from system headers
  VTKAEST_EndpointSide,
  VTKAEST_ServiceSide
};

#endif // vtkAsioInternalsEngineSideTypes_h

// VTK-HeaderTest-Exclude: vtkAsioInternalsNetworkPacket.h
