/*=========================================================================

  Program:   ParaView
  Module:    TestNetPacket.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioInternalsNetworkPacket.h"
#include <numeric>

int TestNetPacket(int, char*[])
{
  std::map<std::string, vtkSmartPointer<vtkObject>> content;

  // insert placeholder payloads.
  auto frame = vtk::TakeSmartPointer(vtkUnsignedCharArray::New());
  frame->SetNumberOfComponents(3);
  frame->SetNumberOfTuples(10);
  std::iota(frame->Begin(), frame->End(), 0);
  content.insert({ "frame", frame });

  auto geometry = vtk::TakeSmartPointer(vtkUnsignedCharArray::New());
  geometry->SetNumberOfComponents(1);
  geometry->SetNumberOfValues(100);
  std::iota(geometry->Begin(), geometry->End(), 0);
  content.insert({ "geometry", geometry });

  vtkAsioInternalsNetworkPacket netPacket{ "secret-stuff", "data-service", "data-stream", true,
    std::make_shared<vtkPacket>(
      vtkNJson{ { "int32", 1021 }, { "c-string", "hello-world" },
        { "std-string", std::string("hello world!") }, { "float", 102.4F }, { "double", 10.24 },
        { "bool", false }, { "type", "echo" } },
      content) };
  auto bytes = ToBytes(netPacket);
  // for (auto& v : bytes)
  // {
  //   std::cout << ": 0x" << std::hex << std::setw(2) << std::setfill('0') << int(v) << ' ';
  // }
  // std::cout << '\n';
  return FromBytes(bytes) != netPacket;
}
