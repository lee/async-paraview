/*=========================================================================

  Program:   Visualization Toolkit
  Module:    TestBigData.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <chrono>
#include <new>
#include <vtkChannelSubscription.h>
#include <vtkLogger.h>
#include <vtkNew.h>
#include <vtkPacket.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>
#include <vtkSmartPointer.h>
#include <vtkUnsignedCharArray.h>

#define VERIFY(x)                                                                                  \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Failed " #x);                                                                  \
    std::terminate();                                                                              \
  }

namespace
{

vtkIdType BUFFER_SIZE_BYTES = 4294967296; // 4 giga bytes.

vtkUnsignedCharArray* MakeNewBuffer(vtkIdType numValues)
{
  vtkLogF(INFO, "Using a buffer of size %lld bytes ", numValues);
  auto buf = vtkUnsignedCharArray::New();
  buf->SetNumberOfComponents(1);
  if (!buf->SetNumberOfValues(numValues))
  {
    vtkLog(WARNING, "Failed to allocate " << numValues << " bytes");
    return buf;
  }
  buf->FillValue(2);
  return buf;
}

// CI may fail due to the machines being shared among many jobs. Let's reduce
// the size of the test buffer in this case.
void SetupBufferSize()
{
  if (const char* env_p = std::getenv("GITLAB_CI"))
  {
    vtkLogF(INFO, "Running in Gitlab-CI\n Reducing buffer size...");
    BUFFER_SIZE_BYTES = 1073741824; // 1 giga byte.
  }
}
}

int TestBigData(int, char*[])
{
  SetupBufferSize();
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());

  auto service = engine->CreateService("service1");
  service->GetRequestObservable().take(1).subscribe([](const vtkServiceReceiver& receiver) {
    /* every time we receive a request, we respond with a new buffer */
    const auto& packet = receiver.GetPacket();
    const int requestId = packet.GetJSON().at("requestId").get<int>();

    std::map<std::string, vtkSmartPointer<vtkObject>> payload;
    auto buf = vtk::TakeSmartPointer(MakeNewBuffer(BUFFER_SIZE_BYTES));
    payload.insert({ "buffer", buf });

    const vtkIdType numBytes = buf->GetNumberOfValues();
    vtkLog(INFO, << "Sending requestId=" << requestId << ", numBytes=" << numBytes);

    receiver.Respond(vtkPacket(packet.GetJSON(), payload));
  });

  VERIFY(service->Start());

  auto endpoint = engine->CreateServiceEndpoint("service1");
  VERIFY(engine->Await(endpoint->Connect()));
  vtkLogScopeF(INFO, "TestBigData");

  auto response = engine->Await(endpoint->SendRequest(vtkPacket(vtkNJson{ { "requestId", 1 } })));
  const int requestId = response.GetJSON().at("requestId").get<int>();
  auto* buf = vtkUnsignedCharArray::SafeDownCast(response.GetPayload().at("buffer"));

  const vtkIdType numBytes = buf->GetNumberOfValues();
  vtkLog(INFO, << "Received requestId=" << requestId << ", numBytes=" << numBytes);

  engine->Finalize();
  return EXIT_SUCCESS;
}
