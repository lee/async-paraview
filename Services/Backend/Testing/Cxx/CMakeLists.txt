vtk_add_test_cxx(vtkServicesBackendCxxTests tests
  NO_VALID
  TestBasicConnect.cxx
  TestBigData.cxx
  TestCachedChannel.cxx
  TestNetPacket.cxx
  TestSendMessage.cxx
  TestSendRequest.cxx
  TestSendRequest2.cxx
  TestService2ServiceDataStream.cxx
  TestServiceSameWorkerThread.cxx
  TestUncachedChannel.cxx
  TestEndpointUnsubscribe.cxx
  )
vtk_test_cxx_executable(vtkServicesBackendCxxTests tests)
