/*=========================================================================

  Program:   Visualization Toolkit
  Module:    TestService2ServiceDataStream.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include <vtkChannelSubscription.h>
#include <vtkLogger.h>
#include <vtkNew.h>
#include <vtkPacket.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

int TestService2ServiceDataStream(int /*argc*/, char* /*argv*/[])
{
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());

  // Setup a service for all the IO.
  auto ioService = engine->CreateService("ioService");
  ioService->Start();

  // Setup a service for all the data-processing.
  auto dpService = engine->CreateService("dpService");
  dpService->Start();

  auto ioEndpoint = engine->CreateServiceEndpoint("ioService");
  if (!engine->Await(ioEndpoint->Connect()))
  {
    vtkLogF(ERROR, "failed to connect to ioService");
    return EXIT_FAILURE;
  }

  auto dpEndpoint = engine->CreateServiceEndpoint("dpService");
  if (!engine->Await(dpEndpoint->Connect()))
  {
    vtkLogF(ERROR, "failed to connect to dpService");
    return EXIT_FAILURE;
  }

  // Now, we want dpService to have mechanism to receive data from ioService.
  std::atomic<bool> gotData{ false };
  vtkPacket result;
  auto dsSubscription = dpService->Subscribe("data-stream", "ioService");
  engine->Await(dsSubscription->IsReady());
  dsSubscription->GetObservable().subscribe([&result, &gotData](vtkPacket const& packet) {
    vtkLogF(INFO, "dpService got packet");
    result = packet;
    gotData = true;
  });

  ioService->EnableChannelCache("data-stream");
  ioService->GetRequestObservable().subscribe([](vtkServiceReceiver const& receiver) {
    receiver.Publish("data-stream", receiver.GetPacket());
  });

  const vtkPacket data{ { "publish", 12 } };
  ioEndpoint->SendAMessage(data);

  engine->ProcessEventsFor([&]() -> bool { return gotData; }, std::chrono::seconds(1));
  vtkLogIfF(ERROR, !gotData, "failed to receive data on dpService.");

  engine->Finalize();
  vtkLogIfF(ERROR, gotData && (result != data), "service received corrupt packet");
  return result == data ? EXIT_SUCCESS : EXIT_FAILURE;
}
