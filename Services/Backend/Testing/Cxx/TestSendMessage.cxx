/*=========================================================================

  Program:   ParaView
  Module:    TestSendMessage.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <atomic>
#include <chrono>

#include <vtkNew.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#define CHECK_ERROR(header, status)                                                                \
  if (!status)                                                                                     \
  {                                                                                                \
    vtkLog(ERROR, << header << ". Error occurred.");                                               \
    engine->Finalize();                                                                            \
    return 1;                                                                                      \
  }

int TestSendMessage(int /*argc*/, char* /*argv*/[])
{
  bool success = false;
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());
  auto ds = engine->CreateService("services.data");
  success = ds->Start();
  auto rs = engine->CreateService("services.render");
  success &= rs->Start();
  auto dsep = engine->CreateServiceEndpoint("services.data");
  auto rsep = engine->CreateServiceEndpoint("services.render");
  CHECK_ERROR("Service startup", success)

  auto o1 = dsep->Connect();
  auto o2 = rsep->Connect();
  auto connected = o1.combine_latest(o2)
                     .observe_on(engine->GetCoordination())
                     .map([](const std::tuple<bool, bool>& status) {
                       return std::get<0>(status) && std::get<1>(status);
                     });
  success &= engine->Await(connected.as_dynamic());
  CHECK_ERROR("Connect endpoints with services", success)

  vtkPacket outMsg1(vtkPacket({ { "int32", 1021 }, { "c-string", "hello-world" },
    { "std-string", std::string("hello world!") }, { "float", 102.4F }, { "double", 10.24 },
    { "bool", false }, { "type", "echo" } }));
  // these booleans are set from service threads and read from main thread.
  std::atomic_bool received{ false };
  std::atomic_bool matched{ false };
  auto subscription = ds->GetRequestObservable(true)
                        .map([](auto receiver) { return receiver.GetPacket(); })
                        .observe_on(ds->GetRunLoopScheduler())
                        .subscribe([&received, &matched, outMsg1](auto packet) {
                          received = true;
                          matched = (outMsg1 == packet);
                        });
  dsep->SendAMessage(outMsg1);
  // wait until the message is received on service thread.
  // can go on forever. testing infrastructure should catch timeouts.
  do
  {
    engine->ProcessEvents();
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  } while (!received);

  success &= matched;
  subscription.unsubscribe();
  CHECK_ERROR("One-way communication between endpoint and service", success)

  engine->Finalize();
  return 0;
}
