/*=========================================================================

  Program:   ParaView
  Module:    TestSendRequest2.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <algorithm>
#include <chrono>
#include <random>
#include <string>

#include <vtkNew.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#define CHECK_ERROR(header, status)                                                                \
  if (!status)                                                                                     \
  {                                                                                                \
    vtkLog(ERROR, << header << ". Error occurred.");                                               \
    for (auto& s : subscriptions)                                                                  \
    {                                                                                              \
      s.unsubscribe();                                                                             \
    }                                                                                              \
    engine->Finalize();                                                                            \
    return 1;                                                                                      \
  }

// Stress test send request
int TestSendRequest2(int /*argc*/, char* /*argv*/[])
{
  constexpr int NUM_SERVICES = 6;
  constexpr int NUM_MESSAGES = 100;
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());
  std::vector<vtkSmartPointer<vtkService>> services;
  std::vector<rxcpp::composite_subscription> subscriptions;
  for (int i = 0; i < NUM_SERVICES; ++i)
  {
    auto service = engine->CreateService("services.id" + std::to_string(i));
    // setup service handler. this can be done before the service is started.
    subscriptions.push_back(
      service->GetRequestObservable(true)
        .filter([](const vtkServiceReceiver& receiver) {
          try
          {
            return receiver.GetPacket().GetJSON().at("type").get<std::string>() == "echo";
          }
          catch (std::out_of_range&)
          {
            return true;
          }
        })
        .observe_on(service->GetRunLoopScheduler())
        .subscribe([service](const vtkServiceReceiver& receiver) {
          vtkLogScopeF(INFO, "echoing: %s", receiver.GetPacket().ToString().c_str());
          receiver.Respond(receiver.GetPacket());
        }));
    services.push_back(service);
  }
  bool success = true;
  for (const auto& service : services)
  {
    success &= service->Start();
  }
  CHECK_ERROR("Service startup", success)

  std::vector<vtkSmartPointer<vtkServiceEndpoint>> endpoints;
  for (const auto& service : services)
  {
    auto endpoint = engine->CreateServiceEndpoint(service->GetName());
    endpoints.push_back(endpoint);
  }
  std::queue<rxcpp::observable<bool>> connectedObservables;
  for (const auto& endpoint : endpoints)
  {
    connectedObservables.push(endpoint->Connect());
  }
  while (!connectedObservables.empty())
  {
    auto connected = connectedObservables.front();
    success &= engine->Await(connected.as_dynamic());
    connectedObservables.pop();
  }
  CHECK_ERROR("Connect endpoints with services", success)

  std::deque<vtkPacket> outTestMsgs;
  std::deque<rxcpp::observable<vtkPacket>> recvTestQueue;
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<int> destinationIds(0, NUM_SERVICES - 1);
  for (int i = 0; i < NUM_MESSAGES && success; ++i)
  {
    outTestMsgs.push_back(vtkNJson({ { "type", "echo" }, { "tag", i } }));
    auto endpoint = endpoints[destinationIds(rng)];
    recvTestQueue.push_back(endpoint->SendRequest(outTestMsgs.back()));
  }
  while (!outTestMsgs.empty() && success)
  {
    success &= engine->Await(recvTestQueue.front()) == outTestMsgs.front();
    recvTestQueue.pop_front();
    outTestMsgs.pop_front();
  }
  CHECK_ERROR("Two-way communication between endpoint and service", success)
  for (auto& s : subscriptions)
  {
    s.unsubscribe();
  }
  engine->Finalize();
  return 0;
}
