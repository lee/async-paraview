/*=========================================================================

  Program:   ParaView
  Module:    TestServiceSameWorkerThread.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <atomic>
#include <chrono>
#include <sstream>
#include <string>
#include <thread>

#include <vtkNew.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#define CHECK_ERROR(header, status)                                                                \
  if (!status)                                                                                     \
  {                                                                                                \
    vtkLog(ERROR, << header << ". Error occurred.");                                               \
    engine->Finalize();                                                                            \
    return 1;                                                                                      \
  }

namespace
{
std::string GetCurrentThreadID()
{
  std::stringstream tidStream;
  tidStream << std::this_thread::get_id();
  return tidStream.str();
}

std::string GetServiceThreadID(vtkService* service)
{
  std::string threadID;
  rxcpp::observable<>::just<bool>(true)
    .observe_on(service->GetRunLoopScheduler())
    .as_blocking()
    .subscribe([&threadID](bool) { threadID = ::GetCurrentThreadID(); });
  return threadID;
}
}

int TestServiceSameWorkerThread(int /*argc*/, char* /*argv*/[])
{
  bool success = false;
  constexpr int NUM_SUBSCRIPTIONS = 200;
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());
  auto ds = engine->CreateService("services.data");
  success = ds->Start();
  auto rs = engine->CreateService("services.render");
  success &= rs->Start();
  auto dsep = engine->CreateServiceEndpoint("services.data");
  auto rsep = engine->CreateServiceEndpoint("services.render");
  CHECK_ERROR("Service startup", success)

  auto o1 = dsep->Connect();
  auto o2 = rsep->Connect();
  auto connected = o1.combine_latest(o2)
                     .observe_on(engine->GetCoordination())
                     .map([](const std::tuple<bool, bool>& status) {
                       return std::get<0>(status) && std::get<1>(status);
                     });
  success &= engine->Await(connected.as_dynamic());
  CHECK_ERROR("Connect endpoints with services", success)

  std::string dsThreadID = ::GetServiceThreadID(ds);
  std::string rsThreadID = ::GetServiceThreadID(rs);
  // make a number of subscriptions. all subscribers SHOULD run on same thread.
  std::vector<rxcpp::composite_subscription> dsSubscriptions;
  std::vector<rxcpp::composite_subscription> rsSubscriptions;
  // these booleans are set from service threads and read from main thread.
  std::atomic<bool> dsSuccess{ true };
  std::atomic<bool> rsSuccess{ true };
  // used to indicate completion of final subscription.
  std::atomic<bool> dsFinished{ false };
  std::atomic<bool> rsFinished{ false };
  for (int i = 0; i < NUM_SUBSCRIPTIONS; ++i)
  {
    dsSubscriptions.push_back(
      rxcpp::observable<>::just<bool>(true)
        .observe_on(ds->GetRunLoopScheduler())
        .subscribe([dsThreadID, &dsSuccess, i, &dsFinished, NUM_SUBSCRIPTIONS](auto) {
          vtkLog(INFO, << "DS tid=" << ::GetCurrentThreadID());
          dsSuccess = dsSuccess && (::GetCurrentThreadID() == dsThreadID);
          if (i == NUM_SUBSCRIPTIONS - 1)
          {
            dsFinished = true;
          }
        }));
    rsSubscriptions.push_back(
      rxcpp::observable<>::just<bool>(true)
        .observe_on(rs->GetRunLoopScheduler())
        .subscribe([rsThreadID, &rsSuccess, i, &rsFinished, NUM_SUBSCRIPTIONS](auto) {
          vtkLog(INFO, << "RS tid=" << ::GetCurrentThreadID());
          rsSuccess = rsSuccess && (::GetCurrentThreadID() == rsThreadID);
          if (i == NUM_SUBSCRIPTIONS - 1)
          {
            rsFinished = true;
          }
        }));
  }
  // wait until all those subscribers execute.
  // can go on forever. testing infrastructure should catch timeouts.
  do
  {
    engine->ProcessEvents();
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  } while (!rsFinished && !dsFinished);

  success = dsSuccess && rsSuccess;
  for (int i = 0; i < NUM_SUBSCRIPTIONS; ++i)
  {
    dsSubscriptions[i].unsubscribe();
    rsSubscriptions[i].unsubscribe();
  }
  CHECK_ERROR("Each service maps exactly onto one thread", success)

  engine->Finalize();
  return 0;
}
