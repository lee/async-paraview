/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsNetworkPacket.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioInternalsNetworkPacket.h"
#include "vtkByteSwap.h"
#include "vtkEndian.h"
#include "vtkType.h"

#include <iomanip>
#include <sstream>
#include <vector>

namespace
{
// the ServicesAsio module always transmits network packets as little-endian binaries.
// runtime test for endianness. true: little endian, false: big endian
constexpr bool IsLittleEndian()
{
#ifdef VTK_WORDS_BIGENDIAN
  return false;
#else
  return true;
#endif
}

class OutputArchive
{
public:
  OutputArchive& operator&(int& value)
  {
    *this& value;
    return *this;
  }

  OutputArchive& operator&(const int& value)
  {
    if (!IsLittleEndian())
    {
      auto swapped = value;
      vtkByteSwap::Swap2LE(&swapped);
      data.write(reinterpret_cast<const char*>(&swapped), sizeof(int));
      return *this;
    }
    data.write(reinterpret_cast<const char*>(&value), sizeof(int));
    return *this;
  }

  OutputArchive& operator&(std::size_t& value)
  {
    *this& value;
    return *this;
  }

  OutputArchive& operator&(const std::size_t& value)
  {
    if (!IsLittleEndian())
    {
      auto swapped = value;
      vtkByteSwap::Swap2LE(&swapped);
      data.write(reinterpret_cast<const char*>(&swapped), sizeof(std::size_t));
      return *this;
    }
    data.write(reinterpret_cast<const char*>(&value), sizeof(std::size_t));
    return *this;
  }

  OutputArchive& operator&(bool& value)
  {
    *this& value;
    return *this;
  }

  OutputArchive& operator&(const bool& value)
  {
    if (!IsLittleEndian())
    {
      auto swapped = value;
      vtkByteSwap::Swap2LE(&swapped);
      data.write(reinterpret_cast<const char*>(&swapped), sizeof(bool));
      return *this;
    }
    data.write(reinterpret_cast<const char*>(&value), sizeof(bool));
    return *this;
  }

  OutputArchive& operator&(vtkIdType& value)
  {
    *this& value;
    return *this;
  }

  OutputArchive& operator&(const vtkIdType& value)
  {
    if (!IsLittleEndian())
    {
      auto swapped = value;
      vtkByteSwap::Swap2LE(&swapped);
      data.write(reinterpret_cast<const char*>(&swapped), sizeof(vtkIdType));
      return *this;
    }
    data.write(reinterpret_cast<const char*>(&value), sizeof(vtkIdType));
    return *this;
  }

  OutputArchive& operator&(std::vector<std::uint8_t>& values)
  {
    *this& values;
    return *this;
  }

  OutputArchive& operator&(const std::vector<std::uint8_t>& values)
  {
    data << std::setfill('0') << std::setw(8) << std::hex << values.size();
    for (auto& value : values)
    {
      data << value;
    }
    return *this;
  }

  OutputArchive& operator&(std::string& value)
  {
    *this& value;
    return *this;
  }

  OutputArchive& operator&(const std::string& value)
  {
    data << std::setfill('0') << std::setw(8) << std::hex << value.length();
    for (auto& character : value)
    {
      data << character;
    }
    return *this;
  }

  void write(unsigned char* ptr, vtkIdType size) { data.write(reinterpret_cast<char*>(ptr), size); }

  std::ostringstream data;
};

class InputArchive
{
public:
  InputArchive& operator&(int& value)
  {
    data.read(reinterpret_cast<char*>(&value), sizeof(int));
    if (!IsLittleEndian())
    {
      vtkByteSwap::Swap2LE(&value);
    }
    return *this;
  }
  InputArchive& operator&(std::size_t& value)
  {
    data.read(reinterpret_cast<char*>(&value), sizeof(std::size_t));
    if (!IsLittleEndian())
    {
      vtkByteSwap::Swap2LE(&value);
    }
    return *this;
  }
  InputArchive& operator&(bool& value)
  {
    data.read((char*)&value, sizeof(bool));
    if (!IsLittleEndian())
    {
      vtkByteSwap::Swap2LE(&value);
    }
    return *this;
  }
  InputArchive& operator&(vtkIdType& value)
  {
    data.read((char*)&value, sizeof(vtkIdType));
    if (!IsLittleEndian())
    {
      vtkByteSwap::Swap2LE(&value);
    }
    return *this;
  }

  InputArchive& operator&(std::vector<std::uint8_t>& values)
  {
    std::string header(8, '0');
    std::size_t size = 0;
    data.read((char*)&header.front(), 8);
    std::istringstream headerStream;
    headerStream.str(header);
    headerStream >> std::hex >> size;
    values.resize(size);
    data.read((char*)values.data(), size);
    return *this;
  }

  InputArchive& operator&(std::string& value)
  {
    std::string header(8, '0');
    std::size_t size = 0;
    data.read((char*)&header.front(), 8);
    std::istringstream headerStream;
    headerStream.str(header);
    headerStream >> std::hex >> size;
    value.resize(size);
    data.read((char*)value.data(), size);
    return *this;
  }

  void read(unsigned char* ptr, vtkIdType size) { data.read(reinterpret_cast<char*>(ptr), size); }

  std::istringstream data;
};
}

std::string ToBytes(const vtkAsioInternalsNetworkPacket& netPacket)
{
  ::OutputArchive oarchive;
  oarchive& netPacket.Secret;
  oarchive& netPacket.Destination;
  oarchive& netPacket.Channel;
  oarchive& netPacket.ResponseRequired;
  netPacket.Payload->save(oarchive);
  return oarchive.data.str();
}

vtkAsioInternalsNetworkPacket FromBytes(const std::string& bytes)
{
  vtkAsioInternalsNetworkPacket netPacket;
  ::InputArchive iarchive;
  iarchive.data.str(bytes);
  iarchive& netPacket.Secret;
  iarchive& netPacket.Destination;
  iarchive& netPacket.Channel;
  iarchive& netPacket.ResponseRequired;
  netPacket.Payload = std::make_shared<vtkPacket>();
  netPacket.Payload->load(iarchive);

  return netPacket;
}
