/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkThalliumServiceEndpoint.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkThalliumServiceEndpoint.h"

#include "vtkLogger.h"
#include "vtkObjectFactory.h"
#include "vtkPacket.h"
#include "vtkThalliumServicesEngine.h"
#include "vtkThalliumServicesEngineInternals.h"

#include <cassert>
#include <thallium.hpp>
#include <thallium/serialization/stl/array.hpp>
#include <thallium/serialization/stl/map.hpp>
#include <thallium/serialization/stl/pair.hpp>
#include <thallium/serialization/stl/string.hpp>
#include <thallium/serialization/stl/vector.hpp>
#include <tuple>

//#define DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT

/**
 * This is an internal `thallium::provider` used by vtkThalliumServiceEndpoint.
 * To support publish-subscribe, the vtkThalliumService make a reverse connection
 * to the vtkThalliumServiceEndpoint and then pushes messages to this provider.
 */
class vtkThalliumServiceEndpoint::vtkProvider
  : public thallium::provider<vtkThalliumServiceEndpoint::vtkProvider>
{
  using Superclass = thallium::provider<vtkThalliumServiceEndpoint::vtkProvider>;

public:
  vtkProvider(thallium::engine& engine, vtkThalliumServiceEndpoint* endpoint)
    : Superclass(engine, ++vtkThalliumServiceEndpoint::vtkProvider::NextId)
    , Endpoint(endpoint)
  {
    vtkLogF(INFO, "vtkProvider(%p)", this);
    this->RProcedures.push_back(
      this->define("publish", &vtkThalliumServiceEndpoint::vtkProvider::Publish)
        .disable_response());
  }

  void Reset()
  {
    vtkLogF(INFO, "vtkProvider::Reset(%p)", this);
    for (auto& proc : this->RProcedures)
    {
      proc.deregister();
    }
    this->RProcedures.clear();
    this->Endpoint = nullptr;
  }

private:
  /**
   * This method gets called by `vtkService` when a message is published on a
   * named channel.
   */
  void Publish(const thallium::request& /*req*/, const std::string& channel,
    const vtkPacket& packet, bool cached) const
  {
    vtkLogF(TRACE, "got data on channel %s (cached=%d)", channel.c_str(), cached);
    if (this->Endpoint)
    {
      this->Endpoint->DispatchOnChannel(channel, packet, cached);
    }
  }

  vtkThalliumServiceEndpoint* Endpoint;
  std::vector<thallium::remote_procedure> RProcedures;
  static uint16_t NextId;
};

uint16_t vtkThalliumServiceEndpoint::vtkProvider::NextId;

class vtkThalliumServiceEndpoint::vtkInternals
{
public:
  thallium::engine Engine;

  // the handle is available once a connection is established.
  using SubjectT = rxcpp::subjects::subject<thallium::provider_handle>;
  SubjectT ServiceHandleSubject;
  thallium::provider_handle ServiceHandle;
  rxcpp::composite_subscription ServiceHandleSubscription;

  // this counter is used to identify align requests and their replies.
  std::unique_ptr<vtkThalliumServiceEndpoint::vtkProvider> Provider;
  bool IsServiceRemote{ false };

  vtkInternals(thallium::engine& engine, vtkThalliumServiceEndpoint* /*self*/)
    : Engine(engine)
    , ServiceHandle(thallium::provider_handle())
  {
    this->ServiceHandleSubscription = this->ServiceHandleSubject.get_observable().subscribe(
      [&](thallium::provider_handle handle) { this->ServiceHandle = handle; });
  }

  ~vtkInternals() { this->ServiceHandleSubscription.unsubscribe(); }
};

vtkStandardNewMacro(vtkThalliumServiceEndpoint);
//----------------------------------------------------------------------------
vtkThalliumServiceEndpoint::vtkThalliumServiceEndpoint() = default;

//----------------------------------------------------------------------------
vtkThalliumServiceEndpoint::~vtkThalliumServiceEndpoint()
{
  vtkLogF(INFO, "vtkThalliumServiceEndpoint::~vtkThalliumServiceEndpoint(%p) %s", this,
    this->GetServiceName().c_str());
}

//----------------------------------------------------------------------------
void vtkThalliumServiceEndpoint::InitializeInternal()
{
  auto* engine = vtkThalliumServicesEngine::SafeDownCast(this->GetEngine());
  if (!engine)
  {
    vtkLogF(ERROR, "engine not of correct type!");
    std::terminate();
  }

  auto* sessionInternals = engine->GetInternals();
  auto tlEngine = sessionInternals->GetEngine();

  // This needs to be done here instead of the constructor,
  // since the engine is not setup until `vtkServiceEndpoint::Initialize`.
  this->Internals.reset(new vtkThalliumServiceEndpoint::vtkInternals(tlEngine, this));
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkThalliumServiceEndpoint::ConnectInternal(
  const std::string& serviceName, const std::string& url)
{
  auto& internals = *this->Internals;
  // register thallium::provider on the engine to handle published messages.
  internals.Provider.reset(new vtkThalliumServiceEndpoint::vtkProvider(internals.Engine, this));

  auto source = rxcpp::observable<>::create<bool>(
    [&internals, url, serviceName](const rxcpp::subscriber<bool>& subscriber) {
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
      vtkLogF(WARNING, "subscribed");
#endif
      auto ep = internals.Engine.lookup(url);
      auto handshake = internals.Engine.define("handshake");
      internals.Engine.get_handler_pool().make_thread(
        [handshake, ep, url, serviceName, subscriber, &internals]() {
          std::string token = "connection-secret";
          uint16_t providerId = handshake.on(ep)(token, serviceName);
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
          vtkLogF(WARNING, "id is back %d", providerId);
#endif
          if (providerId)
          {
            internals.ServiceHandleSubject.get_subscriber().on_next(
              thallium::provider_handle(ep, providerId));
            subscriber.on_next(true);
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
            vtkLogF(WARNING, "id emitted %d", providerId);
#endif
          }
          else
          {
            internals.ServiceHandleSubject.get_subscriber().on_next(thallium::provider_handle());
            subscriber.on_next(false);
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
            vtkLogF(WARNING, "id emitted %d", providerId);
#endif
          }
        },
        thallium::anonymous{});
      // TODO update internals.IsServiceRemote based on connection metadata?
      internals.IsServiceRemote = false;
    });
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
  vtkLogF(WARNING, "return source");
#endif
  return
    // since the response will be emitted by the rpc thread but received by the
    // engine thread we use a queue to avoid missing any events
    source.observe_on(rxcpp::observe_on_event_loop());
}

//----------------------------------------------------------------------------
void vtkThalliumServiceEndpoint::ShutdownInternal()
{
  vtkLogF(INFO, "vtkThalliumServiceEndpoint::ShutdownInternal");
  auto& internals = *this->Internals;
  internals.Provider->Reset();
  internals.Provider.reset();
  internals.ServiceHandle = thallium::provider_handle();
}

//----------------------------------------------------------------------------
void vtkThalliumServiceEndpoint::SendMessageInternal(const vtkPacket& packet) const
{
  auto& internals = *this->Internals;
  auto phandle = internals.ServiceHandle;
  if (phandle.is_null())
  {
    vtkLogF(ERROR, "connection has failed; cannot sent message");
    return;
  }
  vtkLogF(TRACE, "sending message to %s", this->GetServiceName().c_str());

  auto send_message = internals.Engine.define("send_message").disable_response();
  const bool isServiceRemote = internals.IsServiceRemote;
  try
  {
    auto callable = send_message.on(phandle).with_serialization_context(isServiceRemote);
    callable(packet);
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Thallium exception in SendMessageInternal!:\n%s", exception.what());
  }
}

//----------------------------------------------------------------------------
rxcpp::observable<vtkPacket> vtkThalliumServiceEndpoint::SendRequestInternal(
  const vtkPacket& packet) const
{
  auto& internals = *this->Internals;
  auto phandle = internals.ServiceHandle;
  if (phandle.is_null())
  {
    vtkLogF(ERROR, "connection has failed; cannot send request");
    return rxcpp::observable<>::just(vtkPacket());
  }
  vtkLogF(TRACE, "sending request to %s", this->GetServiceName().c_str());
  try
  {
    auto source = rxcpp::observable<>::create<vtkPacket>(
      [&internals, phandle, packet](const rxcpp::subscriber<vtkPacket>& subscriber) {
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
        vtkLogF(WARNING, "subscribed to request");
#endif
        auto send_request = internals.Engine.define("send_request");
        const bool isServiceRemote = internals.IsServiceRemote;

        // callable will be called on the (remote) service ...
        auto callable = send_request.on(phandle).with_serialization_context(isServiceRemote);
        auto response = std::make_shared<thallium::async_response>(callable.async(packet));

        internals.Engine.get_handler_pool().make_thread(
          [=]() {
            auto responsePacket = response->wait().as<vtkPacket>();
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
            vtkLogF(WARNING, "response is here");
#endif
            // If the server could not perform the requested operation it will fill the
            // __vtk_error_message__ field.
            if (responsePacket.GetJSON().contains("__vtk_error_message__"))
            {
              const std::string errorMsg =
                responsePacket.GetJSON().at("__vtk_error_message__").get<std::string>();
              subscriber.on_error(rxcpp::rxu::make_error_ptr(std::runtime_error(errorMsg)));
            }
            else
            {
              subscriber.on_next(responsePacket);
            }
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
            vtkLogF(WARNING, "emitted response");
#endif
          },
          thallium::anonymous{});
      });
    return source.take(1);
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Thallium exception in SendRequestInternal!:\n%s", exception.what());
    return rxcpp::observable<>::just(vtkPacket());
  }
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkThalliumServiceEndpoint::SubscribeInternal(const std::string& channel)
{
  auto& internals = *this->Internals;
  auto phandle = internals.ServiceHandle;
  if (phandle.is_null())
  {
    vtkLogF(ERROR, "connection has failed; cannot sent message");
    return rxcpp::observable<>::just(false);
  }
  vtkLogF(INFO, "subscribing to %s on %s", channel.c_str(), this->GetServiceName().c_str());

  try
  {
    auto source = rxcpp::observable<>::create<bool>(
      [&internals, phandle, channel](const rxcpp::subscriber<bool>& subscriber) {
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
        vtkLogF(WARNING, "subscribed to request for subscription");
#endif
        // Send a subscription request.
        auto subscribe = internals.Engine.define("subscribe");
        auto providerId = internals.Provider->get_provider_id();
        const std::string url = internals.Engine.self();

        auto callable = subscribe.on(phandle);
        auto response =
          std::make_shared<thallium::async_response>(callable.async(channel, url, providerId));
        internals.Engine.get_handler_pool().make_thread(
          [=]() {
            bool status = response->wait().as<bool>();
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
            vtkLogF(WARNING, "response is here");
#endif
            subscriber.on_next(status);
#ifdef DEBUG_VTK_THALLIUM_SERVICE_ENDPOINT
            vtkLogF(WARNING, "emitted response");
#endif
          },
          thallium::anonymous{});
      });
    return source.observe_on(rxcpp::observe_on_event_loop()).take(1);
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Thallium exception in SubscribeInternal!:\n%s", exception.what());
    return rxcpp::observable<>::just(false);
  }
}

//----------------------------------------------------------------------------
void vtkThalliumServiceEndpoint::UnsubscribeInternal(const std::string& channel)
{
  auto& internals = *this->Internals;
  if (!internals.Provider)
  {
    return;
  }

  auto phandle = internals.ServiceHandle;
  if (phandle.is_null())
  {
    return;
  }
  vtkLogF(
    INFO, "unsubscribing from channel %s on %s", channel.c_str(), this->GetServiceName().c_str());

  try
  {
    // Send a request for unsubscription.
    auto unsubscribe = internals.Engine.define("unsubscribe").disable_response();
    const std::string url = internals.Engine.self();
    auto providerId = internals.Provider->get_provider_id();
    unsubscribe.on(phandle)(channel, url, providerId);
  }
  catch (const std::exception& exception)
  {
    vtkLogF(ERROR, "Thallium exception in UnsubscribeInternal!:\n%s", exception.what());
  }
}

//----------------------------------------------------------------------------
void vtkThalliumServiceEndpoint::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
