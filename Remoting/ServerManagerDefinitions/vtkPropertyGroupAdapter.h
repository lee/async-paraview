/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPropertyGroupAdapter.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkPropertyGroupAdapter_h
#define vtkPropertyGroupAdapter_h
#include "vtkObject.h"
#include "vtkRemotingServerManagerDefinitionsModule.h"
#include "vtkSmartPointer.h"

#include <string>
#include <vector>

class vtkSMPropertyGroup;
class vtkProxyAdapter;
class vtkSMDocumentation;

class VTKREMOTINGSERVERMANAGERDEFINITIONS_EXPORT vtkPropertyGroupAdapter : public vtkObject
{
public:
  static vtkPropertyGroupAdapter* New();
  vtkTypeMacro(vtkPropertyGroupAdapter, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  std::string GetName() const;
  std::string GetXMLLabel() const;

  std::string GetPanelWidget() const;
  std::string GetPanelVisibility() const;

  std::vector<std::string> GetWidgetDecorators() const;

protected:
  vtkPropertyGroupAdapter();
  ~vtkPropertyGroupAdapter() override;

  //@{
  friend class vtkProxyAdapter;
  void SetSMPropertyGroup(vtkSMPropertyGroup* group);
  void SetName(const std::string& name);
  //@}

private:
  vtkSmartPointer<vtkSMPropertyGroup> SMPropertyGroup;

  std::string Name;

  vtkPropertyGroupAdapter(const vtkPropertyGroupAdapter&) = delete;
  void operator=(const vtkPropertyGroupAdapter&) = delete;
};
#endif
