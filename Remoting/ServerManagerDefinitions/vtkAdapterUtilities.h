/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkAdapterUtilities.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkAdapterUtilities_h
#define vtkAdapterUtilities_h

#include <string>
#include <vector>

class vtkPVXMLElement;

class vtkAdapterUtilities
{
public:
  static std::vector<std::string> GetDecoratorNames(vtkPVXMLElement* hints);
};
#endif
