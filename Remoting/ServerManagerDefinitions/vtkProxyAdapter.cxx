/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkProxyAdapter.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkProxyAdapter.h"
#include "vtkPropertyAdapter.h"
#include "vtkPropertyGroupAdapter.h"
#include "vtkSMOrderedPropertyIterator.h"
#include "vtkSMPropertyGroup.h"
#include "vtkSMProxy.h"
#include "vtkSmartPointer.h"
#include <algorithm> // for std::replace
#include <memory>
#include <vector>

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkProxyAdapter);

//-----------------------------------------------------------------------------
vtkProxyAdapter::vtkProxyAdapter() = default;

//-----------------------------------------------------------------------------
vtkProxyAdapter::~vtkProxyAdapter() = default;

//-----------------------------------------------------------------------------
void vtkProxyAdapter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "SMProxy " << this->SMProxy;
  if (this->SMProxy)
  {
    this->SMProxy->PrintSelf(os, indent.GetNextIndent());
  }
}

//-----------------------------------------------------------------------------
void vtkProxyAdapter::SetSMProxy(vtkSMProxy* proxy)
{
  // based on pqProxyWidget::createPropertyWidgets

  // unique suffix to avoid insiues of embed group when using subproxies
  std::string suffix = proxy->GetXMLLabel();
  std::replace(std::begin(suffix), std::end(suffix), ' ', '_');

  // step 1: iterate over all groups create PropertyGroupAdapters and populate `property2Groups`.
  //         this will make it easier to determine if a property belong to a
  //         group.

  std::map<vtkSMProperty*, std::string> property2Groups;
  std::map<std::string, vtkSmartPointer<vtkPropertyGroupAdapter>> name2Groups;
  for (int i = 0; i < proxy->GetNumberOfPropertyGroups(); i++)
  {
    vtkSMPropertyGroup* smgroup = proxy->GetPropertyGroup(i);
    std::string label;
    if (smgroup->GetXMLLabel())
    {
      label = smgroup->GetXMLLabel();
    }

    auto iter = name2Groups.find(label);
    vtkSmartPointer<vtkPropertyGroupAdapter> group;
    if (iter == name2Groups.end())
    {
      group = vtk::TakeSmartPointer(vtkPropertyGroupAdapter::New());
      group->SetSMPropertyGroup(smgroup);
      if (group->GetName().empty())
      {
        group->SetName("__unnamed_group_" + suffix + std::to_string(i));
      }
      this->PropertyGroups.emplace_back(group);
      name2Groups[label] = group;
    }
    else
    {
      group = iter->second;
    }

    for (int i = 0; i < smgroup->GetNumberOfProperties(); i++)
    {
      property2Groups[smgroup->GetProperty(i)] = label;
    }
  }

  // step 2: iterate over all properties and build an ordered list of properties
  //         that corresponds to the order in which the widgets will be rendered.
  //         this is generally same as the order in the XML with one exception,
  //         properties in groups with same label are placed next to each other.
  std::list<vtkSmartPointer<vtkPropertyAdapter>> orderedProperties;
  std::map<vtkSmartPointer<vtkPropertyGroupAdapter>, decltype(orderedProperties)::iterator>
    groupEndIterators;

  // Add properties
  vtkNew<vtkSMOrderedPropertyIterator> iterator;
  iterator->SetProxy(proxy);
  for (iterator->Begin(); !iterator->IsAtEnd(); iterator->Next())
  {
    vtkSMProperty* smproperty = iterator->GetProperty();
    vtkSmartPointer<vtkPropertyAdapter> property = vtk::TakeSmartPointer(vtkPropertyAdapter::New());
    property->SetSMProperty(smproperty);

    auto iter = property2Groups.find(smproperty);

    if (iter != property2Groups.end())
    {
      vtkSmartPointer<vtkPropertyGroupAdapter> group;
      try
      {
        vtkDebugMacro(<< "Set property " << property->GetLabel() << " in group " << iter->second);
        group = name2Groups.at(iter->second);
      }
      catch (std::out_of_range& e)
      {
        vtkLogF(ERROR, " invalid relation between property and property group");
      }

      property->SetPropertyGroup(group);

      auto groupEndIterator = groupEndIterators.find(group);
      auto insertPosition = (groupEndIterator != groupEndIterators.end())
        ? std::next(groupEndIterator->second)
        : orderedProperties.end();

      groupEndIterators[group] = orderedProperties.insert(insertPosition, property);
    }
    else
    {
      orderedProperties.push_back(property);
    }
  }

  // copy to vector since wrapping cannot handle lists
  this->Properties.reserve(orderedProperties.size());
  std::copy(std::begin(orderedProperties), std::end(orderedProperties),
    std::back_inserter(this->Properties));

  this->SMProxy = proxy;
}

//------------------------------------------------------------------------------
std::vector<vtkSmartPointer<vtkPropertyAdapter>> vtkProxyAdapter::GetProperties() const
{
  return this->Properties;
}

//------------------------------------------------------------------------------
std::vector<vtkSmartPointer<vtkPropertyGroupAdapter>> vtkProxyAdapter::GetPropertyGroups() const
{
  return this->PropertyGroups;
}

//------------------------------------------------------------------------------
std::string vtkProxyAdapter::GetGroup() const
{
  return this->SMProxy->GetXMLGroup();
}

//------------------------------------------------------------------------------
std::string vtkProxyAdapter::GetName() const
{
  return this->SMProxy->GetXMLName();
}

//------------------------------------------------------------------------------
std::string vtkProxyAdapter::GetLabel() const
{
  if (this->SMProxy->GetXMLLabel())
  {
    return this->SMProxy->GetXMLLabel();
  }
  return "";
}

//------------------------------------------------------------------------------
void vtkProxyAdapter::Move(vtkProxyAdapter* other)
{
  this->SMProxy = other->SMProxy;
  other->SMProxy = nullptr;
  this->Properties = std::move(other->Properties);
  this->PropertyGroups = std::move(other->PropertyGroups);
}

//------------------------------------------------------------------------------
vtkSmartPointer<vtkPropertyAdapter> vtkProxyAdapter::GetProperty(const std::string& name) const
{
  vtkSMProperty* smproperty = this->SMProxy->GetProperty(name.c_str());
  vtkSmartPointer<vtkPropertyAdapter> property = vtk::TakeSmartPointer(vtkPropertyAdapter::New());
  property->SetSMProperty(smproperty);
  return property;
}
