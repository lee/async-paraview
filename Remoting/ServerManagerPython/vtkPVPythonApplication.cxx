/*=========================================================================

  Program:   ParaView
  Module:    vtkPVPythonApplication.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPython.h"

#include "vtkPVPythonApplication.h"

#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkObjectFactory.h"
#include "vtkPythonRunLoop.h"

class vtkPVPythonApplication::vtkInternals
{
public:
  std::unique_ptr<vtkDistributedEnvironment> Environment;
};

vtkStandardNewMacro(vtkPVPythonApplication);
//----------------------------------------------------------------------------
vtkPVPythonApplication::vtkPVPythonApplication()
  : Internals(new vtkPVPythonApplication::vtkInternals())
{
}

//----------------------------------------------------------------------------
vtkPVPythonApplication::~vtkPVPythonApplication() = default;

//----------------------------------------------------------------------------
vtkPVPythonApplication* vtkPVPythonApplication::GetInstance()
{
  return vtkPVPythonApplication::SafeDownCast(vtkPVCoreApplication::GetInstance());
}
//----------------------------------------------------------------------------
bool vtkPVPythonApplication::InitializeUsingPython(
  const std::string& executable, vtkMultiProcessController* globalController)
{
  auto* runLoop = vtkPythonRunLoop::GetInstance();
  if (runLoop == nullptr)
  {
    vtkLogF(ERROR, "Initialize vtkPythonRunLoop first !");
    return false;
  }

  if (globalController == nullptr)
  {
    this->Internals->Environment = std::make_unique<vtkDistributedEnvironment>(nullptr, nullptr);
    globalController = this->Internals->Environment->GetController();
  }

  return this->Initialize(executable, runLoop->observe_on_run_loop(), globalController);
}

//----------------------------------------------------------------------------
void vtkPVPythonApplication::FinalizeInternal()
{
  // make sure we have the GIL. If vtkPVPythonApplication is used in the
  // context of a python application this is true. However,
  // vtkPVPythonApplication is also used in catalyst which does not interface
  // with CPython API.
  vtkPythonScopeGilEnsurer gilEnsurer;

  // This function will be called from Python which mean that the GIL will be
  // held.  If a RPC arrives during finalization or there is an incoming
  // request for one, we will have a race condition since the rpc thread needs
  // the GIL to add the request to the loop. So we close the loop here and
  // stop accepting any further events.

  if (auto* runLoop = vtkPythonRunLoop::GetInstance())
  {
    runLoop->Close();
  }
  // FinalizeInternal will be called from Python which mean that the GIL will be held.
  // To avoid race conditions with the RPC thread we release it here.
  // When interpreter exits because of SIGINT, GIL may already be released (on windows)
  if (Py_IsInitialized())
  {
    Py_BEGIN_ALLOW_THREADS

      this->Superclass::FinalizeInternal();

    Py_END_ALLOW_THREADS
  }
  else
  {
    this->Superclass::FinalizeInternal();
  }
}

//----------------------------------------------------------------------------
void vtkPVPythonApplication::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
