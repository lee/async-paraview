set(classes
  vtkPVPythonApplication
  vtkPVPythonInformation
  vtkPVPythonModule)

vtk_module_add_module(AsyncParaView::RemotingServerManagerPython
  CLASSES ${classes})
