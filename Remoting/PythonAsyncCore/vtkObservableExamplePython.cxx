#include "vtkObservableExamplePython.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"

#include <chrono>

vtkStandardNewMacro(vtkObservableExamplePython);
//----------------------------------------------------------------------------
vtkObservableExamplePython::vtkObservableExamplePython()
{
  auto period = std::chrono::milliseconds(50);
  auto values = rxcpp::observable<>::interval(period);
  this->Observable = values.take(10);
}

//----------------------------------------------------------------------------
vtkObservableExamplePython::~vtkObservableExamplePython() = default;

//----------------------------------------------------------------------------
rxcpp::observable<long> vtkObservableExamplePython::GetObservable()
{
  return this->Observable;
}
//----------------------------------------------------------------------------
void vtkObservableExamplePython::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
