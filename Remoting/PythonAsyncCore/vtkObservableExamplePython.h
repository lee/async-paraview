#ifndef vtkObservableExamplePython_h
#define vtkObservableExamplePython_h

#include "vtkRemotingPythonAsyncCoreModule.h" // for exports

#include "vtkObject.h"
#include "vtkPythonObservableWrapper.h"

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class VTKREMOTINGPYTHONASYNCCORE_EXPORT vtkObservableExamplePython : public vtkObject
{
public:
  static vtkObservableExamplePython* New();
  vtkTypeMacro(vtkObservableExamplePython, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  // something that returns an observable, cannot be wrapped as is
  rxcpp::observable<long> GetObservable();
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(long, GetObservable())

protected:
  vtkObservableExamplePython();
  ~vtkObservableExamplePython() override;

private:
  vtkObservableExamplePython(const vtkObservableExamplePython&) = delete;
  void operator=(const vtkObservableExamplePython&) = delete;

  rxcpp::observable<long> Observable;
};

#endif
