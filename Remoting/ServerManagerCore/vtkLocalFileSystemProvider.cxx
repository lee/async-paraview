/*=========================================================================

  Program:   ParaView
  Module:    vtkRemoteFileSystemProvider.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkLocalFileSystemProvider.h"

vtkObjectFactoryNewMacro(vtkLocalFileSystemProvider);
//----------------------------------------------------------------------------
vtkLocalFileSystemProvider::vtkLocalFileSystemProvider() = default;
//----------------------------------------------------------------------------
vtkLocalFileSystemProvider::~vtkLocalFileSystemProvider() = default;
//----------------------------------------------------------------------------
void vtkLocalFileSystemProvider::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
//----------------------------------------------------------------------------
void vtkLocalFileSystemProvider::InitializeInternal(vtkService* service)
{
  // we operate locally no need for a service
  (void)service;
}
//----------------------------------------------------------------------------
rxcpp::observable<vtkPacket> vtkLocalFileSystemProvider::ProcessLocally(const vtkPacket& packet)
{
  // we want something better than rxcpp::observable<>::just(this->Process(packet))
  // bacause we want to pass errors
  return rxcpp::observable<>::create<vtkPacket>(
    [this, packet](rxcpp::subscriber<vtkPacket> subscriber) {
      vtkPacket response = this->Process(packet);
      if (response.GetJSON().contains("__vtk_error_message__"))
      {
        const std::string& msg = response.GetJSON().at("__vtk_error_message__").get<std::string>();
        throw std::runtime_error(msg);
      }
      subscriber.on_next(response);
    });
}
