/*=========================================================================

  Program:   ParaView
  Module:    vtkInformationHelpers.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkInformationHelpers.h"
#include "vtkAlgorithm.h"
#include "vtkExecutive.h"
#include "vtkInformation.h"
#include "vtkLogger.h"
#include "vtkNJson.h"
#include "vtkObjectWrapper.h"
#include "vtkStreamingDemandDrivenPipeline.h"

namespace
{

/**
 * @brief Extract timesteps from a vtkObjectWrapper
 *
 * @return true on success, false otherwise
 * @note logic copied from vtkSITimeStepsProperty of paraview/paraview
 */
bool TimeStepsInformationHelper(const vtkObjectWrapper* wrapper, vtkNJson& values)
{

  // Get reference to the algorithm
  vtkAlgorithm* algo = vtkAlgorithm::SafeDownCast(wrapper->GetVTKObject());

  if (!algo)
  {
    return false;
  }

  vtkInformation* outInfo = algo->GetExecutive()->GetOutputInformation(0);

  // If no information just exit
  if (!outInfo)
  {
    return false;
  }

  // Else find out
  if (outInfo->Has(vtkStreamingDemandDrivenPipeline::TIME_STEPS()))
  {
    const double* timeSteps = outInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
    int len = outInfo->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());

    for (int i = 0; i < len; i++)
    {
      values.push_back(timeSteps[i]);
    }
    return true;
  }

  // No value does not mean failure. So just return true
  return true;
}

}

bool GetValueUsingHelper(
  const std::string& helperName, const vtkObjectWrapper* wrapper, vtkNJson& values)
{
  if (helperName == "TimeStepsInformationHelper")
  {
    return TimeStepsInformationHelper(wrapper, values);
  }
  // TODO add more helpers
  else
  {
    vtkLogF(WARNING, "Support for '%s' is not yet implemented, values will be skipped",
      helperName.c_str());
  }
  return false;
}
