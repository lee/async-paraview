/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPVCoreApplication.txx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkPVCoreApplication_txx
#define vtkPVCoreApplication_txx

#include "vtkServicesEngine.h"

//-----------------------------------------------------------------------------
template <typename Predicate>
void vtkPVCoreApplication::ProcessEventsUntil(
  const vtkrxcpp::schedulers::run_loop& rlp, Predicate stopProcessing) const
{
  do
  {
    while (!rlp.empty() && (rlp.peek().when < rlp.now()))
    {
      rlp.dispatch();
    }
  } while (!stopProcessing());
}

//-----------------------------------------------------------------------------
template <typename T>
inline T vtkPVCoreApplication::Await(
  const rxcpp::schedulers::run_loop& rlp, rxcpp::observable<T> observable) const
{
  return this->GetServicesEngine()->Await(rlp, observable);
}
#endif
