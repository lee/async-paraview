/*=========================================================================

  Program:   ParaView
  Module:    vtkInformationHelpers.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @brief Access value of a property using a helper.
 *
 * This is for proxy properties that use <....Helper/> tag.
 * Some proxy properties do not correspond to a method for the underlying
 * vtkObject in this case we use specific helpers.
 *
 */

#ifndef vtkInformationHelper_h
#define vtkInformationHelper_h

#include "vtkNJsonFwd.h"
#include "vtkRemotingServerManagerCoreModule.h" // for exports

#include <string>

class vtkObjectWrapper;

/**
 * @brief Access the values of a property of an object using a helper instead of
 * directly using reflection.
 *
 * @todo expand accepted arguments to handle more type of helpers.(some have arguments)
 *
 * @return  true of success, false otherwise
 */
bool VTKREMOTINGSERVERMANAGERCORE_EXPORT GetValueUsingHelper(
  const std::string& helperName, const vtkObjectWrapper* wrapper, vtkNJson& values);
#endif
