/*=========================================================================

  Program:   ParaView
  Module:    vtkObjectWrapper.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkMetaReaderWrapper.h"
#include "vtkLogger.h"
#include "vtkMetaReader.h"
#include "vtkObjectFactory.h"
#include "vtkObjectStore.h"
#include "vtkRemoteObjectProvider.h"

// clang-format off
#include <vtk_fmt.h> // needed for `fmt`
#include VTK_FMT(fmt/core.h)
// clang-format on

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkMetaReaderWrapper);

//----------------------------------------------------------------------------
vtkMetaReaderWrapper::vtkMetaReaderWrapper() = default;

//----------------------------------------------------------------------------
vtkMetaReaderWrapper::~vtkMetaReaderWrapper() = default;

//----------------------------------------------------------------------------
void vtkMetaReaderWrapper::PrintSelf(ostream& os, vtkIndent indent)
{
  os << this->FileNameMethod << '\n';
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
bool vtkMetaReaderWrapper::Initialize(
  vtkTypeUInt32 gid, const vtkNJson& state, vtkRemoteObjectProvider* provider)
{
  // creates the vtkMetaReader instance.
  if (this->Superclass::Initialize(gid, state, provider))
  {
    auto store = provider->GetObjectStore();
    auto subProxies = state["subproxies"];
    vtkTypeUInt32 readerGID = VTK_TYPE_UINT32_MAX;
    try
    {
      readerGID = subProxies.at("Reader").get<vtkTypeUInt32>();
    }
    catch (std::exception& e)
    {
      std::string msg =
        vtkfmt::format("Error {} MetaReader state does not have sub-proxies!", e.what());
      throw std::runtime_error(msg);
    }
    // get vtkObject that was created and assigned to the Reader subproxy.
    if (auto reader = store->FindVTKObject<vtkAlgorithm>(readerGID))
    {
      if (auto metaReader = vtkMetaReader::SafeDownCast(this->GetVTKObject()))
      {
        metaReader->SetFileNameMethod(this->FileNameMethod.c_str());
        metaReader->SetReader(reader);
        return true;
      }
      else
      {
        vtkLogF(ERROR, "vtkMetaReader instance not found! Cannot initialize vtkMetaReaderWrapper.");
      }
    }
    else
    {
      vtkLog(ERROR, "Failed to find the sub-proxy reader instance for gid=" << readerGID);
    }
  }
  return false;
}

//----------------------------------------------------------------------------
bool vtkMetaReaderWrapper::ReadXMLAttributes(const vtkpugixml::xml_node& node)
{
  if (this->Superclass::ReadXMLAttributes(node))
  {
    this->FileNameMethod = node.attribute("file_name_method").as_string();
    return true;
  }
  return false;
}

//----------------------------------------------------------------------------
vtkObjectWrapper::PropertyInformation vtkMetaReaderWrapper::TransformPropertyInformation(
  const PropertyInformation& inPropertyInfo)
{
  if (inPropertyInfo.Method == "AddFileName")
  {
    PropertyInformation outPropertyInfo;
    outPropertyInfo.InformationOnly = inPropertyInfo.InformationOnly;
    outPropertyInfo.Method = "SetFileNames";
    outPropertyInfo.Type = inPropertyInfo.Type;
    return outPropertyInfo;
  }
  else
  {
    return inPropertyInfo;
  }
}
