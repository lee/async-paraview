/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingServerManagerCoreLogVerbosity.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkRemotingServerManagerCoreLogVerbosity.h"
#include "vtksys/SystemTools.hxx"

namespace
{
// executed when this library is loaded.
vtkLogger::Verbosity GetInitialRemotingServerManagerCoreVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::RemotingServerManagerCore module.
  const char* VerbosityKey = "VTKREMOTINGSERVERMANAGERCORE_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    const auto verbosity = vtkLogger::ConvertToVerbosity(verbosity_str);
    if (verbosity > vtkLogger::VERBOSITY_INVALID)
    {
      return verbosity;
    }
  }
  return vtkLogger::VERBOSITY_TRACE;
}

// executed when this library is loaded.
vtkLogger::Verbosity GetInitialRemotingServerManagerCoreDataMovementVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::RemotingServerManagerCore module.
  const char* VerbosityKey = "VTKREMOTINGSERVERMANAGERCORE_DATAMOVEMENT_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    const auto verbosity = vtkLogger::ConvertToVerbosity(verbosity_str);
    if (verbosity > vtkLogger::VERBOSITY_INVALID)
    {
      return verbosity;
    }
  }
  return vtkLogger::VERBOSITY_TRACE;
}

thread_local vtkLogger::Verbosity RemotingServerManagerCoreVerbosity =
  GetInitialRemotingServerManagerCoreVerbosity();

thread_local vtkLogger::Verbosity RemotingServerManagerCoreDataMovementVerbosity =
  GetInitialRemotingServerManagerCoreDataMovementVerbosity();
}

//----------------------------------------------------------------------------
vtkLogger::Verbosity vtkRemotingServerManagerCoreLogVerbosity::GetVerbosity()
{
  return ::RemotingServerManagerCoreVerbosity;
}

//----------------------------------------------------------------------------
void vtkRemotingServerManagerCoreLogVerbosity::SetVerbosity(vtkLogger::Verbosity verbosity)
{
  if (verbosity > vtkLogger::VERBOSITY_INVALID)
  {
    ::RemotingServerManagerCoreVerbosity = verbosity;
  }
}

//----------------------------------------------------------------------------
vtkLogger::Verbosity vtkRemotingServerManagerCoreLogVerbosity::GetDataMovementVerbosity()
{
  return ::RemotingServerManagerCoreDataMovementVerbosity;
}

//----------------------------------------------------------------------------
void vtkRemotingServerManagerCoreLogVerbosity::SetDataMovementVerbosity(
  vtkLogger::Verbosity verbosity)
{
  if (verbosity > vtkLogger::VERBOSITY_INVALID)
  {
    ::RemotingServerManagerCoreDataMovementVerbosity = verbosity;
  }
}
