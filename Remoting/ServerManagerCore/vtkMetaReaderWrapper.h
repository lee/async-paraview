/*=========================================================================

  Program:   ParaView
  Module:    vtkMetaReaderWrapper.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkMetaReaderWrapper
 * @brief a wrapper for vtkMetaReader
 *
 * vtkMetaReaderWrapper is used to create/update vtkMetaReader-subclass instances on a
 * remote service using the vtkRemoteObjectProvider.
 */

#ifndef vtkMetaReaderWrapper_h
#define vtkMetaReaderWrapper_h

#include "vtkObjectWrapper.h"

#include "vtkRemotingServerManagerCoreModule.h" // for exports

class vtkRemoteObjectProvider;

class VTKREMOTINGSERVERMANAGERCORE_EXPORT vtkMetaReaderWrapper : public vtkObjectWrapper
{
public:
  static vtkMetaReaderWrapper* New();
  vtkTypeMacro(vtkMetaReaderWrapper, vtkObjectWrapper);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Initializes the wrapper by assigning the FileNameMethod, Reader properties to the wrapped
   * vtkMetaReader instance.
   */
  bool Initialize(
    vtkTypeUInt32 gid, const vtkNJson& state, vtkRemoteObjectProvider* provider) override;

  /**
   *Setup the object wrapper by processing "file_name_method" from the XML file
   */
  bool ReadXMLAttributes(const pugi::xml_node& node) override;

protected:
  vtkMetaReaderWrapper();
  ~vtkMetaReaderWrapper() override;

  std::string FileNameMethod;

  PropertyInformation TransformPropertyInformation(
    const PropertyInformation& inPropertyInfo) override;

private:
  vtkMetaReaderWrapper(const vtkMetaReaderWrapper&) = delete;
  void operator=(const vtkMetaReaderWrapper&) = delete;
};

#endif // vtkMetaReaderWrapper_h
