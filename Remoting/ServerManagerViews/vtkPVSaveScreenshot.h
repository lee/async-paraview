// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class  vtkPVSaveScreenshot
 * @brief  Save screenshot of a view
 *
 */

#ifndef vtkPVSaveScreenshot_h
#define vtkPVSaveScreenshot_h

#include "vtkObject.h"
#include "vtkRemotingServerManagerViewsModule.h" // for exports

class vtkImageWriter;
class vtkDataObject;
class vtkPVView;
class vtkPVImageDelivery;

class VTKREMOTINGSERVERMANAGERVIEWS_EXPORT vtkPVSaveScreenshot : public vtkObject
{
public:
  static vtkPVSaveScreenshot* New();
  vtkTypeMacro(vtkPVSaveScreenshot, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Get/Set the Image writer
   */
  void SetWriter(vtkImageWriter* writer);
  vtkGetObjectMacro(Writer, vtkImageWriter);
  ///@}

  ///@{
  /**
   * Get/Set the View to capture image from
   * @TODO when using multiple views we would need to pass the layout as vtkSMSaveScreenshot in
   * Paraview master does.
   */
  void SetView(vtkPVView* View);
  vtkGetObjectMacro(View, vtkPVView);
  ///@}

  /**
   * Write the data.
   */
  void Write();

protected:
  vtkPVSaveScreenshot();
  ~vtkPVSaveScreenshot() override;

  vtkImageWriter* Writer = nullptr;
  vtkPVView* View = nullptr;

private:
  vtkPVSaveScreenshot(const vtkPVSaveScreenshot&) = delete;
  void operator=(const vtkPVSaveScreenshot&) = delete;

  vtkPVImageDelivery* ImageDelivery;
};

#endif /* end of include guard: vtkPVSaveScreenshot_h */
