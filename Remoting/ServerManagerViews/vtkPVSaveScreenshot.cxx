// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: BSD-3-Clause

#include "vtkPVSaveScreenshot.h"
#include "vtkImageData.h"
#include "vtkImageWriter.h"
#include "vtkInformation.h"
#include "vtkMultiProcessController.h"
#include "vtkPVImageDelivery.h"
#include "vtkPVView.h"
#include "vtkPointData.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPVSaveScreenshot);
vtkCxxSetObjectMacro(vtkPVSaveScreenshot, Writer, vtkImageWriter);
vtkCxxSetObjectMacro(vtkPVSaveScreenshot, View, vtkPVView);

//----------------------------------------------------------------------------
vtkPVSaveScreenshot::vtkPVSaveScreenshot()
{
  this->ImageDelivery = vtkPVImageDelivery::New();
}

//----------------------------------------------------------------------------
vtkPVSaveScreenshot::~vtkPVSaveScreenshot()
{
  this->SetWriter(nullptr);
  this->SetView(nullptr);
  if (this->ImageDelivery)
  {
    this->ImageDelivery->Delete();
  }
}

//-----------------------------------------------------------------------------
void vtkPVSaveScreenshot::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Writer: " << this->Writer << endl;
  os << indent << "View: " << this->View << endl;
}

//----------------------------------------------------------------------------
void vtkPVSaveScreenshot::Write()
{
  if (this->Writer && this->View)
  {
    const bool skipPosting = true; // Do not push this frame on the client
    // TODO make extra render optional ?
    this->View->StillRender(0, skipPosting);
    if (this->View->GetController()->GetLocalProcessId() == 0)
    {
      this->ImageDelivery->SetLosslessMode(true);
      vtkRemoteViewStatsItem item;
      vtkPacket packet = this->ImageDelivery->PackageDisplayFramebuffer(
        this->View->GetRenderWindow(), item, /* prefer_low_quality */ false);
      if (packet.empty())
      {
        vtkErrorMacro("No data captured");
        return;
      }
      vtkSmartPointer<vtkUnsignedCharArray> array =
        vtkUnsignedCharArray::SafeDownCast(packet.GetPayload().at("content"));
      if (!array)
      {
        vtkErrorMacro("No data in array");
        return;
      }
      auto data = vtk::TakeSmartPointer(vtkImageData::New());
      data->SetDimensions(packet.GetJSON()["Width"], packet.GetJSON()["Height"], 1);
      data->GetPointData()->SetScalars(array);
      this->Writer->SetInputDataObject(data);
      this->Writer->Write();
    }
  }
  else
  {
    vtkErrorMacro("Writer or View is not set for vtkPVSaveScreenshot");
  }
}
