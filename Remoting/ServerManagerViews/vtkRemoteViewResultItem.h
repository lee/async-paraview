/*=========================================================================

Program:   ParaView
Module:    vtkRemoteViewResultItem.h

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

   This software is distributed WITHOUT ANY WARRANTY; without even
   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
   PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @struct vtkRemoteViewResultItem
 * @brief encapsulates a remote view's rendering result
 *
 */

#ifndef vtkRemoteViewResultItem_h
#define vtkRemoteViewResultItem_h

#include "vtkNJson.h"
#include "vtkRemoteViewStatsItem.h"
#include "vtkVideoCodecTypes.h"

struct vtkRemoteViewResultItem
{
  std::string Mime;
  int Width = 0;
  int Height = 0;
  vtkIdType Size = 0;
  VTKVideoCodecType Codec = VTKVideoCodecType::VTKVC_VP9;
  std::string CodecLongName;
  uint64_t PTS = 0; // Presentation Time Stamp
  bool KeyFrame = false;
  vtkTypeUInt32 GlobalID = 0;
  vtkRemoteViewStatsItem Stats;

  bool operator==(const vtkRemoteViewResultItem& other) const
  {
    return this->GlobalID == other.GlobalID && this->Mime == other.Mime &&
      this->CodecLongName == other.CodecLongName && this->Width == other.Width &&
      this->Height == other.Height && this->Size == other.Size && this->Codec == other.Codec &&
      this->PTS == other.PTS && this->KeyFrame == other.KeyFrame && this->Stats == other.Stats;
  }

  NLOHMANN_DEFINE_TYPE_INTRUSIVE(vtkRemoteViewResultItem, GlobalID, Mime, Width, Height, Size,
    Codec, CodecLongName, PTS, KeyFrame, Stats);

  void FromJson(const vtkNJson& json) { from_json(json, *this); }

  vtkNJson ToJson() const
  {
    vtkNJson json = vtkNJson::object();
    to_json(json, *this);
    return json;
  }
};

#endif

//  VTK-HeaderTest-Exclude: vtkRemoteViewResultItem.h
