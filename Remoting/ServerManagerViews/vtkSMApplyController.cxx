/*=========================================================================

  Program:   ParaView
  Module:    vtkSMApplyController.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkSMApplyController.h"

#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPVArrayInformation.h"
#include "vtkPVXMLElement.h"
#include "vtkRemotingServerManagerViewsLogVerbosity.h"
#include "vtkSMArrayListDomain.h"
#include "vtkSMCoreUtilities.h"
#include "vtkSMParaViewPipelineControllerWithRendering.h"
#include "vtkSMProperty.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxy.h"
#include "vtkSMProxyIterator.h"
#include "vtkSMProxySelectionModel.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"
#include "vtkSMTransferFunctionManager.h"
#include "vtkSMTransferFunctionProxy.h"
#include "vtkSMViewLayoutProxy.h"
#include "vtkSMViewProxy.h"

#include <set>

namespace
{
/**
 * "Replace input" is a hint given to the GUI to turn off input visibility
 * when the filter is created.
 * Returns if this proxy replaces input on creation.
 * This checks the "Hints" for the proxy, if any. If a `<Visibility>` element
 * is present with `replace_input`. It returns its value.
 * Acceptable values are
 * - 0 : Do not hide the input
 * - 1 : Hide the input
 * - 2 : Conditionally turn off the input. The input should be turned
 *        off if the representation is surface and the opacity is 1.
 */
int ShouldHideInput(vtkSMProxy* producer)
{
  vtkPVXMLElement* hints = producer->GetHints();
  if (!hints)
  {
    return 1;
  }
  for (unsigned int cc = 0; cc < hints->GetNumberOfNestedElements(); cc++)
  {
    vtkPVXMLElement* child = hints->GetNestedElement(cc);
    if (!child || !child->GetName() || strcmp(child->GetName(), "Visibility") != 0)
    {
      continue;
    }
    int replaceInput = 1;
    if (!child->GetScalarAttribute("replace_input", &replaceInput))
    {
      continue;
    }
    return replaceInput;
  }
  return 1; // default value.
}

bool IsMarkedUpdateColoring(vtkSMProxy* proxy)
{
  return (proxy && proxy->HasAnnotation("UPDATE_COLORING") &&
    strcmp(proxy->GetAnnotation("UPDATE_COLORING"), "TRUE") == 0);
}

void UnMarkUpdateColoring(vtkSMProxy* proxy)
{
  if (proxy)
  {
    proxy->RemoveAnnotation("UPDATE_COLORING");
  }
}

}

class vtkSMApplyController::vtkInternals
{
};

vtkObjectFactoryNewMacro(vtkSMApplyController);
//----------------------------------------------------------------------------
vtkSMApplyController::vtkSMApplyController() = default;

//----------------------------------------------------------------------------
vtkSMApplyController::~vtkSMApplyController() = default;

//----------------------------------------------------------------------------
void vtkSMApplyController::MarkShowOnApply(vtkSMProxy* proxy)
{
  if (proxy)
  {
    proxy->SetAnnotation("SHOW_ON_APPLY", "TRUE");
  }
}

//----------------------------------------------------------------------------
void vtkSMApplyController::UnmarkShowOnApply(vtkSMProxy* proxy)
{
  if (proxy)
  {
    proxy->RemoveAnnotation("SHOW_ON_APPLY");
  }
}

//----------------------------------------------------------------------------
bool vtkSMApplyController::IsMarkedShowOnApply(vtkSMProxy* proxy)
{
  return (proxy && proxy->HasAnnotation("SHOW_ON_APPLY") &&
    strcmp(proxy->GetAnnotation("SHOW_ON_APPLY"), "TRUE") == 0);
}

//----------------------------------------------------------------------------
void vtkSMApplyController::Apply(vtkSMSessionProxyManager* pxm, double time)
{
  vtkNew<vtkSMProxyIterator> iter;
  iter->SetSessionProxyManager(pxm);
  iter->SetModeToOneGroup();
  struct PipelineUpdateInfo
  {
    vtkSMProxy* producer = nullptr;
    bool status = false;
  };
  std::vector<rxcpp::observable<PipelineUpdateInfo>> pipelineUpdates;
  // The first pass will show all representations in on_next handler.
  for (iter->Begin("sources"); !iter->IsAtEnd(); iter->Next())
  {
    auto* producer = vtkSMSourceProxy::SafeDownCast(iter->GetProxy());
    if (!vtkSMApplyController::IsMarkedShowOnApply(producer) ||
      producer->HasAnnotation("UNDER_INITIALIZATION"))
    {
      continue;
    }

    // ASYNC FIXME: Enable pipeline updates for different time values (animation)
    pipelineUpdates.emplace_back(
      producer->UpdatePipeline(time).map([producer](const bool& status) -> PipelineUpdateInfo {
        return PipelineUpdateInfo{ producer, status };
      }));
  }
  // Collect all representations that need their coloring updated
  for (iter->Begin("representations"); !iter->IsAtEnd(); iter->Next())
  {
    if (!IsMarkedUpdateColoring(iter->GetProxy()))
    {
      continue;
    }

    pipelineUpdates.emplace_back(
      rxcpp::sources::just(PipelineUpdateInfo{ iter->GetProxy(), true }));
  }

  rxcpp::observable<>::iterate(pipelineUpdates)
    .merge()
    .take(pipelineUpdates.size())
    .filter([](const PipelineUpdateInfo& info) { return info.status; })
    .subscribe(
      // on_next
      [this](const PipelineUpdateInfo& info) { this->FirstPass(info.producer); },
      // on_completed
      [this, pxm, time]() { this->SecondPass(pxm, time); });
}

//----------------------------------------------------------------------------
void vtkSMApplyController::FirstPass(vtkSMProxy* proxy)
{
  vtkVLogScopeF(VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY(), "%s", __func__);
  vtkNew<vtkSMApplyController> controller;

  if (auto* producer = vtkSMSourceProxy::SafeDownCast(proxy))
  {
    vtkVLogScopeF(
      VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY(), "controller->show %s", producer->GetXMLName());
    controller->Show(producer);
  }
  else if (IsMarkedUpdateColoring(proxy))
  {
    controller->ApplyScalarColoring(proxy);
    UnMarkUpdateColoring(proxy);
  }
}

//----------------------------------------------------------------------------
void vtkSMApplyController::SecondPass(vtkSMSessionProxyManager* pxm, double time)
{
  vtkVLogScopeF(VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY(), "%s", __func__);
  vtkNew<vtkSMProxyIterator> iter;
  iter->SetSessionProxyManager(pxm);
  iter->SetModeToOneGroup();
  // If the source is a filter, we hide the inputs (unless explicitly told
  // otherwise).
  for (iter->Begin("sources"); !iter->IsAtEnd(); iter->Next())
  {
    auto producer = vtkSMSourceProxy::SafeDownCast(iter->GetProxy());
    if (!producer)
    {
      continue;
    }
    vtkNew<vtkSMParaViewPipelineControllerWithRendering> controller;
    auto pxm = producer->GetSessionProxyManager();
    auto activeView = this->GetActiveView(pxm);
    // ASYNC FIXME: Uses activeView directly instead of the preferred view because
    // there is not way to get preferred view without creating a new one each time.
    // auto* preferredView =
    //   vtkSMViewProxy::SafeDownCast(controller->ShowInPreferredView(producer, 0,
    //   activeView));
    auto preferredView = activeView;
    if (preferredView)
    {
      // modify visibility only for new proxies, skip already existing ones
      if (vtkSMApplyController::IsMarkedShowOnApply(producer))
      {
        this->HideInputs(producer, preferredView);
        vtkSMApplyController::UnmarkShowOnApply(producer);
      }
    }
  }

  // Update all views.
  for (iter->Begin("views"); !iter->IsAtEnd(); iter->Next())
  {
    if (auto* view = vtkSMViewProxy::SafeDownCast(iter->GetProxy()))
    {
      // ASYNC FIXME: Do not reset camera for every single view.
      // Only the views from which representations were hidden or new representations were
      // shown need to reset camera.
      view->Update().subscribe([this, view, time](bool status) {
        if (status)
        {
          view->ResetCameraUsingVisiblePropBounds();
          vtkSMPropertyHelper helper(view, "ViewTime");
          helper.Set(time);
          view->UpdateVTKObjects();
          view->Update().subscribe([this](auto) {
            vtkVLog(VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY(), << "Updated view");
            this->InvokeEvent(ApplyEvent);
          });
        }
      });
    }
  }
}

//----------------------------------------------------------------------------
void vtkSMApplyController::Show(vtkSMSourceProxy* producer)
{
  auto* pxm = producer->GetSessionProxyManager();
  auto* activeView = this->GetActiveView(pxm);
  auto* activeLayout = activeView ? vtkSMViewLayoutProxy::FindLayout(activeView) : nullptr;

  vtkNew<vtkSMParaViewPipelineControllerWithRendering> controller;

  for (int cc = 0, max = producer->GetNumberOfOutputPorts(); cc < max; ++cc)
  {
    auto* preferredView =
      vtkSMViewProxy::SafeDownCast(controller->ShowInPreferredView(producer, cc, activeView));
    if (!preferredView)
    {
      continue;
    }

    // Assign view to layout. This has no effect if already assigned to a
    // layout.
    controller->AssignViewToLayout(preferredView, activeLayout);

    // If a new layout was created, let's use that from now on.
    activeLayout = vtkSMViewLayoutProxy::FindLayout(preferredView);

    // Set some default coloring
    this->InitializeScalarColoring(producer, cc, preferredView);
  }
}

//----------------------------------------------------------------------------
vtkSMViewProxy* vtkSMApplyController::GetActiveView(vtkSMSessionProxyManager* pxm) const
{
  auto* selModel = pxm->GetSelectionModel("ActiveView");
  return selModel ? vtkSMViewProxy::SafeDownCast(selModel->GetCurrentProxy()) : nullptr;
}

//----------------------------------------------------------------------------
void vtkSMApplyController::HideInputs(vtkSMProxy* producer, vtkSMViewProxy* view)
{
  if (producer->GetProperty("Input") == nullptr)
  {
    // if producer is not a filter, nothing to do.
    return;
  }

  const int replaceInput = ShouldHideInput(producer);
  if (replaceInput == 0)
  {
    return;
  }

  vtkSMPropertyHelper inputHelper(producer, "Input", true);
  for (int cc = 0; cc < producer->GetNumberOfProducers(); cc++)
  {
    vtkSMProxy* inputRepr = view->FindRepresentation(
      vtkSMSourceProxy::SafeDownCast(inputHelper.GetAsProxy(cc)), inputHelper.GetOutputPort(cc));
    if (inputRepr == nullptr)
    {
      // if producer's input has no representation in the view, nothing to do.
      return;
    }

    if (replaceInput == 2)
    {
      if (auto* property = inputRepr->GetProperty("Representation"))
      {
        // Conditionally turn off the input. The input should be turned
        // off if the representation is surface and the opacity is 1.
        std::string reprType = vtkSMPropertyHelper(property).GetAsString();

        double opacity = vtkSMPropertyHelper(inputRepr, "Opacity").GetAsDouble();
        if ((reprType != "Surface" && reprType != "Surface With Edges") ||
          (opacity != 0.0 && opacity < 1.0))
        {
          return;
        }
      }
    }
    vtkNew<vtkSMParaViewPipelineControllerWithRendering> controller;
    controller->Hide(inputRepr, view);
  }
}

//----------------------------------------------------------------------------
void vtkSMApplyController::ResetCamera(vtkSMViewProxy* view)
{
  view->Update().subscribe([view](bool) { view->ResetCameraUsingVisiblePropBounds(); });
}

//----------------------------------------------------------------------------
void vtkSMApplyController::ApplyScalarColoring(vtkSMProxy* repr, bool rescale)
{
  // logic is based on vtkSMPVRepresentationProxy::RescaleTransferFunctionToDataRange of ParaView
  // master
  const bool mapScalars = repr->GetProperty("MapScalars")
    ? vtkSMPropertyHelper(repr, "MapScalars").GetAsInt() == 1
    : true;
  if (!mapScalars)
  {
    return;
  }

  vtkSMPropertyHelper colorArray(repr, "ColorArrayName");
  if (colorArray.GetInputArrayNameToProcess() == nullptr)
  {
    return;
  }

  auto* domain = colorArray.GetProperty()->FindDomain<vtkSMArrayListDomain>();
  if (!domain)
  {
    return;
  }

  // get information about array extends
  auto* arrayInfo = domain->GetArrayInformation(
    colorArray.GetInputArrayAssociation(), colorArray.GetInputArrayNameToProcess());

  if (!arrayInfo)
  {
    return;
  }

  // find or create if it does not exists the color transfer function associated with this array
  vtkNew<vtkSMTransferFunctionManager> manager;
  vtkSMProxy* scalarColorFunction = manager->GetColorTransferFunction(
    colorArray.GetInputArrayNameToProcess(), repr->GetProxyManager());

  int component = vtkSMPropertyHelper(scalarColorFunction, "VectorMode").GetAsInt() != 0
    ? vtkSMPropertyHelper(scalarColorFunction, "VectorComponent").GetAsInt()
    : -1;

  if (component >= arrayInfo->GetNumberOfComponents())
  {
    return;
  }

  if (rescale)
  {
    double range[2];
    arrayInfo->GetComponentFiniteRange(component, range);
    vtkSMCoreUtilities::AdjustRange(range);
    vtkSMTransferFunctionProxy::RescaleTransferFunction(
      scalarColorFunction, range, /*extend=*/true);

    if (scalarColorFunction->GetProperty("ScalarOpacityFunction"))
    {
      vtkSMProxy* sof = manager->GetOpacityTransferFunction(
        colorArray.GetInputArrayNameToProcess(), scalarColorFunction->GetProxyManager());
      if (sof)
      {
        vtkSMTransferFunctionProxy::RescaleTransferFunction(sof, range, /*extend=*/true);
        sof->UpdateVTKObjects();
      }
    }
    scalarColorFunction->UpdateVTKObjects();
    vtkSMPropertyHelper(repr, "LookupTable").Set(scalarColorFunction);
    repr->UpdateVTKObjects();
  }
}

//----------------------------------------------------------------------------
void vtkSMApplyController::InitializeScalarColoring(
  vtkSMProxy* producer, unsigned int outputPort, vtkSMViewProxy* view)
{
  vtkSMProxy* repr = view->FindRepresentation(vtkSMSourceProxy::SafeDownCast(producer), outputPort);
  if (repr == nullptr)
  {
    // if producer's input has no representation in the view, nothing to do.
    return;
  }
  bool hasArray = false;
  vtkSMPropertyHelper colorArrayHelper(repr, "ColorArrayName");
  if (colorArrayHelper.GetInputArrayNameToProcess() != nullptr &&
    colorArrayHelper.GetInputArrayNameToProcess()[0] != '\0')
  {
    // a color array is already set
    hasArray = true;
  }
  const char* arrayName = nullptr;
  int association = 0;
  if (hasArray)
  {
    arrayName = colorArrayHelper.GetInputArrayNameToProcess();
    association = colorArrayHelper.GetInputArrayAssociation();
  }
  else
  {

    auto* colorArrayProperty = colorArrayHelper.GetProperty();
    auto* domain = colorArrayProperty->FindDomain<vtkSMArrayListDomain>();
    if (!domain)
    {
      vtkLogF(ERROR, "Cannot select color array without an associated ArrayListDomain");
      return;
    }
    std::vector<int> candidateArraysIndicies;
    for (unsigned int idx = 0; idx < domain->GetNumberOfStrings(); idx++)
    {
      association = domain->GetFieldAssociation(idx);
      arrayName = domain->GetString(idx);
      if (association != vtkDataObject::FIELD_ASSOCIATION_POINTS)
      {
        continue;
      }
      auto* arrayInfo = domain->GetArrayInformation(association, arrayName);
      if (arrayInfo && arrayInfo->GetNumberOfComponents() == 1)
      {
        candidateArraysIndicies.push_back(idx);
      }
    }

    if (candidateArraysIndicies.empty())
    {
      return;
    }
    // for now we use the first matching
    const int selected = candidateArraysIndicies[0];
    association = domain->GetFieldAssociation(selected);
    arrayName = domain->GetString(selected);

    colorArrayHelper.SetInputArrayToProcess(association, arrayName);
  }

  vtkNew<vtkSMTransferFunctionManager> mgr;
  vtkSMSessionProxyManager* pxm = producer->GetSessionProxyManager();
  vtkSMProxy* lut = mgr->GetColorTransferFunction(arrayName, pxm);
  vtkSMPropertyHelper(repr, "LookupTable").Set(lut);
  if (repr->GetProperty("ScalarOpacityFunction"))
  {
    auto* sof = vtkSMPropertyHelper(lut, "ScalarOpacityFunction").GetAsProxy();
    vtkSMPropertyHelper(repr, "ScalarOpacityFunction").Set(sof);
  }

  this->ApplyScalarColoring(repr);
}

//----------------------------------------------------------------------------
void vtkSMApplyController::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
