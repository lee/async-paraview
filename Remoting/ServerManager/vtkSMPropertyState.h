/*=========================================================================

  Program:   ParaView
  Module:    vtkSMPropertyState.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @class vtkSMPropertyState
 * @brief encapsulates the state of a vtkSMProperty instance.
 *
 * vtkSMPropertyState internally maintains a json representation of all properties and their values.
 * You may query/edit the property values using the public functions.
 *
 * @sa vtkSMState, vtkSMProxyState
 */

#ifndef vtkSMPropertyState_h
#define vtkSMPropertyState_h

#include "vtkIndent.h"                      // for Print
#include "vtkNJson.h"                       // for vtkNJson
#include "vtkRemotingServerManagerModule.h" // for export macro
#include "vtkVariant.h"                     // for value

#include <cstddef> // for size_t
#include <memory>  // for unique_ptr
#include <ostream> // for Print
#include <string>  // for name and id

class vtkStringArray;

/**
 * Helper class to set/get the InputProperty of a proxy.
 */
class VTKREMOTINGSERVERMANAGER_EXPORT vtkSMInputPropertyStateValue : public vtkObject
{
public:
  static vtkSMInputPropertyStateValue* New();
  vtkTypeMacro(vtkSMInputPropertyStateValue, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  vtkGetMacro(Producer, vtkTypeUInt32);
  vtkSetMacro(Producer, vtkTypeUInt32);

  vtkGetMacro(PortIndex, unsigned int);
  vtkSetMacro(PortIndex, unsigned int);

protected:
  vtkSMInputPropertyStateValue();
  ~vtkSMInputPropertyStateValue() override;

  vtkTypeUInt32 Producer = 0;
  unsigned int PortIndex = 0;

private:
  vtkSMInputPropertyStateValue(const vtkSMInputPropertyStateValue&) = delete;
  void operator=(const vtkSMInputPropertyStateValue&) = delete;
};

class VTKREMOTINGSERVERMANAGER_EXPORT vtkSMPropertyState
{
public:
  /**
   * Default constructor to create an empty vtkSMPropertyState.
   */
  vtkSMPropertyState();

  ///@{
  /**
   * Constructors to create a vtkSMPropertyState from a vtkNJson object.
   */
  vtkSMPropertyState(const vtkNJson& json);
  vtkSMPropertyState(vtkNJson&& json);
  ///@}

  ~vtkSMPropertyState();

  vtkSMPropertyState(const vtkSMPropertyState&) = default;
  vtkSMPropertyState& operator=(const vtkSMPropertyState&) = default;

  vtkSMPropertyState(vtkSMPropertyState&&) noexcept = default;
  vtkSMPropertyState& operator=(vtkSMPropertyState&&) noexcept = default;

  const vtkNJson& GetJSON() const;
  void Print(std::ostream& out) const;
  std::string DumpJSON() const;

  std::string GetName() const;
  void SetName(const std::string& value);

  std::size_t GetNumberOfValues() const;
  vtkVariant GetValue(std::size_t index) const;
  void SetValue(std::size_t index, const vtkVariant& value);

private:
  std::shared_ptr<vtkNJson> JSON;
};

inline ostream& operator<<(ostream& os, vtkSMPropertyState& state)
{
  state.Print(os);
  return os;
}
#endif // vtkSMPropertyState_h
