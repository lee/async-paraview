vtk_module_test_data(
      Data/can.ex2
)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  --client
  TestClientApplication,TestPVApplication.cxx,NO_VALID)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  --server
  TestServerApplication,TestPVApplication.cxx,NO_VALID)

vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
  TestDataArraySelectionWrapping.cxx,NO_VALID
  TestProxyDefinitionsIterator.cxx,NO_VALID
  TestSMInputProperty.cxx,NO_VALID)

# TODO
#vtk_add_test_cxx(vtkRemotingServerManagerCxxTests tests
#  --batch
#  TestBatchApplication,TestPVApplication.cxx,NO_VALID)


vtk_test_cxx_executable(vtkRemotingServerManagerCxxTests tests)
