/*=========================================================================

  Program:   ParaView
  Module:    vtkSMProxyState.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkSMProxyState
 * @brief   Encapsulates the state of a vtkSMProxy.
 *
 * vtkSMProxyState internally maintains a json representation of the proxy,
 * all properties and their values.
 * You may query the group, name, ID and property state using the public functions.
 *
 * @sa vtkSMState, vtkSMPropertyState
 */

#ifndef vtkSMProxyState_h
#define vtkSMProxyState_h

#include "vtkNJson.h"                       // for arg
#include "vtkRemotingServerManagerModule.h" // for export macro
#include "vtkSMPropertyState.h"             // for properties
#include "vtkType.h"                        // for vtkTypeUInt32

#include <memory>  // for unique_ptr
#include <ostream> // for Print
#include <string>  // for names

class VTKREMOTINGSERVERMANAGER_EXPORT vtkSMProxyState
{
public:
  /**
   * Default constructor to create an empty vtkSMProxyState.
   */
  vtkSMProxyState();

  ///@{
  /**
   * Constructors to create a vtkSMProxyState from a vtkNJson object.
   */
  vtkSMProxyState(const vtkNJson& json);
  vtkSMProxyState(vtkNJson&& json);
  ///@}

  ~vtkSMProxyState();

  vtkSMProxyState(const vtkSMProxyState&) = default;
  vtkSMProxyState& operator=(const vtkSMProxyState&) = default;

  vtkSMProxyState(vtkSMProxyState&&) noexcept = default;
  vtkSMProxyState& operator=(vtkSMProxyState&&) noexcept = default;

  const vtkNJson& GetJSON() const;
  void Print(std::ostream& out) const;
  std::string DumpJSON() const;

  std::string GetGroupName() const;
  void SetGroupName(const std::string& value);

  std::string GetName() const;
  void SetName(const std::string& value);

  std::string GetRegistrationGroupName() const;
  void SetRegistrationGroupName(const std::string& value);

  std::string GetRegistrationName() const;
  void SetRegistrationName(const std::string& value);

  vtkTypeUInt32 GetID() const;
  void SetID(const vtkTypeUInt32& value);

  void SetPropertyState(const std::string& name, const vtkSMPropertyState& state);
  vtkSMPropertyState GetPropertyState(const std::string& name) const;

private:
  std::shared_ptr<vtkNJson> JSON;
};

inline ostream& operator<<(ostream& os, vtkSMProxyState& state)
{
  state.Print(os);
  return os;
}
#endif // vtkSMProxyState_h
