/*=========================================================================

  Program:   ParaView
  Module:    vtkSMProxyState.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkSMProxyState.h"
#include "vtkSMPropertyState.h"

//-----------------------------------------------------------------------------
vtkSMProxyState::vtkSMProxyState()
  : JSON(std::make_shared<vtkNJson>())
{
}

//-----------------------------------------------------------------------------
vtkSMProxyState::vtkSMProxyState(const vtkNJson& json)
  : JSON(std::make_shared<vtkNJson>(json))
{
}

//-----------------------------------------------------------------------------
vtkSMProxyState::vtkSMProxyState(vtkNJson&& json)
  : JSON(std::make_shared<vtkNJson>(std::move(json)))
{
}

//-----------------------------------------------------------------------------
vtkSMProxyState::~vtkSMProxyState() = default;

//-----------------------------------------------------------------------------
const vtkNJson& vtkSMProxyState::GetJSON() const
{
  return *this->JSON;
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::Print(std::ostream& out) const
{
  out << this->JSON->dump(1) << std::endl;
}

//-----------------------------------------------------------------------------
std::string vtkSMProxyState::DumpJSON() const
{
  return this->JSON->dump();
}

//-----------------------------------------------------------------------------
std::string vtkSMProxyState::GetGroupName() const
{
  return this->JSON->at("group").get<std::string>();
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::SetGroupName(const std::string& value)
{
  (*this->JSON)["group"] = value;
}

//-----------------------------------------------------------------------------
std::string vtkSMProxyState::GetName() const
{
  return this->JSON->at("name").get<std::string>();
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::SetName(const std::string& value)
{
  (*this->JSON)["name"] = value;
}

//-----------------------------------------------------------------------------
std::string vtkSMProxyState::GetRegistrationGroupName() const
{
  return this->JSON->at("registration_group").get<std::string>();
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::SetRegistrationGroupName(const std::string& value)
{
  (*this->JSON)["registration_group"] = value;
}

//-----------------------------------------------------------------------------
std::string vtkSMProxyState::GetRegistrationName() const
{
  return this->JSON->at("registration_name").get<std::string>();
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::SetRegistrationName(const std::string& value)
{
  (*this->JSON)["registration_name"] = value;
}

//-----------------------------------------------------------------------------
vtkTypeUInt32 vtkSMProxyState::GetID() const
{
  return this->JSON->at("id").get<vtkTypeUInt32>();
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::SetID(const vtkTypeUInt32& value)
{
  (*this->JSON)["id"] = value;
}

//-----------------------------------------------------------------------------
vtkSMPropertyState vtkSMProxyState::GetPropertyState(const std::string& name) const
{
  return this->JSON->at("properties").at(name);
}

//-----------------------------------------------------------------------------
void vtkSMProxyState::SetPropertyState(const std::string& name, const vtkSMPropertyState& state)
{
  this->JSON->at("properties")[name] = state.GetJSON();
}
