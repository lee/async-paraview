/*=========================================================================

  Program:   ParaView
  Module:    vtkSMState.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkSMState.h"
#include "vtkSMProxyState.h"

//-----------------------------------------------------------------------------
vtkSMState::vtkSMState()
  : JSON(std::make_shared<vtkNJson>())
{
}

//-----------------------------------------------------------------------------
vtkSMState::vtkSMState(const vtkNJson& json)
  : JSON(std::make_shared<vtkNJson>(json))
{
}

//-----------------------------------------------------------------------------
vtkSMState::vtkSMState(vtkNJson&& json)
  : JSON(std::make_shared<vtkNJson>(std::move(json)))
{
}

//-----------------------------------------------------------------------------
vtkSMState::~vtkSMState() = default;

//-----------------------------------------------------------------------------
const vtkNJson& vtkSMState::GetJSON() const
{
  return this->GetJSON();
}

//-----------------------------------------------------------------------------
void vtkSMState::Print(std::ostream& out) const
{
  out << this->JSON->dump(1) << std::endl;
}

//-----------------------------------------------------------------------------
std::string vtkSMState::DumpJSON() const
{
  return this->JSON->dump();
}

//-----------------------------------------------------------------------------
int vtkSMState::GetVersionMajor() const
{
  return this->JSON->at("version_major").get<int>();
}

//-----------------------------------------------------------------------------
void vtkSMState::SetVersionMajor(int value)
{
  (*this->JSON)["version_major"] = value;
}

//-----------------------------------------------------------------------------
int vtkSMState::GetVersionMinor() const
{
  return this->JSON->at("version_minor").get<int>();
}

//-----------------------------------------------------------------------------
void vtkSMState::SetVersionMinor(int value)
{
  (*this->JSON)["version_minor"] = value;
}

//-----------------------------------------------------------------------------
int vtkSMState::GetVersionPatch() const
{
  return this->JSON->at("version_patch").get<int>();
}

//-----------------------------------------------------------------------------
void vtkSMState::SetVersionPatch(int value)
{
  (*this->JSON)["version_patch"] = value;
}

//-----------------------------------------------------------------------------
vtkSMProxyState vtkSMState::GetProxyState(const std::string& proxyName) const
{
  return this->JSON->at(proxyName);
}

//-----------------------------------------------------------------------------
vtkSMProxyState vtkSMState::GetProxyState(const char* proxyName) const
{
  if (proxyName != nullptr)
  {
    return this->JSON->at(proxyName);
  }
  return {};
}

//-----------------------------------------------------------------------------
vtkSMProxyState vtkSMState::GetProxyState(vtkTypeUInt32 uid) const
{
  auto proxyName = this->JSON->at(std::to_string(uid)).get<std::string>();
  return this->JSON->at(proxyName);
}

//-----------------------------------------------------------------------------
void vtkSMState::SetProxyState(
  const std::string& proxyName, const vtkTypeUInt32& uid, const vtkSMProxyState& state)
{
  auto key = std::to_string(uid);
  if (this->JSON->count(key))
  {
    const auto& name = this->JSON->at(key).get<std::string>();
    if (name != proxyName)
    { // erase the entry for the previous name at uid.
      this->JSON->erase(name);
    }
  }
  else
  {
    // create a new uid <-> proxyName entry.
    (*this->JSON)[key] = proxyName;
  }
  // store the state
  (*this->JSON)[proxyName] = state.GetJSON();
}
