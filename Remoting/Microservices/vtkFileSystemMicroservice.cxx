/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkFileSystemMicroservice.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkFileSystemMicroservice.h"

#include "vtkClientSession.h"
#include "vtkCollection.h"
#include "vtkCollectionIterator.h"
#include "vtkLocalFileSystemProvider.h"
#include "vtkPVCoreApplication.h"
#include "vtkPVFileInformation.h"
#include "vtkPacket.h"
#include "vtkRemoteFileSystemProvider.h"
#include "vtkRemotingCoreUtilities.h"
#include "vtkSmartPointer.h"
#include "vtksys/SystemTools.hxx"

#include <set>

bool StartsWith(const std::string& path, const std::string& token)
{
  // rfind(token,position) find the last occurance of token in path starting at
  // position 0 and proceeding from right to left could be replaced with
  // starts_with in C++20
  return path.rfind(token, 0) == 0;
}

class vtkFileSystemMicroservice::vtkInternals
{
public:
  const std::thread::id OwnerTID{ std::this_thread::get_id() };
  // FIXME ideally vtkWeakPointer but threadsafe
  vtkClientSession* Session = { nullptr };
  bool SymmetricMPIMode = false;

  // Normally, providers are part of services which reside on the server side.
  // However, filesystem browsing is special because we want to perfom the
  // "processing" on the client side too. To keep the duplication minimal we use a
  // local filesystem provider and communicate to it through packets even
  // though it is local.
  vtkNew<vtkLocalFileSystemProvider> LocalProvider;

  //-----------------------------------------------------------------------------

  bool IsInitialized() const { return Session != nullptr; }

  vtkTypeUInt32 EnumToServiceLocation(vtkFileSystemMicroservice::Location location) const
  {
    switch (location)
    {
      case Location::DATA_SERVICE:
        return vtkClientSession::DATA_SERVER;

      case Location::RENDER_SERVICE:
        return vtkClientSession::RENDER_SERVER;

      case Location::CLIENT:
      default:
        return 0;
    }
  }

  // Based on whether we are querying the local or remote filesystem we execute
  // the command using the local filesystem provider of this class or send a
  // request to access the remote one.
  rxcpp::observable<vtkPacket> CreateResponseObservable(
    const vtkPacket& packet, vtkFileSystemMicroservice::Location location) const
  {
    if (location == Location::CLIENT)
    {
      if (this->SymmetricMPIMode)
      {
        return rxcpp::observable<>::error<vtkPacket>(
          std::runtime_error("Local Provider should not be used in SymmetricMPIMode , it will "
                             "create data races among the processes"));
      }
      return this->LocalProvider->ProcessLocally(std::move(packet));
    }
    else
    {
      return this->Session->SendRequest(this->EnumToServiceLocation(location), std::move(packet));
    }
  }
};

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSmartPointer<vtkPVFileInformation>> vtkFileSystemMicroservice::ListDirectory(
  const std::string& absolutePath, vtkFileSystemMicroservice::Location location) const
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<vtkSmartPointer<vtkPVFileInformation>>(
      std::runtime_error("Session was not set!"));
  }

  auto fileInfoRequest = vtkSmartPointer<vtkPVFileInformation>::New();
  fileInfoRequest->Initialize();
  fileInfoRequest->SetListDirectories(true);
  fileInfoRequest->SetPath(absolutePath);
  fileInfoRequest->SetReadDetailedFileInformation(true);

  auto packet =
    vtkRemoteFileSystemProvider::ListDirectory(fileInfoRequest, internals.SymmetricMPIMode);

  return internals.CreateResponseObservable(packet, location)
    .map([&internals, location](const vtkPacket& packet) {
      auto fileInfoResponse = vtk::TakeSmartPointer(vtkPVFileInformation::New());
      vtkRemoteFileSystemProvider::ParseResponse(packet, fileInfoResponse);
      return fileInfoResponse;
    })
    .take(1); // this is a single value observable
}

//-----------------------------------------------------------------------------
rxcpp::observable<bool> vtkFileSystemMicroservice::MakeDirectory(
  const std::string& absolutePath, vtkFileSystemMicroservice::Location location) const
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<bool>(std::runtime_error("Session was not set!"));
  }

  auto packet =
    vtkRemoteFileSystemProvider::MakeDirectory(absolutePath, internals.SymmetricMPIMode);
  return internals.CreateResponseObservable(packet, location)
    .map([](const vtkPacket& packet) { return packet.GetJSON().value("status", false); })
    .take(1);
}

//-----------------------------------------------------------------------------
rxcpp::observable<bool> vtkFileSystemMicroservice::Remove(
  const std::string& absolutePath, vtkFileSystemMicroservice::Location location) const
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<bool>(std::runtime_error("Session was not set!"));
  }

  auto packet = vtkRemoteFileSystemProvider::Remove(absolutePath, internals.SymmetricMPIMode);
  return internals.CreateResponseObservable(packet, location)
    .map([](const vtkPacket& packet) { return packet.GetJSON().value("status", false); })
    .take(1);
}

//-----------------------------------------------------------------------------
rxcpp::observable<bool> vtkFileSystemMicroservice::Rename(const std::string& oldAbsolutePath,
  const std::string& newAbsolutePath, vtkFileSystemMicroservice::Location location) const
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<bool>(std::runtime_error("Session was not set!"));
  }

  auto packet = vtkRemoteFileSystemProvider::Rename(
    oldAbsolutePath, newAbsolutePath, internals.SymmetricMPIMode);
  return internals.CreateResponseObservable(packet, location)
    .map([](const vtkPacket& packet) { return packet.GetJSON().value("status", false); })
    .take(1);
}

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkFileSystemMicroservice);
//-----------------------------------------------------------------------------
vtkFileSystemMicroservice::vtkFileSystemMicroservice()
  : Internals(new vtkFileSystemMicroservice::vtkInternals())
{
}

//-----------------------------------------------------------------------------
vtkFileSystemMicroservice::~vtkFileSystemMicroservice() = default;

//-----------------------------------------------------------------------------
void vtkFileSystemMicroservice::SetSession(vtkClientSession* session)
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  if (internals.Session != session)
  {
    internals.Session = session;
    internals.SymmetricMPIMode = vtkPVCoreApplication::GetInstance()->GetSymmetricMPIMode();
  }
}

//-----------------------------------------------------------------------------
vtkClientSession* vtkFileSystemMicroservice::GetSession() const
{
  const auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  return internals.Session;
}

//-----------------------------------------------------------------------------
rxcpp::observable<bool> vtkFileSystemMicroservice::AddRootDirectory(const std::string& mappedPath,
  const std::string& realPath, vtkFileSystemMicroservice::Location location) const
{
  const auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);

  auto packet =
    vtkRemoteFileSystemProvider::AddRootDirectory(mappedPath, realPath, internals.SymmetricMPIMode);
  return internals.CreateResponseObservable(packet, location)
    .map([](const vtkPacket& packet) { return packet.GetJSON().value("status", false); })
    .take(1);
}

//-----------------------------------------------------------------------------
rxcpp::observable<bool> vtkFileSystemMicroservice::RemoveRootDirectory(
  const std::string& mappedPath, vtkFileSystemMicroservice::Location location) const
{
  const auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);

  auto packet =
    vtkRemoteFileSystemProvider::RemoveRootDirectory(mappedPath, internals.SymmetricMPIMode);
  return internals.CreateResponseObservable(packet, location)
    .map([](const vtkPacket& packet) { return packet.GetJSON().value("status", false); })
    .take(1);
}

//-----------------------------------------------------------------------------
void vtkFileSystemMicroservice::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
