vtk_module_test_data(
  Data/can.e.4/can.e.4.0
  Data/can.e.4/can.e.4.1
  Data/can.e.4/can.e.4.2
  Data/can.e.4/can.e.4.3
  Data/rock.vti
  Data/FileSeries/,REGEX:.*
)

set(src_without_baselines
  TestActiveObjectsMicroservice.cxx
  TestDataFileMicroserviceOpenGroup.cxx
  TestDataFileMicroserviceOpenSeries.cxx
  TestDataFileMicroserviceOpenSingle.cxx
  TestFileSystemMicroservice.cxx
  TestFileSystemMicroserviceRestrictBrowsing.cxx
  TestPipelineBuilderMicroservice.cxx
  TestPipelineViewerMicroservice.cxx
  TestStateMicroservice.cxx
  TestSelectionMicroservice.cxx
)


set(TestSelectionMicroservice_NUMPROCS 1)
# make sure the test directory is set for all versions of the tests (builtin,client-server,client-server-mpi)
set(TestFileSystemMicroservice_ARGS "--test-directory=${CMAKE_BINARY_DIR}/Testing/Temporary")
set(TestFileSystemMicroserviceRestrictBrowsing_ARGS "--test-directory=${CMAKE_BINARY_DIR}/Testing/Temporary")

vtk_add_test_cxx(vtkRemotingMicroservicesCxxTests tests
  NO_VALID NO_OUTPUT
  ${src_without_baselines}
)

if (TARGET AsyncParaView::RemotingServerManagerViews)
  set(src_with_baseline
    TestPipelineBuilderMicroserviceWithRendering.cxx)
  vtk_add_test_cxx(vtkRemotingMicroservicesCxxTests tests
    NO_DATA
    ${src_with_baseline}
    )
endif()

vtk_test_cxx_executable(vtkRemotingMicroservicesCxxTests tests)

foreach(test IN LISTS src_without_baselines)
  get_filename_component(name ${test} NAME_WE)
  parat_add_client_server_tests(
    TEST_NAME ${name}
    CLIENT vtkRemotingMicroservicesCxxTests
    CLIENT_ARGS ${name}
    SERVER_ARGS "--timeout=100"
  )
endforeach()

foreach(test IN LISTS src_with_baseline)
  message(STATUS "APV_TEST_BASELINE_DIR: ${APV_TEST_BASELINE_DIR}")
  get_filename_component(name ${test} NAME_WE)
  parat_add_client_server_tests(
    TEST_NAME ${name}
    CLIENT vtkRemotingMicroservicesCxxTests
    BASELINE_DIR ${APV_TEST_BASELINE_DIR}
    CLIENT_ARGS ${name}
    SERVER_ARGS "--timeout=100"
  )
endforeach()
