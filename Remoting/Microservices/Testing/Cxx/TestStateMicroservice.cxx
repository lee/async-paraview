/*=========================================================================

Program:   ParaView
Module:    TestStateMicroservice.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkStateMicroservice. Sets up a sphere source and saves the state of server manager.
 */

#include "vtkClientSession.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPVCoreApplication.h"
#include "vtkProxyDefinitionManager.h"
#include "vtkSMParaViewPipelineController.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSmartPointer.h"
#include "vtkStateMicroservice.h"

#include <chrono>
#include <cstdlib>
#include <exception>
#include <thread>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

rxcpp::observable<vtkSMProxy*> CreateProxy(
  vtkClientSession* session, const std::string& groupname, const std::string& name)
{
  vtkSMSessionProxyManager* pxm = session->GetProxyManager();
  vtkProxyDefinitionManager* pdm = pxm->GetProxyDefinitionManager();

  vtkSMProxy* proxy = nullptr;
  if (pxm && pxm->GetPrototypeProxy(groupname.c_str(), name.c_str()))
  {
    proxy = pxm->NewProxy(groupname.c_str(), name.c_str());
  }
  vtkNew<vtkSMParaViewPipelineController> controller;
  return controller->InitializeProxy(proxy)
    .map([proxy, name, groupname, pxm](bool status) {
      if (status)
      {
        pxm->RegisterProxy(groupname.c_str(), name.c_str(), proxy);
        proxy->UpdateVTKObjects();
        vtkLog(INFO, << "Initialized proxy - " << name << " group - " << groupname);
        return proxy;
      }
      else
      {
        std::stringstream errorMsg;
        errorMsg << "Failed to initialize proxy : " << name << " of group " << groupname;
        throw std::runtime_error(errorMsg.str());
      }
    })
    .take(1);
}

bool DoStateMicroserviceTest(vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{
  auto proxyMaker = [&session](
                      const char* groupname, const char* name) -> rxcpp::observable<vtkSMProxy*> {
    return CreateProxy(session, groupname, name);
  };

  vtkSmartPointer<vtkSMProxy> sphereProxy = nullptr;
  vtkNew<vtkStateMicroservice> stateSaver;
  auto app = vtkPVCoreApplication::GetInstance();

  bool done = false;
  vtkTypeUInt32 gid = 0;
  proxyMaker("sources", "SphereSource")
    .subscribe(
      // on_next
      [&sphereProxy, &done, &gid](vtkSMProxy* proxy) {
        sphereProxy = vtk::TakeSmartPointer(proxy);
        vtkLog(INFO, << "Sphere MTime "
                     << sphereProxy->GetMTime()); // silly test to see if its not nullptr.
        gid = sphereProxy->GetGlobalID();
        done = true;
      },
      // on_error
      [](rxcpp::util::error_ptr ep) { vtkLog(ERROR, << rxcpp::util::what(ep)); });

  app->ProcessEventsUntil(runLoop, [&done]() { return done; });

  // Try with a session.
  stateSaver->SetSession(session);
  auto appState = stateSaver->Save();

  VALIDATE(appState.GetVersionMajor() == vtkPVCoreApplication::GetVersionMajor());
  VALIDATE(appState.GetVersionMinor() == vtkPVCoreApplication::GetVersionMinor());
  VALIDATE(appState.GetVersionPatch() == vtkPVCoreApplication::GetVersionPatch());
  VALIDATE(appState.GetProxyState(gid).GetGroupName() == "sources");
  VALIDATE(appState.GetProxyState(gid).GetID() == gid);
  VALIDATE(appState.GetProxyState(gid).GetName() == "SphereSource");
  VALIDATE(appState.GetProxyState(gid).GetRegistrationGroupName() == "sources");
  VALIDATE(appState.GetProxyState(gid).GetRegistrationName() == "SphereSource");
  VALIDATE(appState.GetProxyState(gid).GetPropertyState("Center").GetNumberOfValues() == 3);
  VALIDATE(appState.GetProxyState(gid).GetPropertyState("Center").GetValue(0) == 0.0);
  VALIDATE(appState.GetProxyState(gid).GetPropertyState("Center").GetValue(1) == 0.0);
  VALIDATE(appState.GetProxyState(gid).GetPropertyState("Center").GetValue(2) == 0.0);
  return true;
}

int TestStateMicroservice(int argc, char* argv[])
{
  // Create the primary event-loop for the application. In a Qt application,
  // vtkPVGUIApplication creates one for us using rxqt::run_loop.
  rxcpp::schedulers::run_loop runLoop;

  // Initialize the MPI environment.
  vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  // This test does not deal with a GUI.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestStateMicroservice application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
    app->GetOptions()->SetLogStdErrVerbosity(vtkLogger::GetCurrentVerbosityCutoff());
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    if (id == 0)
    {
      app->Exit(1);
    }
    else
    {
      const bool success = DoStateMicroserviceTest(app->GetSession(id), runLoop);
      app->Await(
        runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  });
  return app->Run(runLoop);
}
