/*=========================================================================

Program:   ParaView
Module:    TestDataFileMicroserviceOpenSingle.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkDataFileMicroservice::Open(filename)
 * Validates that the microservice is able to determine possible file readers for a filename.
 */
#include "vtkClientSession.h"
#include "vtkDataFileMicroservice.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPVCoreApplication.h"
#include "vtkPVDataInformation.h"
#include "vtkSmartPointer.h"
#include "vtkTestUtilities.h"

#include <cstdlib>
#include <vector>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

namespace
{
std::string filename;
};

bool DoDataFileMicroserviceOpenSingleTest(
  vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{
  vtkNew<vtkDataFileMicroservice> dataFileMicroService;
  dataFileMicroService->SetSession(session);

  auto app = vtkPVCoreApplication::GetInstance();

  auto results = app->Await(runLoop, dataFileMicroService->FindPossibleReaders(::filename));
#ifndef NDEBUG
  for (auto it = results->GetPointer(0); it < results->GetPointer(results->GetNumberOfValues());
       ++it)
  {
    std::cout << *it << std::endl;
  }
#endif
  VALIDATE(results->GetNumberOfValues() == 1);
  VALIDATE(results->GetValue(0) == "sources,XMLImageDataReader")

  auto reader = vtk::TakeSmartPointer(app->Await(runLoop, dataFileMicroService->Open(::filename)));
  auto success = app->Await(runLoop, reader->UpdatePipeline(0));
  auto dataInfoRequest = vtkSmartPointer<vtkPVDataInformation>::New();
  dataInfoRequest->SetPortNumber(0);
  // if on MPI we need to gather data from all ranks
  auto di = vtk::MakeSmartPointer(vtkPVDataInformation::SafeDownCast(
    app->Await(runLoop, reader->GatherInformation(dataInfoRequest))));
#ifndef NDEBUG
  di->Print(std::cout);
#endif
  VALIDATE(di->GetDataSetType() == VTK_IMAGE_DATA);
  VALIDATE(di->GetNumberOfCells() == 16917660);
  return success;
}

int TestDataFileMicroserviceOpenSingle(int argc, char* argv[])
{

  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestDataFileMicroserviceOpenSingle application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  if (options->GetDataDirectory().empty())
  {
    char* name = vtkTestUtilities::ExpandDataFileName(argc, argv, "Testing/Data/rock.vti");
    ::filename = name;
    delete[] name;
  }
  else
  {
    ::filename = options->GetDataDirectory() + "/Testing/Data/rock.vti";
  }

  vtkTypeUInt32 id = app->Await(runLoop, sessionIdObservable);
  const bool success = DoDataFileMicroserviceOpenSingleTest(app->GetSession(id), runLoop);
  app->Await(runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
  app->Finalize();
  app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
  return app->GetExitCode();
}
