/*=========================================================================

Program:   ParaView
Module:    TestDataFileMicroserviceOpenGroup.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkDataFileMicroservice::Open(filegroup)
 * Validates that the microservice is able to determine possible file readers for a filegroup.
 */
#include "vtkClientSession.h"
#include "vtkDataFileMicroservice.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPVCoreApplication.h"
#include "vtkPVDataInformation.h"
#include "vtkSmartPointer.h"
#include "vtkTestUtilities.h"
#include "vtkType.h"

#include <cstdlib>
#include <vector>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

namespace
{
std::vector<std::string> filegroup;
};

bool DoDataFileMicroserviceOpenGroupTest(
  vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{
  vtkNew<vtkDataFileMicroservice> dataFileMicroService;
  dataFileMicroService->SetSession(session);

  auto app = vtkPVCoreApplication::GetInstance();

  auto results = app->Await(runLoop, dataFileMicroService->FindPossibleReaders(::filegroup));
#ifndef NDEBUG
  for (auto it = results->GetPointer(0); it < results->GetPointer(results->GetNumberOfValues());
       ++it)
  {
    std::cout << *it << std::endl;
  }
#endif
  VALIDATE(results->GetNumberOfValues() == 1);
  VALIDATE(results->GetValue(0) == "sources,IOSSReader")

  auto reader = vtk::TakeSmartPointer(app->Await(runLoop, dataFileMicroService->Open(::filegroup)));
  auto success = app->Await(runLoop, reader->UpdatePipeline(0));
  auto dataInfoRequest = vtkSmartPointer<vtkPVDataInformation>::New();
  dataInfoRequest->SetPortNumber(0);
  // if on MPI we need to gather data from all ranks
  auto di = vtk::MakeSmartPointer(vtkPVDataInformation::SafeDownCast(
    app->Await(runLoop, reader->GatherInformation(dataInfoRequest))));
#ifndef NDEBUG
  di->Print(std::cout);
#endif
  VALIDATE(di->GetDataSetType() == VTK_UNSTRUCTURED_GRID);
  VALIDATE(di->GetNumberOfDataSets() == 5);
  VALIDATE(di->GetNumberOfPoints() == 10516);
  VALIDATE(di->GetNumberOfCells() == 7152);
  return success;
}

int TestDataFileMicroserviceOpenGroup(int argc, char* argv[])
{

  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestDataFileMicroserviceOpenGroup application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  if (options->GetDataDirectory().empty())
  {
    for (int i = 0; i < 4; ++i)
    {
      std::string filename = "Testing/Data/can.e.4/can.e.4." + std::to_string(i);
      char* name = vtkTestUtilities::ExpandDataFileName(argc, argv, filename.c_str());
      ::filegroup.emplace_back(name);
      delete[] name;
    }
  }
  else
  {
    for (int i = 0; i < 4; ++i)
    {
      std::string filename =
        options->GetDataDirectory() + "/Testing/Data/can.e.4/can.e.4." + std::to_string(i);
      ::filegroup.emplace_back(filename);
    }
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    if (id == 0)
    {
      app->Exit(1);
    }
    else
    {
      const bool success = DoDataFileMicroserviceOpenGroupTest(app->GetSession(id), runLoop);
      app->Await(
        runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  });
  return app->Run(runLoop);
}
