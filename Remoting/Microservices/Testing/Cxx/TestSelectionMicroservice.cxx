/*=========================================================================

Program:   ParaView
Module:    TestFileSystemMicroservice.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkFileSystemMicroservice.
 */

#include "vtkClientSession.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPVDataInformation.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMSourceProxy.h"
#include "vtkSelectionMicroservice.h"
#include "vtkTestUtilities.h"
#include "vtkrxcpp/rx-util.hpp"

#include <cstdlib>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

#define VTK_EXPECT(x, n)                                                                           \
  if (x != n)                                                                                      \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x " == " #n ")...failed!");                                   \
    throw std::runtime_error("Test failed");                                                       \
  }

bool DoSelectionMicroserviceTest(vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{
  vtkNew<vtkSelectionMicroservice> sservice;
  vtkNew<vtkPipelineBuilderMicroservice> builder;
  auto app = vtkPVApplication::GetInstance();

  builder->SetSession(session);
  sservice->SetSession(session);
  auto source = app->Await(runLoop, builder->CreateSource("sources", "FastUniformGrid"));
  std::string queryString = "(DistanceSquared<=1)";
  auto selection = app->Await(runLoop,
    sservice->QuerySelect(
      queryString, vtkSelectionMicroservice::SelectionFieldType::POINT, /*invert*/ false));
  selection->UpdateVTKObjects();
  auto extractor = app->Await(runLoop, sservice->Extract(source, selection));

  extractor->UpdateVTKObjects();
  bool status = app->Await(runLoop, extractor->UpdatePipeline(0));
  VTK_EXPECT(status, true);

  auto dataInfoRequest = vtkSmartPointer<vtkPVDataInformation>::New();
  dataInfoRequest->SetPortNumber(0);
  // if on MPI we need to gather data from all ranks
  auto di = vtk::MakeSmartPointer(vtkPVDataInformation::SafeDownCast(
    app->Await(runLoop, extractor->GatherInformation(dataInfoRequest))));

  if (app->GetNumberOfRanks() > 1)
  {
    VTK_EXPECT(di->GetNumberOfPoints(), 12); // this includes ghost points
  }
  else
  {
    VTK_EXPECT(di->GetNumberOfPoints(), 7);
  }
  source->Delete();
  selection->Delete();
  extractor->Delete();
  return true;
}

int TestSelectionMicroservice(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // process command line arguments.
  CLI::App cli("TestSelectionMicroservice application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  auto* options = app->GetOptions();
  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop));
  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }
  vtkTypeUInt32 id = app->Await(runLoop, sessionIdObservable);

  if (id == 0)
  {
    app->Exit(1);
  }
  else
  {
    bool success = DoSelectionMicroserviceTest(app->GetSession(id), runLoop);
    app->Await(runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
    app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
  }

  return app->Run(runLoop);
}
