/*=========================================================================

Program:   ParaView
Module:    TestPipelineViewerMicroservice.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkActiveObjectsMicroservice
 * Validates that the two observables of vtkActiveObjectsMicroservice are emitting the expected
 * results as new proxies are created and selected.
 */
#include "vtkActiveObjectsMicroservice.h"
#include "vtkClientSession.h"
#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkPipelineViewerMicroservice.h"
#include "vtkSMProxy.h"
#include "vtkSMProxySelectionModel.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"
#include "vtkSmartPointer.h"
#include "vtkTestUtilities.h"

#include <cstdlib>
#include <vector>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

bool DoPipelineViewerMicroserviceTest(
  vtkClientSession* session, rxcpp::schedulers::run_loop& runLoop)
{

  vtkNew<vtkPipelineBuilderMicroservice> pipelineBuilder;
  vtkNew<vtkPipelineViewerMicroservice> pipelineViewer;
  auto app = vtkPVApplication::GetInstance();

  pipelineBuilder->SetSession(session);
  pipelineViewer->SetSession(session);

  rxcpp::composite_subscription subscription = pipelineViewer->GetObservable().subscribe(
    [](const std::vector<vtkSmartPointer<vtkPipelineElement>>& pipeline) {
      vtkLog(WARNING, "pipeline size " << pipeline.size());
      vtkLogF(INFO, "pipeline : {");
      for (const auto& item : pipeline)
      {
        vtkLogF(INFO, "name %s, item %d , parents %d", item->GetName().c_str(), item->GetID(),
          item->GetParentIDs().empty() ? 0 : item->GetParentIDs()[0]);
      }
      vtkLogF(INFO, " }");
    });

  vtkSmartPointer<vtkSMSourceProxy> source1 = nullptr;
  bool done = false;
  pipelineBuilder->CreateSource("sources", "SphereSource")
    .subscribe([&source1](vtkSMSourceProxy* newProxy) {
      VALIDATE(newProxy);
      source1 = vtk::TakeSmartPointer(newProxy);
    });

  vtkSmartPointer<vtkSMSourceProxy> source2 = nullptr;
  pipelineBuilder->CreateSource("sources", "RTAnalyticSource")
    .subscribe([&source2](vtkSMSourceProxy* newProxy) {
      VALIDATE(newProxy);
      source2 = vtk::TakeSmartPointer(newProxy);
    });

  // before moving on lets make source all sources have been constructed

  vtkSmartPointer<vtkSMSourceProxy> source3 = vtk::TakeSmartPointer(
    app->Await(runLoop, pipelineBuilder->CreateSource("sources", "CylinderSource")));
  VALIDATE(source3 != nullptr);

  vtkSmartPointer<vtkSMSourceProxy> filter1;
  pipelineBuilder->CreateFilter("filters", "ShrinkFilter", source1)
    .subscribe([&filter1](vtkSMSourceProxy* newProxy) {
      VALIDATE(newProxy);
      filter1 = vtk::TakeSmartPointer(newProxy);
    });
  vtkSmartPointer<vtkSMSourceProxy> filter2;
  pipelineBuilder->CreateFilter("filters", "ShrinkFilter", source2)
    .subscribe([&filter2](vtkSMSourceProxy* newProxy) {
      VALIDATE(newProxy);
      filter2 = vtk::TakeSmartPointer(newProxy);
    });

  vtkSmartPointer<vtkSMSourceProxy> filter3 = vtk::TakeSmartPointer(
    app->Await(runLoop, pipelineBuilder->CreateFilter("filters", "ShrinkFilter", source3)));
  VALIDATE(filter3 != nullptr);
  subscription.unsubscribe();
  return true;
}

int TestPipelineViewerMicroservice(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestPipelineViewerMicroservice application");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());

  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
  }

  sessionIdObservable.subscribe([&](vtkTypeUInt32 id) {
    if (id == 0)
    {
      app->Exit(1);
    }
    else
    {
      const bool success = DoPipelineViewerMicroserviceTest(app->GetSession(id), runLoop);
      app->Await(
        runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  });
  return app->Run(runLoop);
}
