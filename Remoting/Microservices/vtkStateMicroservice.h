/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkStateMicroservice.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkStateMicroservice
 * @brief A microservice to save and load application state.
 *
 * vtkStateMicroservice can be used to save a snapshot of the application state
 * or load the application state from a state file.
 *
 */

#ifndef vtkStateMicroservice_h
#define vtkStateMicroservice_h

#include "vtkObject.h"

#include "vtkRemotingMicroservicesModule.h" // for export macro
#include "vtkSMState.h"                     // for arg

#include <memory> // for unique_ptr

class vtkClientSession;
class VTKREMOTINGMICROSERVICES_EXPORT vtkStateMicroservice : public vtkObject
{
public:
  vtkTypeMacro(vtkStateMicroservice, vtkObject);
  static vtkStateMicroservice* New();
  void PrintSelf(ostream& os, vtkIndent indent) override;

  vtkSMState Save();

  ///@{
  /**
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

protected:
  vtkStateMicroservice();
  ~vtkStateMicroservice() override;

private:
  vtkStateMicroservice(const vtkStateMicroservice&) = delete;
  void operator=(const vtkStateMicroservice&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
