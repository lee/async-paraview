/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkVariantMap.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVariantMap.h"
#include "vtkObjectFactory.h"
#include <map>

class vtkVariantMap::vtkInternals
{
public:
  std::map<std::string, vtkVariant> data;
};

//=============================================================================

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVariantMap);

//-----------------------------------------------------------------------------
vtkVariantMap::vtkVariantMap()
  : Internals(new vtkVariantMap::vtkInternals())
{
}

//-----------------------------------------------------------------------------
vtkVariantMap::~vtkVariantMap() = default;

//-----------------------------------------------------------------------------
void vtkVariantMap::PrintSelf(ostream& os, vtkIndent indent)
{
  const auto& internals = (*this->Internals);
  for (const auto& item : internals.data)
  {
    os << indent << item.first << " : ";
    if (item.second.IsVTKObject())
    {
      os << item.second.ToVTKObject() << "\n";
    }
    else
    {
      os << item.second.ToString() << "\n";
    }
  }
}

//-----------------------------------------------------------------------------
void vtkVariantMap::Set(const std::string& key, const vtkVariant& value)
{
  this->Internals->data.insert({ key, value });
}

//-----------------------------------------------------------------------------
vtkVariant vtkVariantMap::Get(const std::string& key) const
{
  const auto& internals = (*this->Internals);
  const auto iter = internals.data.find(key);
  if (iter == internals.data.end())
  {
    return vtkVariant();
  }
  return iter->second;
}

//-----------------------------------------------------------------------------
std::map<std::string, vtkVariant>::iterator vtkVariantMap::begin()
{
  return this->Internals->data.begin();
}

//-----------------------------------------------------------------------------
std::map<std::string, vtkVariant>::iterator vtkVariantMap::end()
{
  return this->Internals->data.end();
}

//-----------------------------------------------------------------------------
std::map<std::string, vtkVariant>::const_iterator vtkVariantMap::begin() const
{
  return this->Internals->data.cbegin();
}

//-----------------------------------------------------------------------------
std::map<std::string, vtkVariant>::const_iterator vtkVariantMap::end() const
{
  return this->Internals->data.cend();
}
