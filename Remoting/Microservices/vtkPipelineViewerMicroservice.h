/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPipelineViewerMicroservice.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkPipelineViewerMicroservice
 * @brief Get the connectivity of the pipeline associated with a session.
 *
 *
 */

#ifndef vtkPipelineViewerMicroservice_h
#define vtkPipelineViewerMicroservice_h

#include "vtkObject.h"
#include "vtkRemotingMicroservicesModule.h" // for exports
#include "vtkSmartPointer.h"                // for vtkSmartPointer

#include "vtkPythonObservableWrapper.h" // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkSMProxy.h" // due to vtkPythonObservableWrapper limitations full definition is required

#include <memory> // for std::unique_ptr

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkClientSession;
class vtkSMViewProxy;

class vtkPipelineElement;

/**
 * @brief Adaptor for vtkSMProxy for representing elements in the pipeline browser.
 */
class VTKREMOTINGMICROSERVICES_EXPORT vtkPipelineElement : public vtkObject
{
public:
  static vtkPipelineElement* New();
  vtkTypeMacro(vtkPipelineElement, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  // Get proxy global ids for representations of this elements on \p view
  std::vector<vtkTypeUInt32> GetRepresentationIDs(vtkSMViewProxy* view);

  // Get Global id of the underlying proxy
  vtkTypeUInt32 GetID() { return this->Proxy->GetGlobalID(); }

  ///@{
  // Get/Set  Name of the underlying proxy
  std::string GetName() { return ProxyName; }
  void SetName(const std::string& name) { ProxyName = name; };
  ///@}

  // Get proxy global ids for all proxies that are inputs to this.
  // @note only pipeline proxies are included.
  std::vector<vtkTypeUInt32> GetParentIDs();

  void SetProxy(vtkSMProxy* proxy) { Proxy = proxy; };

private:
  vtkSMProxy* Proxy;
  // this is the name shown on the ui.
  std::string ProxyName;

protected:
  vtkPipelineElement();
  ~vtkPipelineElement() override;

private:
  vtkPipelineElement(const vtkPipelineElement&) = delete;
  void operator=(const vtkPipelineElement&) = delete;
};

class VTKREMOTINGMICROSERVICES_EXPORT vtkPipelineViewerMicroservice : public vtkObject
{
public:
  static vtkPipelineViewerMicroservice* New();
  vtkTypeMacro(vtkPipelineViewerMicroservice, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Returns an observable for the current pipeline connectivity.
   *
   * The return value is a cold observable i.e. it does not start emitting
   * values until something subscribes to it.
   *
   * @section Triggers Triggers
   *
   * * on_next: every time a pipeline proxy is created or deleted via vtkPipelineBuilderMicroservice
   *
   * * on_error: if Session is not set.
   *
   * * on_completed: when this instance is destroyed.
   */
  rxcpp::observable<std::vector<vtkSmartPointer<vtkPipelineElement>>> GetObservable() const;
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    std::vector<vtkSmartPointer<vtkObject>>, GetObservable() const);
  ///@}

  // Get current state of the pipeline connectivity.
  std::vector<vtkSmartPointer<vtkPipelineElement>> GetCurrentState() const;

  ///@{
  /**
   * Get/set the session. Changing the session will cause the current/selection to
   * change and hence trigger `on_next` call on the subscribed observables, if any.
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

  void UnsubscribeAll();

protected:
  vtkPipelineViewerMicroservice();
  ~vtkPipelineViewerMicroservice() override;

private:
  vtkPipelineViewerMicroservice(const vtkPipelineViewerMicroservice&) = delete;
  void operator=(const vtkPipelineViewerMicroservice&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
