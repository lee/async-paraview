/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPropertyManagerMicroservice.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPropertyManagerMicroservice.h"
#include "vtkSMInputProperty.h"
#include "vtkSMProperty.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMPropertyIterator.h"
#include "vtkSMProxy.h"
#include "vtkSMProxyListDomain.h"
#include "vtkVariant.h"

class vtkPropertyManagerMicroservice::vtkInternals
{
public:
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPropertyManagerMicroservice);
//-----------------------------------------------------------------------------
vtkPropertyManagerMicroservice::vtkPropertyManagerMicroservice()
  : Internals(new vtkPropertyManagerMicroservice::vtkInternals())
{
}
//-----------------------------------------------------------------------------
vtkPropertyManagerMicroservice::~vtkPropertyManagerMicroservice() = default;
//-----------------------------------------------------------------------------
std::vector<vtkVariant> vtkPropertyManagerMicroservice::GetPropertyValue(
  vtkSMProxy* proxy, const std::string& name, bool useUnchecked)
{
  if (!proxy)
  {
    vtkLogF(ERROR, "provided proxy is null, property is %s ", name.c_str());
    return {};
  }
  std::vector<vtkVariant> value;
  vtkSMProperty* property = proxy->GetProperty(name.c_str());
  if (!property)
  {
    vtkLogF(ERROR, "unknown property %s for proxy %s", name.c_str(), proxy->GetXMLName());
    return {};
  }
  vtkSMPropertyHelper helper(property);
  helper.SetUseUnchecked(useUnchecked);
  value.resize(helper.GetNumberOfElements());
  for (int i = 0; i < helper.GetNumberOfElements(); i++)
  {
    value[i] = helper.GetAsVariant(i);
  }
  return value;
}

//-----------------------------------------------------------------------------
void vtkPropertyManagerMicroservice::SetPropertyValue(
  vtkSMProxy* proxy, const std::string& name, const vtkVariant& value, bool useUnchecked)
{
  const std::vector<vtkVariant> array({ value });
  this->SetPropertyValue(proxy, name, array, useUnchecked);
}

//-----------------------------------------------------------------------------
void vtkPropertyManagerMicroservice::SetPropertyValue(vtkSMProxy* proxy, const std::string& name,
  const std::vector<vtkVariant>& value, bool useUnchecked)
{
  if (!proxy)
  {
    vtkLogF(ERROR, "provided proxy is null, property is %s ", name.c_str());
    return;
  }
  vtkSMProperty* property = proxy->GetProperty(name.c_str());
  if (!property)
  {
    vtkLogF(ERROR, "unknown property %s for proxy %s", name.c_str(), proxy->GetXMLName());
    return;
  }
  vtkSMPropertyHelper helper(property);
  helper.SetUseUnchecked(useUnchecked);
  if (helper.GetNumberOfElements() != value.size())
  {
    bool expected_for_input_propery = false;
    if (property->GetElementType() == vtkSMPropertyTypes::INPUT)
    {
      expected_for_input_propery = true;
    }

    // it is expected for a proxy property with no domains to have a mismatch since the number of
    // elements is specified from the Domains.
    const bool expected_for_proxy_propery =
      (property->GetElementType() == vtkSMPropertyTypes::PROXY &&
        property->GetNumberOfDomains() == 0);
    const bool expected = (expected_for_input_propery || expected_for_proxy_propery);
    if (!expected)
    {
      vtkLogF(ERROR, "Size mitsmatch between property %s and vector size: %ld !", name.c_str(),
        value.size());
      return;
    }
    helper.SetNumberOfElements(value.size());
  }

  for (unsigned int i = 0; i < helper.GetNumberOfElements(); i++)
  {
    if (helper.GetAsVariant(i) != value[i])
    {
      helper.Set(i, value[i]);
    }
  }
}

//-----------------------------------------------------------------------------
void vtkPropertyManagerMicroservice::SetPropertyValueElement(
  vtkSMProxy* proxy, const std::string& name, const vtkVariant& value, int index, bool useUnchecked)
{
  if (!proxy)
  {
    vtkLogF(ERROR, "provided proxy is null, property is %s ", name.c_str());
    return;
  }
  vtkSMProperty* property = proxy->GetProperty(name.c_str());
  if (!property)
  {
    vtkLogF(ERROR, "unknown property %s for proxy %s", name.c_str(), proxy->GetXMLName());
    return;
  }
  vtkSMPropertyHelper helper(property);
  helper.SetUseUnchecked(useUnchecked);
  if (helper.GetNumberOfElements() <= index)
  {
    // input properties accept a single proxy unless MultipleInput is set
    bool expected_for_input_propery = false;
    if (property->GetElementType() == vtkSMPropertyTypes::INPUT)
    {
      if (index == 0)
      {
        expected_for_input_propery = true;
      }
      else
      {
        vtkSMInputProperty* ip = vtkSMInputProperty::SafeDownCast(property);
        expected_for_input_propery = ip->GetMultipleInput();
      }
    }

    // it is expected for a proxy property with no domains to have a mismatch since the number of
    // elements is specified from the Domains.
    const bool expected_for_proxy_propery =
      (property->GetElementType() == vtkSMPropertyTypes::PROXY &&
        property->GetNumberOfDomains() == 0);
    const bool expected = (expected_for_input_propery || expected_for_proxy_propery);
    if (!expected)
    {
      vtkLogF(ERROR, "Size mismatch between property size %s(%d) and index: %d !", name.c_str(),
        helper.GetNumberOfElements(), index);
      return;
    }
  }

  vtkVariant transformedValue = value;

  // string was passed for a proxy property. Check if there is an arraylist
  // domain and convert the value to a proxy
  if (value.IsString() && property->GetElementType() == vtkSMPropertyTypes::PROXY)
  {
    if (vtkSMProxyListDomain* domain = property->FindDomain<vtkSMProxyListDomain>())
    {
      vtkSMProxy* pvalue = domain->GetProxyWithName(value.ToString().c_str());
      transformedValue = pvalue;
    }
    else
    {
      vtkLogF(ERROR, "Type mitsmatch between property %s and value of type: %d !", name.c_str(),
        value.GetType());
      return;
    }
  }

  helper.Set(index, transformedValue);
}

//-----------------------------------------------------------------------------
vtkVariant vtkPropertyManagerMicroservice::GetPropertyValueElement(
  vtkSMProxy* proxy, const std::string& name, int index, bool useUnchecked)
{
  if (!proxy)
  {
    vtkLogF(ERROR, "provided proxy is null, property is %s ", name.c_str());
    return vtkVariant();
  }
  vtkSMProperty* property = proxy->GetProperty(name.c_str());
  if (!property)
  {
    vtkLogF(ERROR, "unknown property %s for proxy %s", name.c_str(), proxy->GetXMLName());
    return vtkVariant();
  }
  vtkSMPropertyHelper helper(property);
  helper.SetUseUnchecked(useUnchecked);
  vtkVariant value = helper.GetAsVariant(index);
  return value;
}

//-----------------------------------------------------------------------------
int vtkPropertyManagerMicroservice::GetNumberOfValues(
  vtkSMProxy* proxy, const std::string& name, bool useUnchecked)
{
  vtkSMProperty* property = proxy->GetProperty(name.c_str());
  if (!property)
  {
    vtkLogF(ERROR, "unknown property %s for proxy %s", name.c_str(), proxy->GetXMLName());
    return -1;
  }
  vtkSMPropertyHelper helper(property);
  helper.SetUseUnchecked(useUnchecked);
  return helper.GetNumberOfElements();
}

//-----------------------------------------------------------------------------
void vtkPropertyManagerMicroservice::SetNumberOfValues(
  vtkSMProxy* proxy, const std::string& name, int size, bool useUnchecked)
{
  vtkSMProperty* property = proxy->GetProperty(name.c_str());
  if (!property)
  {
    vtkLogF(ERROR, "unknown property %s for proxy %s", name.c_str(), proxy->GetXMLName());
    return;
  }
  vtkSMPropertyHelper helper(property);
  helper.SetUseUnchecked(useUnchecked);
  helper.SetNumberOfElements(size);
}

//-----------------------------------------------------------------------------
void vtkPropertyManagerMicroservice::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
