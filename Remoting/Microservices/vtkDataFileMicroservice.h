/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkDataFileMicroservice.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkDataFileMicroservice
 * @brief A microservice which opens data files that can be visualized in ParaView.
 *
 * vtkDataFileMicroservice loads a data file from the file system. The file extension
 * is matched against registered file readers in order to use the correct file reader.
 *
 * The `FindPossibleReaders` method finds possible readers for a file or fileseries.
 * The `Open` method returns a reader proxy for a fileseries.
 * In this case, the first available reader is used.
 */

#ifndef vtkDataFileMicroservice_h
#define vtkDataFileMicroservice_h

#include "vtkObject.h"

#include "vtkPythonObservableWrapper.h"     // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkRemotingMicroservicesModule.h" // for exports
#include "vtkSMSourceProxy.h"               // for return
#include "vtkStringArray.h"                 // for return

#include <memory> // for std::unique_ptr
#include <string> // for arg
#include <vector> // for arg

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkClientSession;

class VTKREMOTINGMICROSERVICES_EXPORT vtkDataFileMicroservice : public vtkObject
{
public:
  static vtkDataFileMicroservice* New();
  vtkTypeMacro(vtkDataFileMicroservice, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * Get the names of proxies that can be used to open the file.
   *
   * Throws std::runtime_error if session is not set.
   */
  rxcpp::observable<vtkSmartPointer<vtkStringArray>> FindPossibleReaders(
    const std::string& filename);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    vtkSmartPointer<vtkObject>, FindPossibleReaders(const std::string& filename));

  /**
   * Get the names of proxies that can be used to open a group of files.
   *
   * Throws std::runtime_error if session is not set.
   *
   * @note A fileseries is expected to have similarly named files with alphanumeric suffices.
   * Ex: ["can.ex.0", "can.ex.1", "can.ex.2"] is a valid file group.
   */
  rxcpp::observable<vtkSmartPointer<vtkStringArray>> FindPossibleReaders(
    const std::vector<std::string>& filegroup);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    vtkSmartPointer<vtkObject>, FindPossibleReaders(const std::vector<std::string>& filegroup));

  /**
   * Get a reader which can open the data file.
   *
   * Throws std::runtime_error if session is not set.
   */
  rxcpp::observable<vtkSMSourceProxy*> Open(const std::string& filename);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(vtkObject*, Open(const std::string& filename));

  /**
   * Get a reader which can open the group of data files.
   *
   * Throws std::runtime_error if session is not set.
   *
   * @note A filegroup is expected to have similarly named files with alphanumeric suffices.
   * Ex: ["can.ex.0", "can.ex.1", "can.ex.2"] is a valid file group.
   */
  rxcpp::observable<vtkSMSourceProxy*> Open(const std::vector<std::string>& filegroup);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(vtkObject*, Open(const std::vector<std::string>& filegroup));

  ///@{
  /**
   * Get/set the session. Changing the session will cause the current/selection to
   * change and hence trigger `on_next` call on the subscribed observables, if any.
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

protected:
  vtkDataFileMicroservice();
  ~vtkDataFileMicroservice() override;

private:
  vtkDataFileMicroservice(const vtkDataFileMicroservice&) = delete;
  void operator=(const vtkDataFileMicroservice&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
