/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkSelectionMicroservice.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkSelectionMicroservice.h"
#include "vtkClientSession.h"
#include "vtkPVCoreApplication.h"
#include "vtkPipelineBuilderMicroservice.h"
#include "vtkPropertyManagerMicroservice.h"
#include "vtkRemotingCoreUtilities.h"
#include "vtkSMParaViewPipelineController.h"
#include "vtkSMProxy.h"
#include "vtk_fmt.h"
// clang-format off
#include VTK_FMT(fmt/core.h)
// clang-format on

class vtkSelectionMicroservice::vtkInternals
{
public:
  const std::thread::id OwnerTID{ std::this_thread::get_id() };
  vtkSmartPointer<vtkClientSession> Session = { nullptr };

  //-----------------------------------------------------------------------------
  bool IsInitialized() const { return Session != nullptr; }
};

//=============================================================================

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkSelectionMicroservice);

//-----------------------------------------------------------------------------
vtkSelectionMicroservice::vtkSelectionMicroservice()
  : Internals(new vtkSelectionMicroservice::vtkInternals())
{
}

//-----------------------------------------------------------------------------
vtkSelectionMicroservice::~vtkSelectionMicroservice() = default;

//-----------------------------------------------------------------------------
void vtkSelectionMicroservice::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Initialized: " << (this->Internals->IsInitialized() ? "yes" : "no") << "\n";
}

//-----------------------------------------------------------------------------
void vtkSelectionMicroservice::SetSession(vtkClientSession* session)
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  if (internals.Session != session)
  {
    // TODO error handling
    vtkNew<vtkSMParaViewPipelineController> controller;
    controller->InitializeSession(session);
    internals.Session = session;
  }
}

//-----------------------------------------------------------------------------
vtkClientSession* vtkSelectionMicroservice::GetSession() const
{
  auto& internals = (*this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  return internals.Session;
}

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSMSourceProxy*> vtkSelectionMicroservice::QuerySelect(
  const std::string& query, int elementType, bool invert)
{
  vtkLogF(WARNING, "Entetring");
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error("Session was not set!"));
  }

  if (query.empty())
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error("Empty Query provided"));
  }

  if (elementType != SelectionFieldType::POINT && elementType != SelectionFieldType::CELL)
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error(fmt::format("Unsupported elementType value provided: {}", elementType)));
  }

  auto builder = vtkSmartPointer<vtkPipelineBuilderMicroservice>::New();
  builder->SetSession(internals.Session);
  return builder->CreateSource("sources", "SelectionQuerySource")
    .map([query, elementType, invert](vtkSMSourceProxy* selection) {
      vtkSmartPointer<vtkPropertyManagerMicroservice> pmanager;
      pmanager->SetPropertyValue(selection, "QueryString", vtkVariant(query));
      pmanager->SetPropertyValue(selection, "ElementType", elementType);
      pmanager->SetPropertyValue(selection, "InsideOut", invert);
      selection->UpdateVTKObjects();
      return selection;
    })
    .take(1);
}

//-----------------------------------------------------------------------------
rxcpp::observable<vtkSMSourceProxy*> vtkSelectionMicroservice::Extract(
  vtkSMSourceProxy* producer, vtkSMSourceProxy* selection)
{
  const auto& internals = (*this->Internals);
  if (!internals.IsInitialized())
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error("Session was not set!"));
  }
  if (producer == nullptr)
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error("Provided producer for Extract is null!"));
  }
  if (selection == nullptr)
  {
    return rxcpp::observable<>::error<vtkSMSourceProxy*>(
      std::runtime_error("Provided selection for Extract is null!"));
  }

  auto builder = vtkSmartPointer<vtkPipelineBuilderMicroservice>::New();
  builder->SetSession(internals.Session);
  return builder->CreateFilter("filters", "ExtractSelection", producer)
    .map([selection](vtkSMSourceProxy* proxy) {
      vtkSmartPointer<vtkPropertyManagerMicroservice> pmanager;
      pmanager->SetPropertyValue(proxy, "Selection", vtkVariant(selection));
      proxy->UpdateVTKObjects();
      return proxy;
    })
    .take(1);
}
