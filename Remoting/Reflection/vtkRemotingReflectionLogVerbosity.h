/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingReflectionLogVerbosity.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkRemotingReflectionLogVerbosity_h
#define vtkRemotingReflectionLogVerbosity_h

#include "vtkLogger.h"
#include "vtkRemotingReflectionModule.h"

/**
 * Looks up system environment for `VTKREMOTINGREFLECTION_LOG_VERBOSITY` that shall
 * be used to set logger verbosity for the `vtkRemotingReflectionModule`.
 * The default value is TRACE.
 *
 * Accepted string values are OFF, ERROR, WARNING, INFO, TRACE, MAX, INVALID or ASCII
 * representation for an integer in the range [-9,9].
 *
 * @note This class internally uses vtkLogger::ConvertToVerbosity(const char*).
 */
class VTKREMOTINGREFLECTION_EXPORT vtkRemotingReflectionLogVerbosity
{
public:
  /**
   * Get the current log verbosity for the RemotingReflection module.
   */
  static vtkLogger::Verbosity GetVerbosity();
  static void SetVerbosity(vtkLogger::Verbosity verbosity);
};

/**
 * Macro to use for verbosity when logging ParaView::RemotingReflection messages. Same as calling
 * vtkRemotingReflectionLogVerbosity::GetVerbosity() e.g.
 *
 * @code{cpp}
 *  vtkVLogF(VTKRemotingReflection_LOG_VERBOSITY(), "a message");
 * @endcode
 */
#define VTKREMOTINGREFLECTION_LOG_VERBOSITY() vtkRemotingReflectionLogVerbosity::GetVerbosity()

#endif // vtkRemotingReflectionLogVerbosity_h
