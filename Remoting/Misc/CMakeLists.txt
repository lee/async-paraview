# SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
# SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
# SPDX-License-Identifier: BSD-3-Clause
set(classes
  vtkSMExtractSelectionProxyInitializationHelper)

vtk_module_add_module(AsyncParaView::RemotingMisc
  CLASSES ${classes})
