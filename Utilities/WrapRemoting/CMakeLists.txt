if (NOT TARGET AsyncParaView::WrapRemoting)
  vtk_module_add_executable(AsyncParaView::WrapRemoting
    vtkWrapRemoting.cxx)
endif()
